#!/bin/bash

WHERE=${HOME}/tfm_jmjurado
BINS="gpu_v42_laptop"
THRS="32 64 128 256"

declare -a TESTS=("-s 1 -p nube_66M.ply -i images_180.txt"
                  "-s 1 -p nube_66M.ply -i images_1352.txt"
		  "-s 1 -p nube_271M.ply -i images_180.txt"
		  "-s 1 -p nube_271M.ply -i images_1352.txt"
		  "-s 1 -p nube_542M.ply -i images_180.txt"
		  "-s 1 -p nube_542M.ply -i images_1352.txt")

#GNU CUDA
GCC_ver=`gcc --version | head -n 1`
echo -e "\n* Compilando CUDA con GNU: ${GCC_ver}"

cd ${WHERE}
make -f makefile_LAPTOP cleanall
make -f makefile_LAPTOP ${BINS}

for r in "${TESTS[@]}"
do
	echo -e "\n* TEST: $r"

	for b in ${BINS}
	do
		for t in ${THRS}
		do
			echo -e "\n** $t hilos/bloque"
			echo -e "./$b -h $t $r"
			echo -e "\n*** Run #1"
			./$b -h $t $r
			echo -e "\n*** Run #2"
			./$b -h $t $r
			echo -e "\n*** Run #3"
			./$b -h $t $r
		done
	done
done
