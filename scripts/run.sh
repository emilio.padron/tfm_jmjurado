#!/bin/bash

WHERE=${HOME}/tfm_jmjurado

#GNU
GCC_ver=`gcc --version | head -n 1`
echo -e "\n* Compilando pureCPU con GNU: ${GCC_ver}"

cd ${WHERE}/pureCPU
make -f makefile_benchgnu cleanall
make -f makefile_benchgnu

echo -e "\n* Ejecutando: GNU -O0"
./O0

echo -e "\n* Ejecutando: GNU -O1"
./O1

echo -e "\n* Ejecutando: GNU -O2"
./O2

echo -e "\n* Ejecutando: GNU -O3"
./O3

echo -e "\n* Ejecutando: GNU -Ofast"
./Ofast

echo -e "\n* Ejecutando: GNU OPMP (2 hilos)"
export OMP_NUM_THREADS=2
./Opmp

echo -e "\n* Ejecutando: GNU OPMP (4 hilos)"
export OMP_NUM_THREADS=4
./Opmp

echo -e "\n* Ejecutando: GNU OPMP (8 hilos)"
export OMP_NUM_THREADS=8
./Opmp


#GNU CUDA
echo -e "\n* Compilando CUDA con GNU: ${GCC_ver}"

cd ${WHERE}
make  cleanall
make gpu gpu_v31 gpu_v32

echo -e "\n* Ejecutando: CUDA base (GNU)"

echo -e "\n** 32 hilos/bloque (GNU)"
./gpu 32

echo -e "\n** 64 hilos/bloque (GNU)"
./gpu 64

echo -e "\n** 128 hilos/bloque (GNU)"
./gpu 128

echo -e "\n* Ejecutando: CUDA out-of-core bloq [v31] (GNU)"

echo -e "\n** 32 hilos/bloque (GNU)"
./gpu_v31 32

echo -e "\n** 64 hilos/bloque (GNU)"
./gpu_v31 64

echo -e "\n** 128 hilos/bloque (GNU)"
./gpu_v31 128

echo -e "\n* Ejecutando: CUDA out-of-core non-bloq [v32] (GNU)"

echo -e "\n** 32 hilos/bloque (GNU)"
./gpu_v32 32

echo -e "\n** 64 hilos/bloque (GNU)"
./gpu_v32 64

echo -e "\n** 128 hilos/bloque (GNU)"
./gpu_v32 128


#Intel
ICC_ver=`icc --version | head -n 1`
echo -e "\n* Compilando pureCPU con Intel: ${ICC_ver}"

cd ${WHERE}/pureCPU
make -f makefile_benchintel cleanall
make -f makefile_benchintel

echo -e "\n* Ejecutando: Intel -O0"
./iO0

echo -e "\n* Ejecutando: Intel -O1"
./iO1

echo -e "\n* Ejecutando: Intel -O2"
./iO2

echo -e "\n* Ejecutando: Intel -O3"
./iO3

echo -e "\n* Ejecutando: Intel -Ofast"
./iOfast

echo -e "\n* Ejecutando: Intel -fast"
./ifast

echo -e "\n* Ejecutando: Intel -parallel"
./iparallel

echo -e "\n* Ejecutando: Intel OPMP (2 hilos)"
export OMP_NUM_THREADS=2
./iOpmp

echo -e "\n* Ejecutando: Intel OPMP (4 hilos)"
export OMP_NUM_THREADS=4
./iOpmp

echo -e "\n* Ejecutando: Intel OPMP (8 hilos)"
export OMP_NUM_THREADS=8
./iOpmp

exit 0

# No Intel with CUDA for the time being...

#Intel CUDA
echo -e "\n* Compilando CUDA con Intel: ${ICC_ver}"

cd ${WHERE}
make -f makefile_Intel cleanall
make -f makefile_Intel igpu igpu_v31 igpu_v32

echo -e "\n* Ejecutando: CUDA base (Intel)"

echo -e "\n** 32 hilos/bloque (Intel)"
./igpu 32

echo -e "\n** 64 hilos/bloque (Intel)"
./igpu 64

echo -e "\n** 128 hilos/bloque (Intel)"
./igpu 128

echo -e "\n* Ejecutando: CUDA out-of-core bloq [v31] (Intel)"

echo -e "\n** 32 hilos/bloque (Intel)"
./igpu_v31 32

echo -e "\n** 64 hilos/bloque (Intel)"
./igpu_v31 64

echo -e "\n** 128 hilos/bloque (Intel)"
./igpu_v31 128

echo -e "\n* Ejecutando: CUDA out-of-core non-bloq [v32] (Intel)"

echo -e "\n** 32 hilos/bloque (Intel)"
./igpu_v32 32

echo -e "\n** 64 hilos/bloque (Intel)"
./igpu_v32 64

echo -e "\n** 128 hilos/bloque (Intel)"
./igpu_v32 128
