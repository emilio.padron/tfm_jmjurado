#!/bin/bash

WHERE=${HOME}/tfm_jmjurado
BINS="gpu_v32i gpu_v42 gpu_v42b"
THRS="32 62 128"

declare -a TESTS=("-n 1 -s 1 -p nube_66M.ply -i images_180.txt"
                 "-n 1 -s 1 -p nube_66M.ply -i images_1352.txt"
		 "-n 1 -s 1 -p nube_271M.ply -i images_180.txt"
		 "-n 1 -s 1 -p nube_271M.ply -i images_1352.txt"
                 "-n 1 -s 2 -p nube_66M.ply -i images_180.txt"
                 "-n 1 -s 2 -p nube_66M.ply -i images_1352.txt"
		 "-n 1 -s 2 -p nube_271M.ply -i images_180.txt"
		 "-n 1 -s 2 -p nube_271M.ply -i images_1352.txt"
                 "-n 4 -s 1 -p nube_66M.ply -i images_180.txt"
                 "-n 4 -s 1 -p nube_66M.ply -i images_1352.txt"
		 "-n 4 -s 1 -p nube_271M.ply -i images_180.txt"
		 "-n 4 -s 1 -p nube_271M.ply -i images_1352.txt"
                 "-n 4 -s 2 -p nube_66M.ply -i images_180.txt"
                 "-n 4 -s 2 -p nube_66M.ply -i images_1352.txt"
		 "-n 4 -s 2 -p nube_271M.ply -i images_180.txt"
		 "-n 4 -s 2 -p nube_271M.ply -i images_1352.txt"
                 "-n 8 -s 1 -p nube_66M.ply -i images_180.txt"
                 "-n 8 -s 1 -p nube_66M.ply -i images_1352.txt"
		 "-n 8 -s 1 -p nube_271M.ply -i images_180.txt"
		 "-n 8 -s 1 -p nube_271M.ply -i images_1352.txt"
                 "-n 8 -s 2 -p nube_66M.ply -i images_180.txt"
                 "-n 8 -s 2 -p nube_66M.ply -i images_1352.txt"
		 "-n 8 -s 2 -p nube_271M.ply -i images_180.txt"
		 "-n 8 -s 2 -p nube_271M.ply -i images_1352.txt"
                 "-n 11 -s 1 -p nube_66M.ply -i images_180.txt"
                 "-n 11 -s 1 -p nube_66M.ply -i images_1352.txt"
		 "-n 11 -s 1 -p nube_271M.ply -i images_180.txt"
		 "-n 11 -s 1 -p nube_271M.ply -i images_1352.txt"
                 "-n 11 -s 2 -p nube_66M.ply -i images_180.txt"
                 "-n 11 -s 2 -p nube_66M.ply -i images_1352.txt"
		 "-n 11 -s 2 -p nube_271M.ply -i images_180.txt"
		 "-n 11 -s 2 -p nube_271M.ply -i images_1352.txt")

#GNU CUDA
GCC_ver=`gcc --version | head -n 1`
echo -e "\n* Compilando CUDA con GNU: ${GCC_ver}"

cd ${WHERE}
make -f makefile_PLUTON cleanall
make -f makefile_PLUTON ${BINS}

for r in "${TESTS[@]}"
do
	echo -e "\n* TEST: $r"

	for b in ${BINS}
	do
		for t in ${THRS}
		do
			echo -e "\n** $t hilos/bloque"
			echo -e "./$b -h $t $r"
			echo -e "\n*** Run #1"
			./$b -h $t $r
			echo -e "\n*** Run #2"
			./$b -h $t $r
			echo -e "\n*** Run #3"
			./$b -h $t $r
		done
	done
done
