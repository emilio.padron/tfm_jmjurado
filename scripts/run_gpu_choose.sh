#!/bin/bash

WHERE=${HOME}/tfm_jmjurado

#GNU CUDA
GCC_ver=`gcc --version | head -n 1`
echo -e "\n* Compilando CUDA con GNU: ${GCC_ver}"

cd ${WHERE}
make cleanall
make gpu gpu_v31 gpu_v32

echo -e "\n* Ejecutando: CUDA base (GNU)"

echo -e "\n** 32 hilos/bloque (GNU)"
./gpu 32 0
./gpu 32 1

echo -e "\n** 64 hilos/bloque (GNU)"
./gpu 64 0
./gpu 64 1

echo -e "\n** 128 hilos/bloque (GNU)"
./gpu 128 0
./gpu 128 1

echo -e "\n* Ejecutando: CUDA out-of-core bloq [v31] (GNU)"

echo -e "\n** 32 hilos/bloque (GNU)"
./gpu_v31 32 0
./gpu_v31 32 1

echo -e "\n** 64 hilos/bloque (GNU)"
./gpu_v31 64 0
./gpu_v31 64 1

echo -e "\n** 128 hilos/bloque (GNU)"
./gpu_v31 128 0
./gpu_v31 128 1

echo -e "\n* Ejecutando: CUDA out-of-core non-bloq [v32] (GNU)"

echo -e "\n** 32 hilos/bloque (GNU)"
./gpu_v32 32 0
./gpu_v32 32 1

echo -e "\n** 64 hilos/bloque (GNU)"
./gpu_v32 64 0
./gpu_v32 64 1

echo -e "\n** 128 hilos/bloque (GNU)"
./gpu_v32 128 0
./gpu_v32 128 1

exit 0

# No Intel with CUDA for the time being...

#Intel CUDA
ICC_ver=`icc --version | head -n 1`
echo -e "\n* Compilando CUDA con Intel: ${ICC_ver}"

cd ${WHERE}
make -f makefile_Intel cleanall
make -f makefile_Intel igpu igpu_v31 igpu_v32

echo -e "\n* Ejecutando: CUDA base (Intel)"

echo -e "\n** 32 hilos/bloque (Intel)"
./igpu 32

echo -e "\n** 64 hilos/bloque (Intel)"
./igpu 64

echo -e "\n** 128 hilos/bloque (Intel)"
./igpu 128

echo -e "\n* Ejecutando: CUDA out-of-core bloq [v31] (Intel)"

echo -e "\n** 32 hilos/bloque (Intel)"
./igpu_v31 32

echo -e "\n** 64 hilos/bloque (Intel)"
./igpu_v31 64

echo -e "\n** 128 hilos/bloque (Intel)"
./igpu_v31 128

echo -e "\n* Ejecutando: CUDA out-of-core non-bloq [v32] (Intel)"

echo -e "\n** 32 hilos/bloque (Intel)"
./igpu_v32 32

echo -e "\n** 64 hilos/bloque (Intel)"
./igpu_v32 64

echo -e "\n** 128 hilos/bloque (Intel)"
./igpu_v32 128
