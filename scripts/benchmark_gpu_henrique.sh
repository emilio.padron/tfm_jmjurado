#!/bin/bash

WHERE=${HOME}/tfm_jmjurado
#BINS="gpu_v42c gpu_v43 gpu_v42_laptop gpu_soc_noUM gpu_soc"
BINS="gpu_v43"
THRS="128"

declare -a TESTS=("-p nube_66M.ply -i images_180.txt"
                 "-p nube_271M.ply -i images_180.txt"
                 "-p nube_542M.ply -i images_180.txt"
                 "-p nube_1084M.ply -i images_180.txt"
                 "-p nube_66M.ply -i images_1352.txt"
                 "-p nube_271M.ply -i images_1352.txt"
                 "-p nube_542M.ply -i images_1352.txt"
		 "-p nube_1084M.ply -i images_1352.txt")

#GNU CUDA
GCC_ver=`gcc --version | head -n 1`
echo -e "\n* Compilando CUDA con GNU: ${GCC_ver}"

cd ${WHERE}
make -f makefile cleanall
make -f makefile ${BINS}

for r in "${TESTS[@]}"
do
	echo -e "\n* TEST: $r"

	for b in ${BINS}
	do
		for t in ${THRS}
		do
			echo -e "\n** $t hilos/bloque"
			echo -e "./$b -h $t $r"
			echo -e "\n*** Run #1"
			./$b -h $t $r
			echo -e "\n*** Run #2"
			./$b -h $t $r
		done
	done
done
