#!/bin/bash
#SBATCH -t 18:00:00 # execution time hh:mm:ss *OB*
#SBATCH --cpus-per-task 16
#SBATCH --mem 32768
#SBATCH --gres gpu:K20:1
echo
echo Nodo de ejecución=${SLURM_JOB_NODELIST}
echo Cores reservados en el nodo=${SLURM_CPUS_PER_TASK}
echo Cola=${SLURM_JOB_PARTITION}

WHERE=${HOME}/tfm_jmjurado
BINS="cpu omp"

declare -a TESTS=("-p nube_66M.ply -i images_180.txt"
                  "-p nube_66M.ply -i images_1352.txt"
		  "-p nube_271M.ply -i images_180.txt"
		  "-p nube_271M.ply -i images_1352.txt")

module load gnu8

#GNU CUDA
GCC_ver=`gcc --version | head -n 1`
echo -e "\n* Compilando con GNU: ${GCC_ver}"

cd ${WHERE}
make -f makefile_PLUTON cleanall
make -f makefile_PLUTON ${BINS}

export OMP_NUM_THREADS=32

for r in "${TESTS[@]}"
do
	echo -e "\n* TEST: $r"

	for b in ${BINS}
	do
	    echo -e "./$b $r"
	    ./$b $r
	done
done
