## submit.sh

Script to run experiments in FT2's queueing system (slurm)


### Submit job to Finis Terrae II execution queues

```
sbatch submit.sh
```

More useful slurm commands to manage your jobs: `squeue`, `scancel`, etc.

Slurm cheat sheet: https://slurm.schedmd.com/pdfs/summary.pdf


## run.sh

Script to run experiments out of a queueing system (pure bash)


## Set-up

- In all cases: you need to adjust the path of your repository
(`WHERE` variable in the script)

- *CUB lib* (used for CUDA code) is expected in `${HOME}/cub-1.8.0`
in both `makefile_CESGA` and `makefile_CESGA_Intel`, but is expected
in `/opt/cub-1.8.0` in the general `makefile`

You can download this library here: 
	https://nvlabs.github.io/cub/index.html

- Input files for the experiments are expected in subdirectories `files`.
My piece of advice: put the `files` subdirectory with the input files
in you repository root directory and create a symbolic link in the pureCPU
directory for the benchmarks.


## Working interactive in FT2

Obtain a node to execute CUDA stuff interactively:

```
compute --gpu
```

