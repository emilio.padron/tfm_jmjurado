#!/bin/sh
#SBATCH -t 00:20:00 # execution time hh:mm:ss *OB*
#SBATCH -n 1 #tasks (for example, MPI processes)
#SBATCH -c 8 #cores/task (for example, shared-mem threads/process)
#SBATCH -p cola-corta
echo
echo Nodo de ejecución=${SLURM_JOB_NODELIST}
echo Cores reservados en el nodo=${SLURM_CPUS_PER_TASK}
echo Cola=${SLURM_JOB_PARTITION}

WHERE=${HOME}/tfm_jmjurado

#GNU
echo -e "\nCargamos módulo para compilar con GNU Compiler con soporte CUDA"
module load gcccore/8.3.0

GCC_ver=`gcc --version | head -n 1`
echo -e "\n* Compilando pureCPU con GNU: ${GCC_ver}"

cd ${WHERE}/pureCPU
make -f makefile_benchgnu cleanall
make -f makefile_benchgnu

echo -e "\n* Ejecutando: GNU -O0"
./O0

echo -e "\n* Ejecutando: GNU -O1"
./O1

echo -e "\n* Ejecutando: GNU -O2"
./O2

echo -e "\n* Ejecutando: GNU -O3"
./O3

echo -e "\n* Ejecutando: GNU -Ofast"
./Ofast

echo -e "\n* Ejecutando: GNU OPMP (2 hilos)"
export OMP_NUM_THREADS=2
./Opmp

echo -e "\n* Ejecutando: GNU OPMP (4 hilos)"
export OMP_NUM_THREADS=4
./Opmp

echo -e "\n* Ejecutando: GNU OPMP (8 hilos)"
export OMP_NUM_THREADS=8
./Opmp


#Intel
echo -e "\nCargamos módulo para compilar con último ICC disponible en FT2"
module load intel/2018.5.274

ICC_ver=`icc --version | head -n 1`
echo -e "\n* Compilando pureCPU con Intel: ${ICC_ver}"

cd ${HOME}/tfm_jmjurado/pureCPU
make -f makefile_benchintel cleanall
make -f makefile_benchintel

echo -e "\n* Ejecutando: Intel -O0"
./iO0

echo -e "\n* Ejecutando: Intel -O1"
./iO1

echo -e "\n* Ejecutando: Intel -O2"
./iO2

echo -e "\n* Ejecutando: Intel -O3"
./iO3

echo -e "\n* Ejecutando: Intel -Ofast"
./iOfast

echo -e "\n* Ejecutando: Intel -fast"
./ifast

echo -e "\n* Ejecutando: Intel -parallel"
./iparallel

echo -e "\n* Ejecutando: Intel OPMP (2 hilos)"
export OMP_NUM_THREADS=2
./iOpmp

echo -e "\n* Ejecutando: Intel OPMP (4 hilos)"
export OMP_NUM_THREADS=4
./iOpmp

echo -e "\n* Ejecutando: Intel OPMP (8 hilos)"
export OMP_NUM_THREADS=8
./iOpmp
