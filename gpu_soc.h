#ifndef _gpu_h
#define _gpu_h

#include <cuda_runtime.h>
#include <chrono>
#include "cpu.h"

class gputime {
public:
  unsigned int ncams {};
  unsigned int nparts {};

  std::chrono::duration<double> preliminary;

  struct percam {
    std::chrono::duration<double> allcomputation;
  } *camtime;

  struct perpartition {
    std::chrono::duration<double> allcomputation;
  } *partime;

  std::chrono::duration<double> kernels_with_transfers;

  gputime(unsigned int ncams) {
    gputime::ncams = ncams;
    camtime = new percam[ncams];
    partime = NULL;
  }

  gputime(unsigned int ncams, unsigned int nparts) {
    gputime::ncams = ncams;
    gputime::nparts = nparts;
    camtime = new percam[ncams];
    partime = new perpartition[nparts];
  }

  void addpartitions(unsigned int nparts) {
    gputime::nparts = nparts;
    partime = new perpartition[nparts];
  }

  ~gputime() {
    delete [] camtime;
    delete [] partime;
  }
};

unsigned int GPU_mapping_and_occlusion(Point *cloud, const unsigned long cloud_points, Image* images, const unsigned int num_images, frag snapshots[], const unsigned int blk_size, gputime *gpu_times, size_t nStreams, const size_t vram = 1024ul * 1024 * 1024 /*1 GB VRAM by default */);

#endif
