#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <chrono>
#include <iostream>
#include <iomanip>
#include <vector>
#include "cpu.h"
#include "gpu_common.h"
#include "gpu.h"
using namespace std;

void printout_report(gputime *gpu_times, bool verbose);

/*
  Función principal
*/
int main(int argc, char* argv[])
{
  long npoints;    // Points in dataset
  int device = 0;  // CUDA Device
  unsigned int cb; // CUDA Threads/Block
  size_t vram;     // Max GPU mem to use
  bool verbose;

  show_invocation(argc, argv);

  if (parse_args_gpuver(argc, argv, &device, &cb, &vram, &verbose) != 0)
    return -1;

  show_dataset_process();

  deviceinit(device, cb);

  /*LECTURA DE FICHEROS*/
  auto start = std::chrono::steady_clock::now();

  //Se lee la nube de puntos
  npoints = read_cloud();

  //Se lee fichero 1: imágenes
  Image *images = read_images();

  //Se lee fichero 2: cámaras por punto / puntos por cámara
  //Skipped in pure gpu version

  //Se lee fichero 3: matriz de transformación (ICP)
  float *transf_matrix;
  transf_matrix = (float*)malloc(4 * 4 * sizeof(float));
  read_transformation_matrix(transf_matrix);
  //Se lee fichero 4: matrices de reflectancia
  //Reflectance_matrix* reflectance_v = read_reflectance_matrix();

  auto end = std::chrono::steady_clock::now();

  std::cout << "Files loaded in " << (std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()) / 1000.0 << " ms" << std::endl;

  printf("Cloud size = %lu (%lu MBytes)\n", npoints, npoints * sizeof(Point) / 1024 / 1024);
  printf("Number of images = %u (960 x 1280 = %ld)\n", num_images, images_resolution);

  get_GPU_memory_footprint(npoints, num_images, vram);

  //Calculo de la matriz de rotación y escala (ICP)
  float* rotation_matrix;
  rotation_matrix = (float*)malloc(3 * 3 * sizeof(float));
  float v_scale[3];
  for (int i = 0; i < 3; i++) {
    float s_index = 0;
    for (int j = 0; j < 3; j++) {
      s_index += transf_matrix[j * 4 + i] * transf_matrix[j * 4 + i];
    }
    v_scale[i] = sqrtf(s_index);
  }
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      rotation_matrix[j * 3 + i] = transf_matrix[j * 4 + i] / v_scale[i];
    }
  }


  /****************GPU*********************/

  // Buffer for all snapshots
  frag *snapshots = (frag *) malloc(num_images * fragimages_size);
  gputime gpu_times(num_images);

  start = std::chrono::steady_clock::now();

  GPU_mapping_and_occlusion(cloud, npoints, images, num_images, rotation_matrix, snapshots, cb, &gpu_times, vram * 1024 * 1024 * 1024);

  end = std::chrono::steady_clock::now();

  gpu_times.kernels_with_transfers = end - start;

  printout_report(&gpu_times, verbose);

  return(0);
}

void printout_report(gputime *gpu_times, bool verbose)
{
  std::cout << "Total Time for Mapping & Occlusion (kernels + transfers): " << (std::chrono::duration_cast<std::chrono::microseconds>(gpu_times->kernels_with_transfers).count()) / 1000.0 << " ms" << std::endl;

  if (verbose) {
    std::cout << std::endl << "Breaking down mapping & occlusion on GPU: per camera" << std::endl;
    std::chrono::duration<double> project(0.0), sort(0.0), reduce(0.0), fill(0.0), transfer(0.0);

    for(int i=0; i<num_images; i++) {
      project += gpu_times->camtime[i].project;
      sort += gpu_times->camtime[i].sort;
      reduce += gpu_times->camtime[i].reduce;
      fill += gpu_times->camtime[i].fill;
      transfer += gpu_times->camtime[i].transfer;

      std::cout << std::setprecision(2) << std::fixed;
      std::cout << "CAM#" << std::setw(2) << i                            \
                << ": Proj " << (std::chrono::duration_cast<std::chrono::microseconds>(gpu_times->camtime[i].project).count()) /1000.0 \
                << "ms \t Sort " << (std::chrono::duration_cast<std::chrono::microseconds>(gpu_times->camtime[i].sort).count()) /1000.0 \
                << "ms \t Redu " << (std::chrono::duration_cast<std::chrono::microseconds>(gpu_times->camtime[i].reduce).count()) /1000.0 \
                << "ms \t Fill " << (std::chrono::duration_cast<std::chrono::microseconds>(gpu_times->camtime[i].fill).count()) /1000.0 \
                << "ms \t GPU->CPU " << (std::chrono::duration_cast<std::chrono::microseconds>(gpu_times->camtime[i].transfer).count()) /1000.0 \
                << " ms" << std::endl;
    }

    std::cout << std::endl << "Aggregate times:" << std::endl;
    std::cout << "-> Proj " << (std::chrono::duration_cast<std::chrono::microseconds>(project).count()) / 1000.0 << " ms" << std::endl \
              << "-> Sort " << (std::chrono::duration_cast<std::chrono::microseconds>(sort).count()) / 1000.0 << " ms" << std::endl \
              << "-> Redu " << (std::chrono::duration_cast<std::chrono::microseconds>(reduce).count()) / 1000.0 << " ms" << std::endl \
              << "-> Fill " << (std::chrono::duration_cast<std::chrono::microseconds>(fill).count()) / 1000.0 << " ms" << std::endl \
              << "-> GPU->CPU " << (std::chrono::duration_cast<std::chrono::microseconds>(transfer).count()) / 1000.0
              << " ms" << std::endl;
  }

#ifdef _SYNCTIME_
  std::cout << "-> Preliminary time (Transfer CPU->GPU & allocations): " << (std::chrono::duration_cast<std::chrono::microseconds>(gpu_times->preliminary).count()) / 1000.0 << " ms" << std::endl;
#endif
}
