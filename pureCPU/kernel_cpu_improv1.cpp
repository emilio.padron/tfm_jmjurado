#include <stdio.h>
#include <chrono>
#include <math.h>
#include <iostream>
#include <assert.h>
#include<string>
#include "rply.h"
#include <fstream>
#include <sstream>
#include <vector>
#include <cstring>
using namespace std;

//const int N = 4651218;  // Tama�o de la nube
//const int N = 744671;
//const int N = 3395368;
const int N = 66374475;

struct spectral_point {
	float *nir;
	float *red;
	float *green;
	float *reg;
};

struct c_p {
	int id_point;
	int id_camara;
};


struct Camera_points {
	int id_camera;
	int *points;
	unsigned int num_points;
};

struct Point_cameras {
	int id_point;
	int *cameras;
	unsigned int num_cameras;
};

struct Point {
	float position[3];
	int r;
	int g;
	int b;
};

struct Image {
	float position[3];
	float rotation_matrix[9];
	int band;
	float A[4];
	float D[4];
	float cx;
	float cy;
};

struct Reflectance_matrix {
	int band;
	float* m;
};

struct Band {
	int band;
	float A[4];
	float D[4];
	float cx;
	float cy;
};

struct Projection {
	float px; //pixelx
	float py; //pixely
	int c; //c�mara sobre la que se proyecta
	int id_point;
	float dist; //distancia del punto a la c�mara
	bool occluded;
};

//Variables globales

Point *cloud;
int i = 0;
int num_images;
int num_projections;


//Leer la nube (PLY)
static int vertex_x(p_ply_argument argument) {
	long eol;
	ply_get_argument_user_data(argument, NULL, &eol);
	//printf("%g ", ply_get_argument_value(argument));
	cloud[i].position[0] = ply_get_argument_value(argument);
	return 1;
}
static int vertex_y(p_ply_argument argument) {
	long eol;
	ply_get_argument_user_data(argument, NULL, &eol);
	//printf("%g ", ply_get_argument_value(argument));
	cloud[i].position[1] = ply_get_argument_value(argument);
	return 1;
}
static int vertex_z(p_ply_argument argument) {
	long eol;
	ply_get_argument_user_data(argument, NULL, &eol);
	//printf("%g ", ply_get_argument_value(argument));
	cloud[i].position[2] = ply_get_argument_value(argument);
	return 1;
}
static int color_r(p_ply_argument argument) {
	long eol;
	ply_get_argument_user_data(argument, NULL, &eol);
	//printf("%g ", ply_get_argument_value(argument));
	cloud[i].r = ply_get_argument_value(argument);
	return 1;
}
static int color_g(p_ply_argument argument) {
	long eol;
	ply_get_argument_user_data(argument, NULL, &eol);
	//printf("%g ", ply_get_argument_value(argument));
	cloud[i].g = ply_get_argument_value(argument);
	return 1;
}
static int color_b(p_ply_argument argument) {
	long eol;
	ply_get_argument_user_data(argument, NULL, &eol);
	//printf("%g \n", ply_get_argument_value(argument));
	cloud[i].b = ply_get_argument_value(argument);
	i++;
	return 1;
}


void mult_matrix(float* result, float* m1, float* m2, int n) {
	float res;
	for (int r = 0; r < n; ++r) {
		for (int c = 0; c < n; ++c) {
			res = 0;
			for (int k = 0; k < n; ++k) {
				res += m1[r * n + k] * m2[k * n + c];
			}
			result[r * n + c] = res;
		}
	}
}

void matrix_trans(float *trans, float *matrix) {

	for (int c = 0; c < 3; c++) {
		for (int d = 0; d < 3; d++) {
			trans[c * 3 + d] = matrix[d * 3 + c];
		}
	}

}

void read_cloud(const int cloud_size) {

	p_ply ply = ply_open("./files/nube.ply", NULL, 0, NULL);
	cloud = (Point*)malloc(cloud_size * sizeof(Point));

	if (ply_read_header(ply)) {

		ply_set_read_cb(ply, "vertex", "x", vertex_x, NULL, 0);
		ply_set_read_cb(ply, "vertex", "y", vertex_y, NULL, 0);
		ply_set_read_cb(ply, "vertex", "z", vertex_z, NULL, 0);
		ply_set_read_cb(ply, "vertex", "red", color_r, NULL, 0);
		ply_set_read_cb(ply, "vertex", "green", color_g, NULL, 0);
		ply_set_read_cb(ply, "vertex", "blue", color_b, NULL, 1);

		if (ply_read(ply)) {
			ply_close(ply);
		}
	}
	printf("Cloud loaded \n");

}

//Leer par�metros de cada imagen (posici�n y matriz de rotaci�n)
Image* read_images() {

	ifstream file("./files/images.txt");
	string line;

	getline(file, line);
	stringstream iss1(line);
	iss1 >> num_images;
	Image* images;
	images = (Image*)malloc(num_images * sizeof(Image));

	int it = 0;
	while (getline(file, line))
	{
		stringstream iss(line);

		Image im;
		iss >> im.position[0] >> im.position[1] >> im.position[2];
		for (int r = 0; r < 3; ++r) {
			getline(file, line);
			stringstream iss2(line);
			for (int c = 0; c < 3; ++c) {
				iss2 >> im.rotation_matrix[r * 3 + c];
			}
		}
		getline(file, line);
		stringstream iss2(line);
		iss2 >> im.band;

		getline(file, line);
		stringstream iss3(line);
		iss3 >> im.D[0] >> im.D[1] >> im.D[2] >> im.D[3];

		getline(file, line);
		stringstream iss4(line);
		iss4 >> im.A[0] >> im.A[1] >> im.A[2] >> im.A[3];

		getline(file, line);
		stringstream iss5(line);
		iss5 >> im.cx >> im.cy;

		images[it] = im;
		++it;
	}
	printf("File 1 loaded \n");
	return images;
}


//Leer camaras asociadas a cada punto 3D
//vector<Point_cameras> read_cameras_point(const int cloud_size) {
//
//	vector<Point_cameras> projs;
//	ifstream file("./files/point_cameras.txt");
//	string line;
//	int it = 0;
//	int cams;
//	while (getline(file, line))
//	{
//		if (line != "") {
//			Point_cameras c_p;
//			stringstream iss(line);
//			iss >> cams;
//			c_p.num_cameras = cams;
//			c_p.cameras = (int*)malloc(cams * sizeof(int));
//			c_p.id_point = it;
//			int token;
//			int k = 0;
//			while (iss >> token) {
//				c_p.cameras[k] = token;
//				++k;
//			}
//			projs.push_back(c_p);
//		}
//		++it;
//	}
//	printf("File 1 loaded \n");
//	return projs;
//}

vector<Point_cameras> read_cameras_point(Camera_points* camera_points,const int cloud_size) {

	vector<Point_cameras> point_cameras;

	FILE *f;
	f = fopen("./files/point_cameras.bin", "rb");
	int n; //n�mero de proyecciones globales
	fread(&n, sizeof(int), 1, f);
	num_projections = 0;
	for (int i = 0; i < n; i++) {
		Point_cameras c_p;

		int id_point;
		fread(&id_point, sizeof(int), 1, f);
		//cout << "idPoint: " << id_point << "\n";
		c_p.id_point = id_point;

		int size;
		fread(&size, sizeof(int), 1, f);
		//cout <<"size: "<< size << "\n";
		c_p.num_cameras = size;
		num_projections += size;
		int *cameras;
		cameras = (int*)malloc(size * sizeof(int));
		fread(cameras, sizeof(int), size, f);
		c_p.cameras = cameras;
		point_cameras.push_back(c_p);
		//for (int j = 0; j < size; j++) {
		//	cout << cameras[j] << " ";
		//}
		//cout << endl;
	}

	//Creamos el vector de camera_points;

	for (int i = 0; i < num_images; i++) {
		vector<int> v;
		for (int j = 0; j < point_cameras.size(); j++) {
			for (int k = 0; k < point_cameras.at(j).num_cameras; k++) {
				if (point_cameras.at(j).cameras[k] == i)
					v.push_back(point_cameras.at(j).id_point);
			}
		}
		Camera_points c_p;
		c_p.num_points = v.size();
		c_p.id_camera = i;
		int *points;
		points = (int*)malloc(v.size() * sizeof(int));
		for (int j = 0; j < v.size(); j++) {
			points[j] = v.at(j);
		}
		c_p.points = points;
		camera_points[i] = c_p;
	}

	printf("File 2 loaded \n");

	return point_cameras;
}




//Leer matriz de transformaci�n (ICP)
void read_transformation_matrix(float *result) {

	float *m1;
	float *m2;
	m1 = (float*)malloc(4 * 4 * sizeof(float));
	m2 = (float*)malloc(4 * 4 * sizeof(float));

	ifstream file("./files/transformation_matrix.txt");
	string line;
	if (file.is_open()) {
		//M1
		int it = 0;
		getline(file, line);
		while (line[0] != '*')
		{
			stringstream linestream(line);
			for (int c = 0; c < 4; c++) {
				linestream >> m1[it * 4 + c];
			}
			getline(file, line);
			++it;
		}

		//M2
		it = 0;
		while (getline(file, line))
		{
			stringstream linestream(line);
			for (int c = 0; c < 4; c++) {
				linestream >> m2[it * 4 + c];
			}
			++it;
		}
		file.close();
	}
	else printf("Unable to open file \n");

	mult_matrix(result, m2, m1, 4);

	printf("File 3 loaded \n");

}

//Leer matrices de reflectancia

Reflectance_matrix* read_reflectance_matrix() {

	Reflectance_matrix* reflec_v;

	FILE *f;
	f = fopen("./files/reflectance.bin", "rb");
	int num_img;
	fread(&num_img, sizeof(int), 1, f);
	reflec_v = (Reflectance_matrix*)malloc(num_img * sizeof(Reflectance_matrix));
	for (int i = 0; i < num_img; i++) {

		Reflectance_matrix reflec;
		int band;
		fread(&band, sizeof(int), 1, f);
		reflec.band = band;

		float* matrix;
		matrix = (float*)malloc(960 * 1280 * sizeof(float));
		fread(matrix, sizeof(float), 960 * 1280, f);
		reflec.m = matrix;
		reflec_v[i] = reflec;
	}
	printf("File 4 loaded \n");
	return reflec_v;
	/*


	ifstream file("./files/reflectance.txt");
	string line;
	getline(file, line);

	stringstream iss1(line);
	iss1 >> num_images;
	reflec_v = (Reflectance_matrix*)malloc(num_images * sizeof(Reflectance_matrix));

	int it = 0;
	while (getline(file, line))
	{
		Reflectance_matrix reflec;
		reflec.m = (float**)malloc(960 * sizeof(float*));
		stringstream iss2(line);
		iss2 >> reflec.band;
		for (int r = 0; r < 960; r++) {
			getline(file, line);
			stringstream iss3(line);
			float token;
			int c = 0;
			reflec.m[r] = (float*)malloc(1280 * sizeof(float));
			while (iss3 >> token) {
				reflec.m[r][c] = token;
				++c;
			}
		}
		getline(file, line);
		reflec_v[it] = reflec;
		++it;
	}
	*/
}

//Leer metadatos en cada banda
void read_metadata(Band* metadata) {

	ifstream file("./files/metadata.txt");
	string line;
	if (file.is_open()) {
		for (int it = 0; it < 4; it++) {
			getline(file, line);
			stringstream iss1(line);
			iss1 >> metadata[it].band;

			getline(file, line);
			stringstream iss2(line);
			iss2 >> metadata[it].A[0] >> metadata[it].A[1] >> metadata[it].A[2] >> metadata[it].A[3];

			getline(file, line);
			stringstream iss3(line);
			iss3 >> metadata[it].D[0] >> metadata[it].D[1] >> metadata[it].D[2] >> metadata[it].D[3];

			getline(file, line);
			stringstream iss4(line);
			iss4 >> metadata[it].cx >> metadata[it].cy;
		}
	}
	printf("File 5 loaded \n");
}


void mapping(Projection** projections, Point *cloud, vector<Point_cameras> point_cameras, Image *images, float *rotation_matrix) {

  float trans_m1[9], trans_m2[9], rot_m[9];
  // trans_m1: matriz transpuesta de la matriz de rotaci�n de la c�mara
  // trans_m2: matriz transpuesta de la matriz de rotaci�n de la transformaci�n ICP

  //Transposici�n de matriz de rotaci�n
  matrix_trans(trans_m2, rotation_matrix);

	int size = point_cameras.size();
	for (int it = 0; it < size; it++) {

		Projection* proj_point;
		unsigned int num_cameras = point_cameras[it].num_cameras;
		proj_point = (Projection*)malloc(num_cameras * sizeof(Projection));

		//printf("IT: %d \n", num_cameras);

		//C�maras para cada punto
		for (int k = 0; k < num_cameras; k++) {

			//Camara visible desde ese punto
			//Id del punto en la nube
			int cam = point_cameras[it].cameras[k];
			int id_point = point_cameras[it].id_point;

			/*****Transformaci�n geom�trica******/

			//Transposici�n de matrices
			matrix_trans(trans_m1, images[cam].rotation_matrix);

			//Multiplicaci�n de matrices
			mult_matrix(rot_m, trans_m1, trans_m2, 3);

			//Traslaci�n y Rotaci�n
			
			float res[3];
			float temp;
			for (int r = 0; r < 3; ++r) {
				temp = 0;
				for (int c = 0; c < 3; ++c) {

					temp += rot_m[r * 3 + c] * (cloud[id_point].position[c] - images[cam].position[c]);
				}
				res[r] = temp;
			}
			//printf("POINT %d, x: %f \n", id_point, cloud[id_point].position[0]); 
			//printf("POINT %d, x: %f \n", id_point, res[0]);

			/*****Proyecci�n*******/

			Projection p;

			//Fisheye Polynomial
			float D[4] = { images[cam].D[0], images[cam].D[1], images[cam].D[2], images[cam].D[3] };
			//Fisheye Affine Matrix
			float A[4] = { images[cam].A[0], images[cam].A[1], images[cam].A[2], images[cam].A[3] };

			float cx = images[cam].cx;
			float cy = images[cam].cy;

			//Punto 3D
			float x = res[0];
			float y = res[1];
			float z = res[2];

			float r = sqrt(pow(x, 2) + pow(y, 2));
			float theta = (2 / M_PI) * atan(r / z);
			float poly = D[0] + D[1] * theta + D[2] * theta * theta + D[3] * theta * theta * theta;
			float xh = (poly * x) / r;
			float yh = (poly * y) / r;

			//Coordenadas de la imagen

			float px = A[2] * xh + A[3] * yh + cy;
			float py = A[0] * xh + A[1] * yh + cx;

			//P�xel en la imagen proyectado por el Punto 3D
			p.px = round(px);
			p.py = round(py);

			p.c = cam;
			p.id_point = id_point;
			float distance = sqrtf(pow((x - images[cam].position[0]), 2) + pow((y - images[cam].position[1]), 2) + pow((z - images[cam].position[2]), 2));
			p.dist = distance;
			proj_point[k] = p;
			if (p.px < 0 || p.px > 960 || p.py < 0 || p.py > 1280) {
				printf("POINT %d -> c: %d, px: %f, py: %f, dist: %f \n", id_point, cam, p.px, p.py, p.dist);
			}

		}
		projections[it] = proj_point;

	}
	//printf("Projection -> x: %f y: %f  \n", projections[0][0].px, projections[0][0].py);

}

void oclussion_test(Projection** projections, vector<Point_cameras> point_cameras) {
	
	for (int it = 0; it < num_images; it++) {

		//Se inicializa la matriz de oclusi�n a 0
		float *m_occlusion;
		int numbytes = 960 * 1280 * sizeof(float);
		m_occlusion = (float*)malloc(numbytes);
		memset(m_occlusion, 0x77, numbytes);

		c_p *indices;
		numbytes = 960 * 1280 * sizeof(c_p);
		indices = (c_p*)malloc(numbytes);
		memset(indices, -1, numbytes);
		int id_point, id_cam;
		for (int i = 0; i < point_cameras.size(); i++) {
			for (int j = 0; j < point_cameras[i].num_cameras; j++) {
				if (projections[i][j].c == it) {
					int px = projections[i][j].px;
					int py = projections[i][j].py;
					float dist = projections[i][j].dist;
					if (dist < m_occlusion[px * 1280 + py]) {
						id_point = indices[px * 1280 + py].id_point;
						//Si hay un punto proyectado sobre ese pixel
						if (id_point != -1) {
							id_cam = indices[px * 1280 + py].id_camara;
							projections[id_point][id_cam].occluded = true;
						}
						indices[px * 1280 + py].id_point = i;
						indices[px * 1280 + py].id_camara = j;
						m_occlusion[px * 1280 + py] = dist;
						projections[i][j].occluded = false;
					}
					else {
						projections[i][j].occluded = true;
					}
				}
			}
		}
	}

	int cont = 0;
	for (int i = 0; i < point_cameras.size(); i++) {
		for (int j = 0; j < point_cameras[i].num_cameras; j++) {
			if (projections[i][j].occluded == true)
				++cont;
	//		printf("ID: %d, CAM: %d, oc: %d , px: %f , py: %f dist: %f \n", projections[i][j].id_point, projections[i][j].c, projections[i][j].occluded, projections[i][j].px, projections[i][j].py, projections[i][j].dist);
		}
	}
	printf("Number of occluded points: %d \n", cont);


}
void reflectance_assignment() {


}


/*
  Funci�n principal
*/
int main(int argc, char* argv[])
{

	// N�mero de elementos en los vectores (predeterminado: N)
	unsigned int n = (argc > 1) ? atoi(argv[1]) : N;

	/*LECTURA DE FICHEROS*/
	auto start = std::chrono::steady_clock::now();

	//Se lee la nube de puntos
	read_cloud(n);

	//Se lee fichero 1: im�genes
	Image *images = read_images();

	//Se lee fichero 2: c�maras por punto / puntos por c�mara

	Camera_points* camera_points;
	camera_points = (Camera_points*)malloc(num_images * sizeof(Camera_points));
	vector<Point_cameras> point_cameras = read_cameras_point(camera_points, n);

	//Se lee fichero 3: matriz de transformaci�n (ICP)
	float *transf_matrix;
	transf_matrix = (float*)malloc(4 * 4 * sizeof(float));
	read_transformation_matrix(transf_matrix);
	//Se lee fichero 4: matrices de reflectancia
	Reflectance_matrix* reflectance_v = read_reflectance_matrix();

	auto end = std::chrono::steady_clock::now();

	std::cout << "Files loaded in " << (std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()) / 1000.0 << " ms" << std::endl;

	printf("Cloud size = %u \n", n);
	printf("Number of images = %u \n", num_images);
	printf("Number of 3D points to be projected: %d \n", point_cameras.size());
	printf("Total of projections to be calculated: %d \n", num_projections);

	//Calculo de la matriz de rotaci�n y escala (ICP)
	float* rotation_matrix;
	rotation_matrix = (float*)malloc(3 * 3 * sizeof(float));
	float v_scale[3];
	for (int i = 0; i < 3; i++) {
		float s_index = 0;
		for (int j = 0; j < 3; j++) {
			s_index += transf_matrix[j * 4 + i] * transf_matrix[j * 4 + i];
		}
		v_scale[i] = sqrtf(s_index);
	}
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			rotation_matrix[j * 3 + i] = transf_matrix[j * 4 + i] / v_scale[i];
		}
	}


	/****************CPU*********************/
	
	Projection** projections;
	projections = (Projection**)malloc(point_cameras.size() * sizeof(Projection*));

	printf("\n----------------CPU---------------- \n");

	start = std::chrono::steady_clock::now();
	mapping(projections, cloud, point_cameras, images, rotation_matrix);
	end = std::chrono::steady_clock::now();

	std::cout << "Time for Mapping: " << (std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()) / 1000.0 << " ms" << std::endl;

	auto start2 = std::chrono::steady_clock::now();
	oclussion_test(projections, point_cameras);
	end = std::chrono::steady_clock::now();

	std::cout << "Time for Occlusion: " << (std::chrono::duration_cast<std::chrono::microseconds>(end - start2).count()) / 1000.0 << " ms" << std::endl;

	std::cout << "Total elapsed time: " << (std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()) / 1000.0 << " ms" << std::endl;

	return(0);

}
