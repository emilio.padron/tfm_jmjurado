Pure cpu sequential versions (removing all cuda stuff)

    kernel_cpu: Juanma's original
    kernel_cpu_improv0: Change dynamic allocation in mapping loop for static arrays
    kernel_cpu_improv1: Put transpose computation for constant rotation_matrix out of the loop
    kernel_cpu_improv2: Precompute transpose rotation matrix for cameras when loaded

See commit 6e6b17161c24f5761e9244fe484bb7ac704eccf7 for details
