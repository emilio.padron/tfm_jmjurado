#include <cstdio>
#include <cfloat>
#include <iostream>
#include <vector>
#include "cpu.h"
#include "gpu_common.h"
#include "gpu_soc.h"

__global__ void GPUmapping_kernel(const unsigned long npoints, Point c_cloud[], const Image *cam_info, frag zbuffer[]);
__global__ void GPUResetSnapshot(frag zbuffer[]);

unsigned int GPU_mapping_and_occlusion(Point *cloud, const unsigned long cloud_points, Image* images, const unsigned int num_images, frag snapshots[], const unsigned int blk_size, gputime *gpu_times, size_t nStreams, const size_t vram)
{
  // Out-of-core algorithm: dataset partition, overlapping computation and transfers
  //
  // lock_mem: size of constant data needed in vram during the whole computation
  //
  // dyn_mem: size of structures that can be split for the out-of-core computation
  //          basically: whole dataset
  //          i.e. data bytes/point x number of points in dataset
  //
  // avail_mem: vram available after considering the lock_mem
  //
  // => avail_mem will be split into nStreams parts: 1 residentpart/CUDAstream
  //    Each part basically has:
  //    + data_set: part of dataset to be tx and processed (npb points/part)
  //    + snapshot: z-buffer with frag info per pixel: dist + point_id
  //                (fragimages_size = images_resolution * sizeof(frag))
  //
  // Summary:
  //
  //   VRAM = lock_mem + avail_mem =
  //          lock_mem + nStreams x (data_set + snapshot) =
  //          lock_mem + nStreams x (npb x databytes/point + fragimages_size)
  //
  // To partition data avail_mem is divided this way:
  //
  //  avail_mem = nStreams x (npb x databytes/point + fragimages_size)
  //
  // so, *npb* is the number of points per partition resident in VRAM,
  // being each resident partition tx/processed by a different CUDA Stream
  // => The idea, again, is to overlap computation and communication
  //
  //   npb = (avail_mem / nStreams - fragimages_size) / databytes/point
  //         [truncate to lower integer this quotient]
  //
  // Hence, dyn_mem is partitioned in parts (aka blocks) with npb points
  // => npb points * databytes/point = npb_size
  //
  // Dataset partition: #parts = cloud_points / npb
  //                    [round to higher integer whis quotient]

#ifdef _SYNCTIME_
  auto start = std::chrono::steady_clock::now();
#endif

  size_t avail_mem = vram - 1024*1024; // Estimation: 1MB of storage permanently needed in VRAM

  // Current implementation: 2 streams
  // nvram region to process current block and write current snapshot
  nStreams = 2;
  size_t npb;
  npb = (avail_mem - fragimages_size*2) / (2*sizeof(Point));

  size_t npb_size = npb * sizeof(Point);
  // (integer division truncates fractional results toward 0)

  size_t partitions = cloud_points / npb + (cloud_points % npb != 0);
  size_t npb_last = cloud_points % npb;
  if (npb_last == 0)
    npb_last = npb;

  Point *d_block[nStreams]; // npb x sizeof(Point)
  frag *snapshot[nStreams], // next snapshot to update/sync
    *h_snapshot[nStreams], // fragimages_size x number of Streams
    *d_snapshot[nStreams]; // fragimages_size

  // Array with streams artifacts
  cudaStream_t streams[nStreams];
  cudaStream_t auxstreams[nStreams];

  for (size_t stream = 0; stream < nStreams; ++stream) {
    cuda( cudaMalloc((void**) &d_block[stream], npb_size) );

    cuda( cudaMalloc((void**) &d_snapshot[stream], fragimages_size) );

    //h_snapshot = (frag *) malloc(nStreams * fragimages_size);
    cuda( cudaMallocHost((void**)&h_snapshot[stream], fragimages_size) );
    cuda( cudaStreamCreate(&streams[stream]) );
    cuda( cudaStreamCreate(&auxstreams[stream]) );
  }

  // CUDA stuff: threads/cuda_blk and grid of cuda_blks
  dim3 dimBlock(blk_size);

  dim3 dimGrid; // 1 thread/point
  dim3 dimGrid2((images_resolution + dimBlock.x - 1) / dimBlock.x);; // 1 thread/pixel

#ifdef _SYNCTIME_
  cuda( cudaDeviceSynchronize() );
  auto end = std::chrono::steady_clock::now();
  gpu_times->preliminary = end - start;
#endif

  cudaEvent_t event_snapshotReady, event_snapshotRcvd;
  //  cudaEventCreate (&event_snapshotReady);
  cudaEventCreateWithFlags(&event_snapshotReady, cudaEventDisableTiming);
  cudaEventCreateWithFlags(&event_snapshotRcvd, cudaEventDisableTiming);

  size_t desp = 0; //iterador para el desplazamiento en la nube
  size_t part = 0; //block currently being managed
  size_t partpoints[nStreams]; // points in block currently being managed
  unsigned int pending = 0;

  for (size_t stream = 0; stream < nStreams; ++stream) {

    partpoints[stream] = (++part < partitions)? npb : npb_last;

    // Copy block to device mem
    cuda( cudaMemcpyAsync(d_block[stream], &cloud[desp], partpoints[stream] * sizeof(Point), cudaMemcpyHostToDevice, streams[stream]) );
    //std::cout << "CPU->GPU<" << stream << "> Blk #" << part-1 << " (" << partpoints[stream] << " points)" << std::endl;

    snapshot[stream] = snapshots; // Reset: first snapshot for first camera!
    ++pending;

    desp += partpoints[stream];
  }

  do {

    for (size_t stream = 0; stream < nStreams; ++stream) {

      if (partpoints[stream] == 0)
        break;

      dimGrid = (partpoints[stream] + dimBlock.x - 1) / dimBlock.x; // 1 thread/point per CUDA BLK

      for (unsigned int c = 0; c < num_images; c++) {

        GPUResetSnapshot<< <dimGrid2, dimBlock, 0, streams[stream]>> > (d_snapshot[stream]);
        GPUmapping_kernel<< <dimGrid, dimBlock, 0, streams[stream]>> > (partpoints[stream], d_block[stream], images + c, d_snapshot[stream]);

        // cuda( cudaPeekAtLastError() );     // only for debugging
        // cuda( cudaDeviceSynchronize() );
        //std::cout << "Kernel<" << stream << "> image #" << c << std::endl;

        cudaEventRecord(event_snapshotReady, streams[stream]);

        // Merge c-1 image
        if (c > 0) {
          cudaEventSynchronize(event_snapshotRcvd);

          frag *partial_snapshot, *final_snapshot;

          partial_snapshot = h_snapshot[stream];
          final_snapshot = snapshot[stream];

          for (size_t j = 0; j < images_resolution; j++) {
            if(partial_snapshot[j].dist < final_snapshot[j].dist){
              final_snapshot[j] = partial_snapshot[j];
            }
          }
          //std::cout << "Merge stream " << stream << " - image #" << c-1 << std::endl;
          snapshot[stream] += images_resolution; // move to snapshot for next camera
        }

        // Transfer snapshot to CPU
        cudaStreamWaitEvent(auxstreams[stream], event_snapshotReady, 0);
        cuda( cudaMemcpyAsync(h_snapshot[stream], d_snapshot[stream], fragimages_size, cudaMemcpyDeviceToHost, auxstreams[stream]) ); // GPU -> CPU
        //std::cout << "GPU->CPU<" << stream << "> image #" << c << " (" << partpoints[stream] << " points)" << std::endl;

        cudaEventRecord(event_snapshotRcvd, auxstreams[stream]);

      }
      --pending;

      // Copy next block for this stream (if needed!)
      if (part < partitions) {
        partpoints[stream] = (++part < partitions)? npb : npb_last;

        cuda( cudaMemcpyAsync(d_block[stream], &cloud[desp], partpoints[stream] * sizeof(Point), cudaMemcpyHostToDevice, streams[stream]) );
        //std::cout << "CPU->GPU<" << stream << "> Blk #" << part-1 << " (" << partpoints[stream] << " points)" << std::endl;

        ++pending;

        desp += partpoints[stream];
      } else { // No more CPU->GPU transfers
        partpoints[stream] = 0;
      }

      // Merge last image from previous block
      cudaEventSynchronize(event_snapshotRcvd);

      // Merge last image from previous block
      frag *partial_snapshot, *final_snapshot;

      partial_snapshot = h_snapshot[stream];
      final_snapshot = snapshot[stream];

      for (size_t j = 0; j < images_resolution; j++) {
        if(partial_snapshot[j].dist < final_snapshot[j].dist){
          final_snapshot[j] = partial_snapshot[j];
        }
      }

      //std::cout << "Merge stream " << stream << " - last image (" << std::endl;

      // Setup stuff for next block!
      snapshot[stream] = snapshots; // Reset: first snapshot for first camera!
    }

  } while (pending);

  for (size_t stream = 0; stream < nStreams; ++stream) {
    cuda( cudaFree(d_block[stream]) );
    cuda( cudaFree(d_snapshot[stream]) );
    cuda( cudaFree(h_snapshot[stream]) );
    cudaStreamDestroy(streams[stream]);
  }

  // Ensure all CUDA stuff has really finished!
  cuda( cudaDeviceSynchronize() );

  return partitions;
}


__device__ void GPU_matrix_trans(float* trans, float* matrix)
{
  for (int c = 0; c < 3; c++) {
    for (int d = 0; d < 3; d++) {
      trans[c * 3 + d] = matrix[d * 3 + c];
    }
  }
}


__device__ void GPU_mult_matrix(float* result, float* m1, float* m2, int n)
{
  float res;
  for (int r = 0; r < n; ++r) {
    for (int c = 0; c < n; ++c) {
      res = 0;
      for (int k = 0; k < n; ++k) {
        res += m1[r * n + k] * m2[k * n + c];
      }
      result[r * n + c] = res;
    }
  }
}


__device__ unsigned long long int my_atomicMin(unsigned long long int* address, float val1, int val2)
{
    frag loc, loctest;
    loc.dist = val1;
    loc.pointid = val2;
    loctest.dist64 = *address;
    while (loctest.dist > val1)
      loctest.dist64 = atomicCAS(address, loctest.dist64, loc.dist64);

    return loctest.dist64;
}


__global__ void GPUmapping_kernel(const unsigned long npoints, Point c_cloud[], const Image *cam_info, frag zbuffer[])
{
  unsigned int task_id = blockIdx.x * blockDim.x + threadIdx.x;

  if (task_id < npoints) {

    //Traslación y Rotación
   float res[3];
    float temp;
    for (int r = 0; r < 3; ++r) {
      temp = 0;
      for (int c = 0; c < 3; ++c) {
        temp += cam_info->rotation_matrix[r * 3 + c] * (c_cloud[task_id].position[c] - cam_info->position[c]);
      }
      res[r] = temp;
    }

    /*****Proyección*******/
    //Fisheye Polynomial
    float D[4] = { cam_info->D[0], cam_info->D[1], cam_info->D[2], cam_info->D[3] };
    //Fisheye Affine Matrix
    float A[4] = { cam_info->A[0], cam_info->A[1], cam_info->A[2], cam_info->A[3] };

    float cx = cam_info->cx;
    float cy = cam_info->cy;

    //Punto 3D
    float x = res[0];
    float y = res[1];
    float z = res[2];

    float r = sqrtf(powf(x, 2) + powf(y, 2));
    float theta = (2 / M_PI) * atanf(r / z);
    float poly = D[0] + D[1] * theta + D[2] * theta * theta + D[3] * theta * theta * theta;
    float xh = (poly * x) / r;
    float yh = (poly * y) / r;

    //Coordenadas de la imagen

    //int px = lrintf(A[2] * xh + A[3] * yh + cy);
    //int py = lrintf(A[0] * xh + A[1] * yh + cx);
    int px = __float2int_rn(A[2] * xh + A[3] * yh + cy);
    int py = __float2int_rn(A[0] * xh + A[1] * yh + cx);

    if (px < 0 || px > 959 || py < 0 || py > 1279)
      return; // Discarded!

    //Pixel en la imagen proyectado por el Punto 3D
    unsigned int fragment_id = px * 1280 + py;

    float dist = sqrtf(powf((x - cam_info->position[0]), 2) + powf((y - cam_info->position[1]), 2) + powf((z - cam_info->position[2]), 2));

    //    frag thisfrag;
    //    thisfrag.dist = dist;
    //    thisfrag.pointid = task_id;
    //    atomicMin(&zbuffer[fragment_id].dist64, thisfrag.dist64);

    // 32 bits float atomics (no 64-bit atomics in CC < 3.5)
    my_atomicMin(&zbuffer[fragment_id].dist64, dist, task_id);
  }

}


__global__ void GPUResetSnapshot(frag zbuffer[])
{
  unsigned int task_id = blockIdx.x * blockDim.x + threadIdx.x;

  if (task_id < 1228800) {
    zbuffer[task_id].pointid = UINT_MAX;
    zbuffer[task_id].dist = FLT_MAX;
  }
}


__global__ void GPUgenerate_snapshot(const int * const nelements, unsigned int src_pixelid[], frag src_value[], frag dst_value[])
{
  const int task_id = blockIdx.x * blockDim.x + threadIdx.x;

  if (task_id < *nelements - 1) {
    unsigned int pixel_id = src_pixelid[task_id];

    //    For some debugging:
    //    printf("Task_id: %d - pixel_id %d - point %d - dist %f\n", task_id, pixel_id, src_value[task_id].pointid, src_value[task_id].dist);

    dst_value[pixel_id].dist = src_value[task_id].dist;
    dst_value[pixel_id].pointid = src_value[task_id].pointid;
  }
}
