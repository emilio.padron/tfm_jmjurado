#ifndef _gpu_h
#define _gpu_h

#include <cuda_runtime.h>
#include "cpu.h"

__global__ void GPUmapping_kernel(const unsigned long npoints, glm::vec3 c_cloud[], Image_glm cam_info[], frag zbuffer[]);
__global__ void GPUResetSnapshot(frag zbuffer[]);__global__ void GPUResetSnapshot(unsigned int nfrags, frag zbuffer[]);
__global__ void GPUFinalSnapshot(unsigned int nfrags, frag zbuffer[], unsigned int finalbuf[]);
int parse_args_socver(int argc, char *argv[], unsigned int *cb, unsigned int *batch, long npoints);

#endif
