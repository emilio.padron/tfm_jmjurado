#include <cstdio>
#include <cub/cub.cuh>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "cpu.h"
#include "gpu_common.h"
#include "gpu_v32.h"

// CustomMin functor for reduction
struct CustomMin {
  CUB_RUNTIME_FUNCTION __forceinline__
  __device__ frag operator()(const frag &a, const frag &b) const {
    return (b.dist < a.dist) ? b : a;
  }
};

unsigned int GPU_mapping_and_occlusion(Point* cloud, const unsigned long cloud_size, Image* images, const unsigned int num_images, float* v_rot_matrix, frag snapshots[], const unsigned int blk_size, gputime *gpu_times, const size_t vram)
{
#ifdef _SYNCTIME_
  auto start = std::chrono::steady_clock::now();
#endif

  size_t dynamic_memory = cloud_size * sizeof(Point);
  size_t lock_memory = 1024 * 1024; // Estimation: 1MB for cub's sort&reduce temp storage
  size_t gpu_memory = vram;

  // CPU->GPU transfer of the complete info about the cameras
  Image *d_images;
  size_t numBytes = num_images * sizeof(Image);
  cudaMalloc((void**)&d_images, numBytes);
  cudaMemcpy(d_images, images, numBytes, cudaMemcpyHostToDevice);
  lock_memory += numBytes;

  // CPU->GPU transfer of the rotation matrix
  float* d_rotation_matrix;
  numBytes = 3 * 3 * sizeof(float);
  cudaMalloc((void**)&d_rotation_matrix, numBytes);
  cudaMemcpy(d_rotation_matrix, v_rot_matrix, numBytes, cudaMemcpyHostToDevice);
  lock_memory += numBytes;

  //La memoria libre de la GPU se divide en 2 bloques
  //Los tamaños de estos dos bloque no tienen que ser los mismos, si la memoria libre no es divisible por 2
  const int memory_blocks = 2;

  // Fragments projected for each camera
  unsigned int *d_frag_key[memory_blocks]; // frag_key = pixelid = pixelx * 1280 + pixely
  frag *d_frag_value[memory_blocks]; // (z value, point id): distancia del punto a la cámara, id punto

  // Extra allocation to sort-by-key previous arrays
  unsigned int *d_frag_key_extra[memory_blocks];
  frag *d_frag_value_extra[memory_blocks];

  dynamic_memory +=
    + 2 * cloud_size * sizeof(unsigned int) // d_frag_key + d_frag_key_extra
    + 2 * cloud_size * sizeof(frag); //d_frag_value + d_frag_value_extra

  //Cálculo del número de particiones y del tamaño de cada partición:
  //dynamic_mem: Memoria necesaria para procesar la nube de datos completa
  //lock_mem: Memoria que necesitamos no relacionada con tamaño de la nube:
  //gpu_mem: Memoria disponible en la GPU para procesar la nube, descontando la lock_mem
  gpu_memory -= lock_memory;

  std::cout << "Locked mem: " << lock_memory / 1024.0f << " KB. Free mem: " << gpu_memory / 1024 / 1024 << " MB. Required mem: " << dynamic_memory / 1024.0f / 1024.0f << " MB." << std::endl;

  int partitions = 1;
  if (dynamic_memory > gpu_memory) {
    partitions += dynamic_memory / gpu_memory;
  }

  size_t size = 1 + cloud_size / partitions; //tamaño del vector que cabe en la memoria de la GPU para cada iteración

  std::cout << "Number of point cloud partitions: " << partitions
            << " (" << size << " Bytes/partition)" << std::endl;

  //Bloques de la nube
  Point *c_blocks[memory_blocks]; //GPU
  int block_size = size / memory_blocks; // tamaño del bloque 2

  //Inicialización
  for (int k = 0; k < memory_blocks; ++k) {
    cudaMalloc((void**) &d_frag_key[k], block_size * sizeof(unsigned int));
    cudaMalloc((void**) &d_frag_key_extra[k], block_size * sizeof(unsigned int));
    cudaMalloc((void**) &d_frag_value[k], block_size * sizeof(frag));
    cudaMalloc((void**) &d_frag_value_extra[k], block_size * sizeof(frag));
    cudaMalloc((void**) &c_blocks[k], block_size * sizeof(Point));
  }

  // Temporary storage required for sorting and reducing
  void *d_temp_storage = NULL;
  size_t temp_storage_bytes = 0, needed_temp_bytes = 0;

  int *d_num_runs_out;
  cudaMalloc((void**) &d_num_runs_out, sizeof(int));

  CustomMin reduction_op; // reduction operation: min value

  //Dimension de hilos por bloque
  dim3 dimBlock(blk_size);

  //Dimensión del GRID
  dim3 dimGrid; // 1 thread/point_in_blk
  dim3 dimGrid2((images_resolution + dimBlock.x - 1) / dimBlock.x);; // 1 thread/pixel

  const int nStreams = memory_blocks;

  // snapshot for current camera
  frag *snapshot = snapshots,
    //Estructura temporal para chequear los fragmentos válidos en cada cámara
    *temp_snapshot;
  temp_snapshot = (frag *) malloc(fragimages_size);

#ifdef _SYNCTIME_
  cudaDeviceSynchronize();
  auto end = std::chrono::steady_clock::now();
  gpu_times->preliminary = end - start;
#endif

  size_t desp; //iterador para el desplazamiento en la nube

  for (unsigned int c = 0; c < num_images; c++) {
#ifdef _SYNCTIME_
    auto start_it = std::chrono::steady_clock::now();
#endif
    numBytes = size; // numBytes is now number of points in current partition
    desp = 0;

    //Se inicializa el snapshot de la cámara c con un valor alto de distancia
    memset(snapshot, 0x77, fragimages_size);

    for (unsigned int i = 0; i < partitions; i++) {

      //Si es la última partición
      if (i == partitions - 1 && partitions > 1) {
        numBytes = cloud_size - desp;
        block_size = numBytes / memory_blocks; // tamaño del bloque 2
      }

      //Se crean 2 streams
      cudaStream_t stream[nStreams];
      cudaStreamCreate(&stream[0]);
      cudaStreamCreate(&stream[1]);

      for (int k = 0; k < nStreams; ++k) {

        //Tranferencia del bloque a la GPU
        cudaMemcpyAsync(c_blocks[k], &cloud[desp], block_size, cudaMemcpyHostToDevice, stream[k]);
        desp += block_size;

        //Llamada al kernel

        // First stage for camera #c: mapping (aka projection)
        // Get fragments for each projected point in c_projections
        // Multiple 3D points can be projected to the same fragment (with different z-component, dist)

        dimGrid = (block_size + blk_size - 1) / blk_size;
        GPUmapping_kernel << <dimGrid, dimBlock, 0, stream[k]>> > (block_size, c, c_blocks[k], d_images, d_rotation_matrix, d_frag_key[k], d_frag_value[k]);

        // Create a set of DoubleBuffers to wrap pairs of device pointers
        // to sort-by-key the arrays (pixelid is the key)
        cub::DoubleBuffer<unsigned int> d_keys(d_frag_key[k], d_frag_key_extra[k]);
        cub::DoubleBuffer<frag> d_values(d_frag_value[k], d_frag_value_extra[k]);

        // Second stage for camera #c: check occlusion and update general zbuffer
        // Fragments for the same pixel are reduced by distance to camera

        // First step: sort-by-key (pixelid is the key)

        // Determine temporary device storage requirements
        cub::DeviceRadixSort::SortPairs(NULL, needed_temp_bytes, d_keys, d_values, block_size);

        // Allocate temporary storage
        if (needed_temp_bytes > temp_storage_bytes) {
          cudaFree(d_temp_storage);
          temp_storage_bytes = needed_temp_bytes;
          cudaMalloc(&d_temp_storage, temp_storage_bytes);
        }

        // Run sorting operation
        cub::DeviceRadixSort::SortPairs(d_temp_storage, temp_storage_bytes, d_keys, d_values, block_size);

        // Second step: reduce-by-key

        // Determine temporary device storage requirements
        cub::DeviceReduce::ReduceByKey(NULL, needed_temp_bytes, d_frag_key[k], d_frag_key_extra[k], d_frag_value[k], d_frag_value_extra[k], d_num_runs_out, reduction_op, block_size);

        // Allocate temporary storage
        if (needed_temp_bytes > temp_storage_bytes) {
          cudaFree(d_temp_storage);
          temp_storage_bytes = needed_temp_bytes;
          cudaMalloc(&d_temp_storage, temp_storage_bytes);
        }

        // Run reduce-by-key
        cub::DeviceReduce::ReduceByKey(d_temp_storage, temp_storage_bytes, d_frag_key[k], d_frag_key_extra[k], d_frag_value[k], d_frag_value_extra[k], d_num_runs_out, reduction_op, block_size);

        // Third stage for camera #c: copy pixels to final positions
        // filling the missing keys with 0 (pixels not covered by any fragment)
        // Every CUDA thread copies a pixel

        // Initialize destination buffer to UINT_MAX
        cudaMemset(d_frag_value[k], UINT_MAX, fragimages_size);

        // Kernel to copy each pixel info into its final location
        // generating the final 960x1280 snapshot with dist+pointid per pixel
        GPUgenerate_snapshot << <dimGrid2, dimBlock >> > (d_num_runs_out, d_frag_key_extra[k], d_frag_value_extra[k] /*src*/, d_frag_value[k] /*dst*/);

        // Transfer snapshot to CPU
        cudaMemcpy(temp_snapshot, d_frag_value[k], fragimages_size, cudaMemcpyDeviceToHost); // GPU -> CPU

        //Merge
        for (size_t j = 0; j < images_resolution; j++) {
          if(temp_snapshot[j].dist < snapshot[j].dist){
            snapshot[j] = temp_snapshot[j];
          }
        }

      }

      //Destrucción de los streams
      cudaStreamDestroy(stream[0]);
      cudaStreamDestroy(stream[1]);

    }

    snapshot += images_resolution; // move to snapshot for next camera

#ifdef _SYNCTIME_
    cudaDeviceSynchronize();
    auto end_it = std::chrono::steady_clock::now();

    gpu_times->camtime[c].allcomputation += (end_it - start_it);
#endif
  }

  free(temp_snapshot);
  cudaFree(d_temp_storage);
  cudaFree(d_num_runs_out);
  cudaFree(d_frag_value_extra);
  cudaFree(d_frag_value);
  cudaFree(d_frag_key_extra);
  cudaFree(d_frag_key);
  cudaFree(d_rotation_matrix);
  cudaFree(d_images);
  cudaFree(c_blocks);

  // Ensure all CUDA stuff has really finished!
  cudaDeviceSynchronize();

  return partitions;
}


__device__ void GPU_matrix_trans(float* trans, float* matrix)
{
  for (int c = 0; c < 3; c++) {
    for (int d = 0; d < 3; d++) {
      trans[c * 3 + d] = matrix[d * 3 + c];
    }
  }
}


__device__ void GPU_mult_matrix(float* result, float* m1, float* m2, int n)
{
  float res;
  for (int r = 0; r < n; ++r) {
    for (int c = 0; c < n; ++c) {
      res = 0;
      for (int k = 0; k < n; ++k) {
        res += m1[r * n + k] * m2[k * n + c];
      }
      result[r * n + c] = res;
    }
  }
}


__global__ void GPUmapping_kernel(const unsigned long npoints, const unsigned int cam_id, Point c_cloud[], Image c_images[], float c_rotation_matrix[], unsigned int frag_pixelid[], frag frag[])
{
  int task_id = blockIdx.x * blockDim.x + threadIdx.x;

  if (task_id < npoints) {

    /*****Transformación geométrica******/

    float trans_m1[9];//matriz transpuesta de la matriz de rotación de la cámara
    float trans_m2[9];//matriz transpuesta de la matriz de rotación de la transformación ICP
    float rot_m[9];

    //Transposición de matrices
    GPU_matrix_trans(trans_m1, c_images[cam_id].rotation_matrix);
    GPU_matrix_trans(trans_m2, c_rotation_matrix);

    //Multiplicación de matrices
    GPU_mult_matrix(rot_m, trans_m1, trans_m2, 3);

    //Traslación y Rotación
    float res[3];
    float temp;
    for (int r = 0; r < 3; ++r) {
      temp = 0;
      for (int c = 0; c < 3; ++c) {
        temp += rot_m[r * 3 + c] * (c_cloud[task_id].position[c] - c_images[cam_id].position[c]);
      }
      res[r] = temp;
    }

    /*****Proyección*******/
    //Fisheye Polynomial
    float D[4] = { c_images[cam_id].D[0], c_images[cam_id].D[1], c_images[cam_id].D[2], c_images[cam_id].D[3] };
    //Fisheye Affine Matrix
    float A[4] = { c_images[cam_id].A[0], c_images[cam_id].A[1], c_images[cam_id].A[2], c_images[cam_id].A[3] };

    float cx = c_images[cam_id].cx;
    float cy = c_images[cam_id].cy;

    //Punto 3D
    float x = res[0];
    float y = res[1];
    float z = res[2];

    float r = sqrt(pow(x, 2) + pow(y, 2));
    float theta = (2 / M_PI) * atan(r / z);
    float poly = D[0] + D[1] * theta + D[2] * theta * theta + D[3] * theta * theta * theta;
    float xh = (poly * x) / r;
    float yh = (poly * y) / r;

    //Coordenadas de la imagen

    int px = lrintf(A[2] * xh + A[3] * yh + cy);
    int py = lrintf(A[0] * xh + A[1] * yh + cx);

    if (px < 0 || px > 959 || py < 0 || py > 1279) {
      frag_pixelid[task_id] = UINT_MAX;
    } else {
      //Pixel en la imagen proyectado por el Punto 3D
      frag_pixelid[task_id] = lrintf(px) * 1280 + lrintf(py);

      frag[task_id].dist = sqrtf(pow((x - c_images[cam_id].position[0]), 2) + pow((y - c_images[cam_id].position[1]), 2) + pow((z - c_images[cam_id].position[2]), 2));
      frag[task_id].pointid = task_id;
    }

  }
}


__global__ void GPUgenerate_snapshot(const int * const nelements, unsigned int src_pixelid[], frag src_value[], frag dst_value[])
{
  const int task_id = blockIdx.x * blockDim.x + threadIdx.x;

  if (task_id < *nelements - 1) {
    unsigned int pixel_id = src_pixelid[task_id];

    dst_value[pixel_id].dist = src_value[task_id].dist;
    dst_value[pixel_id].pointid = src_value[task_id].pointid;
  }
}
