all: cpu omp gpu gpu_v31 gpu_v31b gpu_v31c gpu_v32 gpu_v32c gpu_v32d gpu_v32e gpu_v32f gpu_v32g gpu_v32h gpu_v32i gpu_v42 gpu_v42b gpu_v42c gpu_soc gpu_soc_naive gpu_soc_noUM gpu_v42_laptop gpu_v43
allall: cpu omp gpu gpu_v31 gpu_v31b gpu_v31c gpu_v32d gpu_v32e gpu_v32f gpu_v32g gpu_v32h gpu_v32i gpu_v42 gpu_v42b gpu_syti gpu_v31_syti gpu_v31b_syti gpu_v31c_syti gpu_v32_syti

# Debugging
#CFLAGS=-O0 -g
#NVFLAGS=-O0 -g -G # -I/opt/cub-1.8.0

CC=gcc-7
CXX=g++-7
CFLAGS=-Wall -Wextra -march=native -Ofast -I/usr/local/cuda/targets/aarch64-linux/include
NVFLAGS=-gencode=arch=compute_53,code=sm_53 -O3 -Xcompiler -march=native -Xcompiler -Wall -Xcompiler -Wextra

# More Debugging
#NVFLAGS+=-D_DEBUG_
#CFLAGS+=-D_DEBUG_

#NVFLAGS+=-D_NOTONLYVERT_
#CFLAGS+=-D_NOTONLYVERT_

cpu: cpu.o main_cpu.o rply.o
	$(CXX) -o $@ $^

omp: cpu_omp.o main_cpu.o rply.o
	$(CXX) -o $@ -fopenmp $^

gpu: gpu_common.o gpu.o cpu.o main_gpu.o rply.o
	nvcc -o $@ $^

gpu_v31: gpu_common.o gpu_v31.o cpu.o main_gpu_v31.o rply.o
	nvcc -o $@ $^

gpu_v31b: gpu_common.o gpu_v31b.o cpu.o main_gpu_v31.o rply.o
	nvcc -o $@ $^

gpu_v31c: gpu_common.o gpu_v31c.o cpu.o main_gpu_v31.o rply.o
	nvcc -o $@ $^

gpu_v32: gpu_common.o gpu_v32.o cpu.o main_gpu_v32.o rply.o
	nvcc -o $@ $^

gpu_v32c: gpu_common.o gpu_v32c.o cpu.o main_gpu_v32.o rply.o
	nvcc -o $@ $^

gpu_v32d: gpu_common.o gpu_v32d.o cpu.o main_gpu_v32.o rply.o
	nvcc -o $@ $^

gpu_v32e: gpu_common.o gpu_v32e.o cpu.o main_gpu_v32.o rply.o
	nvcc -o $@ $^

gpu_v32f: gpu_common.o gpu_v32f.o cpu.o main_gpu_v32.o rply.o
	nvcc -o $@ $^

gpu_v32g: gpu_common.o gpu_v32g.o cpu.o main_gpu_v32.o rply.o
	nvcc -o $@ $^

gpu_v32h: gpu_common.o gpu_v32h.o cpu.o main_gpu_v32.o rply.o
	nvcc -o $@ $^

gpu_v32i: gpu_common.o gpu_v32i.o cpu.o main_gpu_v32.o rply.o
	nvcc -o $@ $^

gpu_v42: gpu_common.o gpu_v42.o cpu.o main_gpu_v42.o rply.o
	nvcc -o $@ $^

gpu_v42b: gpu_common.o gpu_v42b.o cpu.o main_gpu_v42b.o rply.o
	nvcc -o $@ $^

gpu_v42c: gpu_common.o gpu_v42c.o cpu.o main_gpu_v42c.o rply.o
	nvcc -o $@ $^

gpu_v43: gpu_common.o gpu_v43.o cpu.o main_gpu_v42c.o rply.o
	nvcc -o $@ $^

gpu_soc: gpu_common.o gpu_soc.o cpu.o main_gpu_soc.o rply.o
	nvcc -o $@ $^

gpu_soc_naive: gpu_common.o gpu_soc_naive.o cpu.o main_gpu_soc_naive.o rply.o
	nvcc -o $@ $^

gpu_soc_noUM: gpu_common.o gpu_soc_noUM.o cpu.o main_gpu_soc_noUM.o rply.o
	nvcc -o $@ $^

gpu_v42_laptop: gpu_common.o gpu_v42_laptop.o cpu.o main_gpu_v42_laptop.o rply.o
	nvcc -o $@ $^

# SYNC TIME versions (sync barriers to measure detailed times)
gpu_syti: gpu_common.o gpu_syti.o cpu.o main_gpu_syti.o rply.o
	nvcc -o $@ $^

gpu_v31_syti: gpu_common.o gpu_v31_syti.o cpu.o main_gpu_v31_syti.o rply.o
	nvcc -o $@ $^

gpu_v31b_syti: gpu_common.o gpu_v31b_syti.o cpu.o main_gpu_v31_syti.o rply.o
	nvcc -o $@ $^

gpu_v31c_syti: gpu_common.o gpu_v31c_syti.o cpu.o main_gpu_v31_syti.o rply.o
	nvcc -o $@ $^

gpu_v32_syti: gpu_common.o gpu_v32_syti.o cpu.o main_gpu_v32_syti.o rply.o
	nvcc -o $@ $^

%.o: %.c
	$(CC) $(CFLAGS) $< -c

%.o: %.cpp
	$(CXX) $(CFLAGS) $< -c

%_omp.o: %.cpp
	$(CXX) $(CFLAGS) -fopenmp $< -c -o $@

%.o: %.cu
	nvcc $(NVFLAGS) $< -c

# Compilation with SYNC TIME
%_syti.o: %.c
	$(CC) $(CFLAGS) -D_SYNCTIME_ $< -c -o $@

%_syti.o: %.cpp
	$(CXX) $(CFLAGS) -D_SYNCTIME_ $< -c -o $@

%_syti.o: %.cu
	nvcc $(NVFLAGS) -D_SYNCTIME_ $< -c -o $@

clean: 
	rm -f *.o

cleanall:
	make clean
	rm -f cpu omp gpu gpu_v31 gpu_v31b gpu_v31c gpu_v32 gpu_v32c gpu_v32d gpu_v32e gpu_v32f gpu_v32g gpu_v32h gpu_v32i gpu_v42 gpu_v42b gpu_v42c gpu_syti gpu_v31_syti gpu_v31b_syti gpu_v31c_syti gpu_v32_syti gpu_soc gpu_soc_naive gpu_soc_noUM gpu_v42_laptop gpu_v43
	rm -f *~
