#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <chrono>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "cpu.h"
#include "rply.h"
#include "gpu_common.h"
#include "gpu_soc_naive.h"
using namespace std;

/*
  Función principal
*/
int main(int argc, char* argv[])
{
  unsigned int batch;
  long npoints;    // Points in dataset
  unsigned int cb; // CUDA Threads/Block

  show_invocation(argc, argv);

  if (parse_args_socver(argc, argv, &cb, &batch, &npoints) != 0)
    return -1;

  show_dataset_process();

  deviceinit(0, cb);

  size_t avail_phy_vidmem = 0, total_phy_vidmem = 0;
  cuda( cudaMemGetInfo(&avail_phy_vidmem, &total_phy_vidmem) );
  std::cout << std::endl
            << avail_phy_vidmem / 1024.0f / 1024.0f / 1024.0f << " GB of "
            << total_phy_vidmem / 1024.0f / 1024.0f / 1024.0f << " GB available before loading stuff"
            << std::endl;

  /*LECTURA DE FICHEROS*/
  auto start = std::chrono::steady_clock::now();

  //Se lee la nube de puntos: device mem or UM
  npoints = read_cloud_UM(npoints);

  if (npoints == 0)
    return -1;

  //Se lee fichero 1: imágenes => RAM
  Image_glm *images = read_images_glm();

  //Se lee fichero 2: matriz de transformación (ICP)
  float transf_matrix[16];
  read_transformation_matrix(transf_matrix);

  auto end = std::chrono::steady_clock::now();

  std::cout << std::endl
            << "Files loaded in "
            << (std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()) / 1000.0
            << " ms" << std::endl;

  printf("\nCloud size: %lu points (%lu MBytes)\n", npoints, npoints * sizeof(Point) / 1024 / 1024);
  printf("Number of images: %u (960 x 1280 = %ld frag/screenshot => %ld x %ld B/frag = %ld B/snapshot)\n", num_images, images_resolution, images_resolution, sizeof(frag), images_resolution * sizeof(frag));

  start = std::chrono::steady_clock::now();

  //Calculo de la matriz de rotación y escala (ICP)
  float rotation_matrix[9];
  float v_scale[3];
  for (int i = 0; i < 3; i++) {
    float s_index = 0;
    for (int j = 0; j < 3; j++) {
      s_index += transf_matrix[j * 4 + i] * transf_matrix[j * 4 + i];
    }
    v_scale[i] = sqrtf(s_index);
  }
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      rotation_matrix[j * 3 + i] = transf_matrix[j * 4 + i] / v_scale[i];
    }
  }

  mat3 multispectralMatrix = glm::transpose(glm::make_mat3(rotation_matrix));

  end = std::chrono::steady_clock::now();

  std::cout << std::endl
            << "Multispectral Matrix computed in "
            << (std::chrono::duration_cast<std::chrono::microseconds>(end - start).count())
            << " us" << std::endl;

  cuda( cudaMemGetInfo(&avail_phy_vidmem, &total_phy_vidmem) );
  std::cout << std::endl
            << avail_phy_vidmem / 1024.0f / 1024.0f / 1024.0f << " GB of "
            << total_phy_vidmem / 1024.0f / 1024.0f / 1024.0f << " GB available before loading stuff"
            << std::endl;

  // Allocate UM space for working with batches of camera info <-> snapshot:
  //   CPU provides a batch of cameras
  //   GPU returns the snapthos with pixel<->point mapping
  Image_glm *cameras;
  cudaError_t status = cudaMallocManaged((void**)&cameras, batch * sizeof(Image_glm));
  if (status != cudaSuccess) {
    printf("Error allocating unified memory for cameras!\n");
    printf("Cameras buffer not allocated! :-/");

    return -1;
  }
  frag *snapshots;
  status = cudaMallocManaged((void**)&snapshots, batch * fragimages_size);
  if (status != cudaSuccess) {
    printf("Error allocating unified memory for snapshots!\n");
    printf("Snapshots buffer not allocated! :-/");

    return -1;
  }
  unsigned int *mapping;
  status = cudaMallocManaged((void**)&mapping, batch * images_resolution * sizeof(unsigned int));
  if (status != cudaSuccess) {
    printf("Error allocating unified memory for final mapping!\n");
    printf("Mapping buffer not allocated! :-/");

    return -1;
  }

  cuda( cudaMemGetInfo(&avail_phy_vidmem, &total_phy_vidmem) );
  std::cout << std::endl
            << avail_phy_vidmem / 1024.0f / 1024.0f / 1024.0f << " GB of "
            << total_phy_vidmem / 1024.0f / 1024.0f / 1024.0f << " GB available before allocating batch buffers"
            << " (batch size: " << batch << ")" << std::endl;

  // CUDA stuff: threads/cuda_blk and grid of cuda_blks
  dim3 dimBlock(cb);
  dim3 dimGrid = (npoints + dimBlock.x - 1) / dimBlock.x; // 1 thread/point
  dim3 dimGrid2((batch*images_resolution + dimBlock.x - 1) / dimBlock.x);; // 1 thread/pixel in batch (for reseting frags)

  for (int i = 0; i < num_images; i+=batch) {

    int top;
    top = i + batch;
    if (top > num_images) {
      top = num_images;
      batch = num_images - i;
    }

    start = std::chrono::steady_clock::now();

    for (int b = i; b < top; b++) {
      // Composited Matrix is stored instead of Rotation Matrix for each camera
      images[b].rotationMatrix = glm::transpose(images[b].rotationMatrix) * glm::transpose(multispectralMatrix);
    }

    cuda( cudaMemcpy(cameras, &images[i], batch * sizeof(Image_glm), cudaMemcpyHostToDevice) );

    GPUResetSnapshot<< <dimGrid2, dimBlock >> > (batch*images_resolution, snapshots);

    for (int b = i; b < top; b++) {
      GPUmapping_kernel<< <dimGrid, dimBlock >> > (npoints, (glm::vec3 *) cloud, cameras + b, snapshots + b);
    }

    GPUFinalSnapshot<< <dimGrid2, dimBlock >> > (batch*images_resolution, snapshots, mapping);

    //cuda( cudaPeekAtLastError() );     // only for debugging

    cuda( cudaDeviceSynchronize() );
    end = std::chrono::steady_clock::now();

    std::cout << std::endl
              << "Batch #" << i << " processed in "
              << (std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()) / 1000.0
              << " ms" << std::endl;
  }

  cuda( cudaFree(mapping) );
  cuda( cudaFree(snapshots) );
  cuda( cudaFree(cameras) );
  cuda( cudaFree(cloud) );
  free(images);

  return(0);
}


long read_cloud_UM(const long howmany)
{
  static p_ply ply;
  long cloud_size;

  ply = ply_open((dataset_path + pointcloud_file).c_str(), NULL, 0, NULL);

  if (ply_read_header(ply)) {
    ply_get_element_info(ply_get_next_element(ply, 0), 0, &cloud_size);

    if (howmany > 0 && howmany < cloud_size) {
      // load only the desired number of points
      cloud_size = howmany;
      ply_setninstances(ply_get_next_element(ply, 0), howmany);
    }

    // Unified memory allocated for INPUT
    cudaError_t status = cudaMallocManaged((void**)&cloud, cloud_size * sizeof(glm::vec3), cudaMemAttachHost);
    if (status != cudaSuccess) {
      printf("Error allocating unified memory for point cloud!\n");
      printf("Cloud not loaded! :-/");

      return 0;
    }

    ply_set_read_cb(ply, "vertex", "x", vertex_x, NULL, 0);
    ply_set_read_cb(ply, "vertex", "y", vertex_y, NULL, 0);
#ifdef _NOTONLYVERT_
    ply_set_read_cb(ply, "vertex", "z", vertex_z, NULL, 0);
    ply_set_read_cb(ply, "vertex", "red", color_r, NULL, 0);
    ply_set_read_cb(ply, "vertex", "green", color_g, NULL, 0);
    ply_set_read_cb(ply, "vertex", "blue", color_b, NULL, 1);
#else
    ply_set_read_cb(ply, "vertex", "z", vertex_z, NULL, 1);
#endif
  }

  if (ply_read(ply)) {
    ply_close(ply);
  }

  printf("Cloud loaded: %ld points\n", cloud_size);

  return cloud_size;
}
