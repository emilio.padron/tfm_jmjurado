#include <cstdio>
#include <cmath>
#include <cstring>
#include <unistd.h>
#include <fstream>
#include <sstream>
#include <iostream>
#include <vector>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include "rply.h"
#include "cpu.h"
using namespace std;

//Variables globales
Point *cloud;
int currentp = 0;
const long batchsize = 90000000; // default: 90M points/batch
int num_images;
int num_projections;

//Consts
const size_t images_resolution = 960 * 1280;
const size_t fragimages_size = images_resolution * sizeof(frag);

//Dataset files
std::string dataset_path, pointcloud_file, images_file, matrix_file,
  mapping_file, reflectance_file;

//Leer la nube (PLY)
int vertex_x(p_ply_argument argument)
{
  long eol;
  ply_get_argument_user_data(argument, NULL, &eol);
  //printf("%g ", ply_get_argument_value(argument));
  cloud[currentp].position[0] = ply_get_argument_value(argument);
  return 1;
}


int vertex_y(p_ply_argument argument)
{
  long eol;
  ply_get_argument_user_data(argument, NULL, &eol);
  //printf("%g ", ply_get_argument_value(argument));
  cloud[currentp].position[1] = ply_get_argument_value(argument);
  return 1;
}


int vertex_z(p_ply_argument argument)
{
  long eol;
  ply_get_argument_user_data(argument, NULL, &eol);
  //printf("%g ", ply_get_argument_value(argument));
  cloud[currentp].position[2] = ply_get_argument_value(argument);
  return 1;
}


#ifdef _NOTONLYVERT_
int color_r(p_ply_argument argument)
{
  long eol;
  ply_get_argument_user_data(argument, NULL, &eol);
  //printf("%g ", ply_get_argument_value(argument));
  cloud[currentp].r = ply_get_argument_value(argument);
  return 1;
}


int color_g(p_ply_argument argument)
{
  long eol;
  ply_get_argument_user_data(argument, NULL, &eol);
  //printf("%g ", ply_get_argument_value(argument));
  cloud[currentp].g = ply_get_argument_value(argument);
  return 1;
}


int color_b(p_ply_argument argument)
{
  long eol;
  ply_get_argument_user_data(argument, NULL, &eol);
  //printf("%g \n", ply_get_argument_value(argument));
  cloud[currentp].b = ply_get_argument_value(argument);
  i++;
  return 1;
}
#endif //_NOTONLYVERT_


void mult_matrix(float* result, float* m1, float* m2, int n)
{
  float res;
  for (int r = 0; r < n; ++r) {
    for (int c = 0; c < n; ++c) {
      res = 0;
      for (int k = 0; k < n; ++k) {
        res += m1[r * n + k] * m2[k * n + c];
      }
      result[r * n + c] = res;
    }
  }
}


void matrix_trans(float *trans, float *matrix)
{
  for (int c = 0; c < 3; c++) {
    for (int d = 0; d < 3; d++) {
      trans[c * 3 + d] = matrix[d * 3 + c];
    }
  }
}

long read_cloud(const long howmany)
{
  p_ply ply = ply_open((dataset_path + pointcloud_file).c_str(), NULL, 0, NULL);

  if (ply_read_header(ply)) {
    long cloud_size;

    ply_get_element_info(ply_get_next_element(ply, 0), 0, &cloud_size);

    if (howmany > 0 && howmany < cloud_size) {
      // load only the desired number of points
      cloud_size = howmany;
      ply_setninstances(ply_get_next_element(ply, 0), howmany);
    }

    cloud = (Point*)malloc(cloud_size * sizeof(Point));

    ply_set_read_cb(ply, "vertex", "x", vertex_x, NULL, 0);
    ply_set_read_cb(ply, "vertex", "y", vertex_y, NULL, 0);
#ifdef _NOTONLYVERT_
    ply_set_read_cb(ply, "vertex", "z", vertex_z, NULL, 0);
    ply_set_read_cb(ply, "vertex", "red", color_r, NULL, 0);
    ply_set_read_cb(ply, "vertex", "green", color_g, NULL, 0);
    ply_set_read_cb(ply, "vertex", "blue", color_b, NULL, 1);
#else
    ply_set_read_cb(ply, "vertex", "z", vertex_z, NULL, 1);
#endif

    if (ply_read(ply)) {
      ply_close(ply);
    }

    printf("Cloud loaded: %ld points\n", cloud_size);

    return cloud_size;
  } else {
    printf("Cloud not loaded! :-/");

    return 0;
  }

}


//Leer parámetros de cada imagen (posición y matriz de rotación)
Image* read_images()
{
  ifstream file((dataset_path + images_file).c_str());
  string line;

  getline(file, line);
  stringstream iss1(line);
  iss1 >> num_images;
  Image* images;
  images = (Image*)malloc(num_images * sizeof(Image));

  int it = 0;
  while (getline(file, line))
    {
      stringstream iss(line);

      Image im;
      iss >> im.position[0] >> im.position[1] >> im.position[2];
      for (int r = 0; r < 3; ++r) {
        getline(file, line);
        stringstream iss2(line);
        for (int c = 0; c < 3; ++c) {
          iss2 >> im.rotation_matrix[r * 3 + c];
        }
      }
      getline(file, line);
      stringstream iss2(line);
      iss2 >> im.band;

      getline(file, line);
      stringstream iss3(line);
      iss3 >> im.D[0] >> im.D[1] >> im.D[2] >> im.D[3];

      getline(file, line);
      stringstream iss4(line);
      iss4 >> im.A[0] >> im.A[1] >> im.A[2] >> im.A[3];

      getline(file, line);
      stringstream iss5(line);
      iss5 >> im.cx >> im.cy;

      images[it] = im;
      ++it;
    }
  printf("File 1 loaded \n");
  return images;
}


Image_glm* read_images_glm()
{
  ifstream file((dataset_path + images_file).c_str());
  string line;

  getline(file, line);
  stringstream iss1(line);
  iss1 >> num_images;
  Image_glm* images;
  images = (Image_glm*)malloc(num_images * sizeof(Image_glm));

  int it = 0;
  while (getline(file, line))
    {
      float pos[3];
      float rot_matrix[9];
      int band;
      float A[4];
      float D[4];
      float ppoint[2];

      stringstream iss(line);
      iss >> pos[0] >> pos[1] >> pos[2];
      for (int r = 0; r < 3; ++r) {
        getline(file, line);
        stringstream iss2(line);
        for (int c = 0; c < 3; ++c) {
          iss2 >> rot_matrix[r * 3 + c];
        }
      }
      getline(file, line);
      stringstream iss2(line);
      iss2 >> band;

      getline(file, line);
      stringstream iss3(line);
      iss3 >> D[0] >> D[1] >> D[2] >> D[3];

      getline(file, line);
      stringstream iss4(line);
      iss4 >> A[0] >> A[1] >> A[2] >> A[3];

      getline(file, line);
      stringstream iss5(line);
      iss5 >> ppoint[0] >> ppoint[1];

      images[it].cameraPosition = glm::make_vec3(pos);
      images[it].rotationMatrix = glm::transpose(glm::make_mat3(rot_matrix));
      images[it].fisheyeAffine = glm::make_vec4(A);
      images[it].fisheyePolynomial = glm::make_vec4(D);
      images[it].principalPoint = glm::make_vec2(ppoint);
      ++it;
    }
  printf("File 1 loaded \n");
  return images;
}


//Leer camaras asociadas a cada punto 3D
//vector<Point_cameras> read_cameras_point(const int cloud_size) {
//
//      vector<Point_cameras> projs;
//      ifstream file("./files/point_cameras.txt");
//      string line;
//      int it = 0;
//      int cams;
//      while (getline(file, line))
//      {
//              if (line != "") {
//                      Point_cameras c_p;
//                      stringstream iss(line);
//                      iss >> cams;
//                      c_p.num_cameras = cams;
//                      c_p.cameras = (int*)malloc(cams * sizeof(int));
//                      c_p.id_point = it;
//                      int token;
//                      int k = 0;
//                      while (iss >> token) {
//                              c_p.cameras[k] = token;
//                              ++k;
//                      }
//                      projs.push_back(c_p);
//              }
//              ++it;
//      }
//      printf("File 1 loaded \n");
//      return projs;
//}

vector<Point_cameras> read_cameras_point()
{
  vector<Point_cameras> point_cameras;

  FILE *f;
  f = fopen((dataset_path + mapping_file).c_str(), "rb");
  int n; //número de proyecciones globales
  fread(&n, sizeof(int), 1, f);
  num_projections = 0;
  for (int i = 0; i < n; i++) {
    Point_cameras c_p;

    int id_point;
    fread(&id_point, sizeof(int), 1, f);
    //cout << "idPoint: " << id_point << "\n";
    c_p.id_point = id_point;

    int size;
    fread(&size, sizeof(int), 1, f);
    //cout <<"size: "<< size << "\n";
    c_p.num_cameras = size;
    num_projections += size;
    int *cameras;
    cameras = (int*)malloc(size * sizeof(int));
    fread(cameras, sizeof(int), size, f);
    c_p.cameras = cameras;
    point_cameras.push_back(c_p);
    //for (int j = 0; j < size; j++) {
    //      cout << cameras[j] << " ";
    //}
    //cout << endl;
  }

  printf("File 2 loaded \n");

  return point_cameras;
}


//Leer matriz de transformación (ICP)
void read_transformation_matrix(float *result)
{
  float m1[16];
  float m2[16];

  ifstream file((dataset_path + matrix_file).c_str());
  string line;
  if (file.is_open()) {
    //M1
    int it = 0;
    getline(file, line);
    while (line[0] != '*')
      {
        stringstream linestream(line);
        for (int c = 0; c < 4; c++) {
          linestream >> m1[it * 4 + c];
        }
        getline(file, line);
        ++it;
      }

    //M2
    it = 0;
    while (getline(file, line))
      {
        stringstream linestream(line);
        for (int c = 0; c < 4; c++) {
          linestream >> m2[it * 4 + c];
        }
        ++it;
      }
    file.close();
  }
  else printf("Unable to open file \n");

  mult_matrix(result, m2, m1, 4);

  printf("File 3 loaded \n");
}


//Leer matrices de reflectancia

Reflectance_matrix* read_reflectance_matrix()
{
  Reflectance_matrix* reflec_v;

  FILE *f;
  f = fopen((dataset_path + reflectance_file).c_str(), "rb");
  int num_img;
  fread(&num_img, sizeof(int), 1, f);
  reflec_v = (Reflectance_matrix*)malloc(num_img * sizeof(Reflectance_matrix));
  for (int i = 0; i < num_img; i++) {

    Reflectance_matrix reflec;
    int band;
    fread(&band, sizeof(int), 1, f);
    reflec.band = band;

    float* matrix;
    matrix = (float*)malloc(images_resolution * sizeof(float));
    fread(matrix, sizeof(float), images_resolution, f);
    reflec.m = matrix;
    reflec_v[i] = reflec;
  }
  printf("File 4 loaded \n");
  return reflec_v;
  /*


    ifstream file("./files/reflectance.txt");
    string line;
    getline(file, line);

    stringstream iss1(line);
    iss1 >> num_images;
    reflec_v = (Reflectance_matrix*)malloc(num_images * sizeof(Reflectance_matrix));

    int it = 0;
    while (getline(file, line))
    {
    Reflectance_matrix reflec;
    reflec.m = (float**)malloc(960 * sizeof(float*));
    stringstream iss2(line);
    iss2 >> reflec.band;
    for (int r = 0; r < 960; r++) {
    getline(file, line);
    stringstream iss3(line);
    float token;
    int c = 0;
    reflec.m[r] = (float*)malloc(1280 * sizeof(float));
    while (iss3 >> token) {
    reflec.m[r][c] = token;
    ++c;
    }
    }
    getline(file, line);
    reflec_v[it] = reflec;
    ++it;
    }
  */
}


//Leer metadatos en cada banda
void read_metadata(Band* metadata)
{
  ifstream file("./files/metadata.txt");
  string line;
  if (file.is_open()) {
    for (int it = 0; it < 4; it++) {
      getline(file, line);
      stringstream iss1(line);
      iss1 >> metadata[it].band;

      getline(file, line);
      stringstream iss2(line);
      iss2 >> metadata[it].A[0] >> metadata[it].A[1] >> metadata[it].A[2] >> metadata[it].A[3];

      getline(file, line);
      stringstream iss3(line);
      iss3 >> metadata[it].D[0] >> metadata[it].D[1] >> metadata[it].D[2] >> metadata[it].D[3];

      getline(file, line);
      stringstream iss4(line);
      iss4 >> metadata[it].cx >> metadata[it].cy;
    }
  }
  printf("File 5 loaded \n");
}


void mapping_and_occlusion(Point *cloud, const unsigned long cloud_points, Image images[], const unsigned int num_images, float* v_rot_matrix, frag snapshots[])
{
  float trans_m1[9], trans_m2[9], rot_m[9];
  // trans_m1: matriz transpuesta de la matriz de rotación de la cámara
  // trans_m2: matriz transpuesta de la matriz de rotación de la transformación ICP

  //Transposición de matriz de rotación
  matrix_trans(trans_m2, v_rot_matrix);

  frag *snapshot;

  for (unsigned int cam = 0; cam < num_images; cam++) {

    snapshot = snapshots + cam * images_resolution;

    //Transposición de matrices
    matrix_trans(trans_m1, images[cam].rotation_matrix);

    //Multiplicación de matrices
    mult_matrix(rot_m, trans_m1, trans_m2, 3);

    #pragma omp parallel for
    for (unsigned long id_point = 0; id_point < cloud_points; id_point++) {

      //Traslación y Rotación
      float res[3];
      float temp;
      for (int r = 0; r < 3; ++r) {
        temp = 0;
        for (int c = 0; c < 3; ++c) {
          temp += rot_m[r * 3 + c] * (cloud[id_point].position[c] - images[cam].position[c]);
        }
        res[r] = temp;
      }
      //printf("POINT %d, x: %f \n", id_point, cloud[id_point].position[0]); 
      //printf("POINT %d, x: %f \n", id_point, res[0]);

      /*****Proyección*******/

      //Fisheye Polynomial
      float D[4] = { images[cam].D[0], images[cam].D[1], images[cam].D[2], images[cam].D[3] };
      //Fisheye Affine Matrix
      float A[4] = { images[cam].A[0], images[cam].A[1], images[cam].A[2], images[cam].A[3] };

      float cx = images[cam].cx;
      float cy = images[cam].cy;

      //Punto 3D
      float x = res[0];
      float y = res[1];
      float z = res[2];

      float r = sqrt(pow(x, 2) + pow(y, 2));
      float theta = (2 / M_PI) * atan(r / z);
      float poly = D[0] + D[1] * theta + D[2] * theta * theta + D[3] * theta * theta * theta;
      float xh = (poly * x) / r;
      float yh = (poly * y) / r;

      //Coordenadas de la imagen
      int px = round(A[2] * xh + A[3] * yh + cy);
      int py = round(A[0] * xh + A[1] * yh + cx);

      if (px < 0 || px > 959 || py < 0 || py > 1279)
        continue; // Discarded!

      //Pixel en la imagen proyectado por el Punto 3D
      unsigned int fragment_id = px * 1280 + py;

      float dist = sqrtf(powf((x - images[cam].position[0]), 2) + powf((y - images[cam].position[1]), 2) + powf((z - images[cam].position[2]), 2));

      if (dist < snapshot[fragment_id].dist) { // Update z-buffer
        snapshot[fragment_id].dist = dist;
        snapshot[fragment_id].pointid = id_point;
      }

    } // next id_point

  } // next cam, next snapshot

}


void oclussion_test(Projection** projections, vector<Point_cameras> point_cameras)
{
  for (int it = 0; it < num_images; it++) {

    //Se inicializa la matriz de oclusión a 0
    float *m_occlusion;
    size_t numbytes = images_resolution * sizeof(float);
    m_occlusion = (float*)malloc(numbytes);
    memset(m_occlusion, 0x77, numbytes);

    c_p *indices;
    numbytes = images_resolution * sizeof(c_p);
    indices = (c_p*)malloc(numbytes);
    memset(indices, -1, numbytes);
    int id_point, id_cam;
    for (long unsigned int i = 0; i < point_cameras.size(); i++) {
      for (unsigned int j = 0; j < point_cameras[i].num_cameras; j++) {
        if (projections[i][j].c == it) {
          int px = projections[i][j].px;
          int py = projections[i][j].py;
          float dist = projections[i][j].dist;
          if (dist < m_occlusion[px * 1280 + py]) {
            id_point = indices[px * 1280 + py].id_point;
            //Si hay un punto proyectado sobre ese pixel
            if (id_point != -1) {
              id_cam = indices[px * 1280 + py].id_camara;
              projections[id_point][id_cam].occluded = true;
            }
            indices[px * 1280 + py].id_point = i;
            indices[px * 1280 + py].id_camara = j;
            m_occlusion[px * 1280 + py] = dist;
            projections[i][j].occluded = false;
          }
          else {
            projections[i][j].occluded = true;
          }
        }
      }
    }
  }

  int cont = 0;
  for (long unsigned int i = 0; i < point_cameras.size(); i++) {
    for (unsigned int j = 0; j < point_cameras[i].num_cameras; j++) {
      if (projections[i][j].occluded == true)
        ++cont;
      //              printf("ID: %d, CAM: %d, oc: %d , px: %f , py: %f dist: %f \n", projections[i][j].id_point, projections[i][j].c, projections[i][j].occluded, projections[i][j].px, projections[i][j].py, projections[i][j].dist);
    }
  }
  printf("Number of occluded points: %d \n", cont);
}


int parse_args(int argc, char *argv[])
{
  int opt;

  // d: path to dataset
  //    all rest filenames are relative to this path
  //    local var: path
  //    default: ./files/
  // p: pointcloud file (ply)
  //    local var: pointcloud
  //    default: nube_66M.ply
  // i: cameras (aka images) file
  //    local var: images
  //    default: images_180.txt
  // t: transformation matrix
  //    local var: matrix
  //    default: transformation_matrix.txt
  // m: file with the mapping points<->cameras
  //    local var: mapping
  //    default: point_cameras.bin
  // r: file with reflectances
  //    local var: reflectance
  //    default: reflectance.bin

  while ((opt = getopt(argc,argv,"d:p:i:t:m:r:")) != EOF)
    switch(opt) {
    case 'd':
      dataset_path = optarg;
      break;
    case 'p':
      pointcloud_file = optarg;
      break;
    case 'i':
      images_file = optarg;
      break;
    case 't':
      matrix_file = optarg;
      break;
    case 'm':
      mapping_file = optarg;
      break;
    case 'r':
      reflectance_file = optarg;
      break;
    case '?': printf("Parameters:\n -d: path to dataset (all filenames are relative to this) DEFAULT: ./files/\n -p: pointcloud file (a ply file) DEFAULT: nube_66M.ply\n -i: cameras (aka images) file DEFAULT: images_180.txt\n -t: file with transformation matrix DEFAULT: transformation_matrix.txt\n -m: file with mapping points<->cameras DEFAULT: point_cameras.txt\n -r: file with reflectances DEFAULT: reflectance.bin\n");
      return -1;
    }

  if (dataset_path.size() == 0)
    dataset_path = "files/";

  if (pointcloud_file.size() == 0)
    pointcloud_file = "nube_66M.ply";

  if (images_file.size() == 0)
    images_file = "images_180.txt";

  if (matrix_file.size() == 0)
    matrix_file = "transformation_matrix.txt";

  if (mapping_file.size() == 0)
    mapping_file = "point_cameras.bin";

  if (reflectance_file.size() == 0)
    reflectance_file = "reflectance.bin";

  return 0;
}

void show_dataset_process(void)
{
  std::cout << "------------------------------------------------------------" << std::endl;
  std::cout << "Dataset to use:" << std::endl;
  std::cout << "-> Point cloud: " << dataset_path << pointcloud_file << std::endl;
  std::cout << "-> Cameras: " << dataset_path << images_file << std::endl;
  std::cout << "-> Transformation matrix: " << dataset_path << matrix_file << std::endl;
  std::cout << "-> Mapping Points<->Cameras: " << dataset_path << mapping_file << std::endl;
  std::cout << "-> Reflectance: " << dataset_path << reflectance_file << std::endl;
  std::cout << "------------------------------------------------------------" << std::endl << std::endl;
}

void show_invocation(int argc, char* argv[])
{
  std::cout << argv[0];
  for (int a = 1; a < argc; a++) {
    std::cout << " " << argv[a];
  }
  std::cout << std::endl;
}

void print_snapshot(frag snapshot[], unsigned int max)
{
  for (unsigned int i = 0; i < images_resolution; i++) {
    if (max == 0)
      break;

    if (snapshot[i].pointid == UINT_MAX)
      continue;

    max--;
    std::cout << i << " " << snapshot[i].pointid << " " << snapshot[i].dist << std::endl;
  }
}

unsigned int check_snapshot(frag snapshot[])
{
  unsigned int assigned_frag = 0;

  for (unsigned int i = 0; i < images_resolution; i++) {
    if (snapshot[i].pointid == UINT_MAX)
      continue;

    assigned_frag++;
  }
}
