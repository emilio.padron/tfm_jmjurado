#include <cstdio>
#include <cub/cub.cuh>
#include <vector>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "cpu.h"
#include "gpu_common.h"
#include "gpu_v32.h"

// CustomMin functor for reduction
struct CustomMin {
  CUB_RUNTIME_FUNCTION __forceinline__
  __device__ frag operator()(const frag &a, const frag &b) const {
    return (b.dist < a.dist) ? b : a;
  }
};

unsigned int GPU_mapping_and_occlusion(Point* cloud, const unsigned long cloud_points, Image* images, const unsigned int num_images, float* v_rot_matrix, frag snapshots[], const unsigned int blk_size, gputime *gpu_times, const size_t vram)
{
  // Out-of-core algorithm: dataset partition, overlapping computation and transfers
  //
  // lock_mem: size of constant data needed in vram during the whole computation
  //
  // dyn_mem: size of structures that can be split for the out-of-core computation
  //          basically: whole dataset + temp structures
  //          => temporary data needed for CUB computations are also counted here
  //          => Hard-coded aproximation: 1MB per dataset partition
  //          i.e. data+control bytes/point x number of points in dataset + 1MB x number of partitions
  //
  // avail_mem: vram available after considering the lock_mem
  //
  // => avail_mem will be split into nStreams parts: 1 residentpart/CUDAstream
  //    Each part basically has:
  //    + data_set: part of dataset to be tx and processed (npb points/part)
  //    + data_aux: aux data needed to process the current part of the dataset loaded (npb auxdata/part)
  //    + date_tmp: additional tmp date used by CUB (1MB/part)
  //
  // Summary:
  //
  //   VRAM = lock_mem + avail_mem =
  //          lock_mem + nStreams x (data_set + data_aux + data_tmp) =
  //          lock_mem + nStreams x (npb x (databytes/point + auxbytes/point) + data_tmp)
  //          lock_mem + nStreams x (npb x (databytes/point + auxbytes/point) + 1MBestimation)
  //
  // To partition data avail_mem is divided this way:
  //
  //  avail_mem = nStreams x (npb x (databytes/point + auxbytes/point) + 1MB)
  //
  // so, *npb* is the number of points per partition resident in VRAM,
  // being each resident partition tx/processed by a different CUDA Stream
  // => The idea, again, is to overlap computation and communication
  //
  //   npb = (avail_mem / nStreams - 1MB) / (databytes/point + auxbytes/point)
  //         [truncate to lower integer this quotient]
  //
  // Hence, dyn_mem is partitioned in parts (aka blocks) with npb points
  // => npb points * databytes/point = npb_size
  // => npb points * auxbytes/point = aux_size
  //
  // Dataset partition: #parts = cloud_points / npb
  //                    [round to higher integer whis quotient]

#ifdef _SYNCTIME_
  auto start = std::chrono::steady_clock::now();
#endif

  const size_t nStreams = 2; // Streams to overlap async tasks: comp & transf

  const size_t auxbytes_point = 2 * sizeof(unsigned int) // CUB aux: d_frag_key + d_frag_key_extra
                       + 2 * sizeof(frag); // CUB aux: d_frag_value + d_frag_value_extra

  size_t dyn_mem = cloud_points * (sizeof(Point) // Data: complete dataset
                   + auxbytes_point); // CUB aux: d_frag_key/value/key_extra/value_extra

  // Temporary storage required for sorting and reducing
  // Hardcoded: 1MB
  // One of this is needed for each Stream procesing a dataset partition
  size_t temp_storage_bytes = 1024 * 1024;

  size_t lock_mem = 1024 * 1024; // Estimation: 1MB for cub's sort&reduce temp storage

  // CPU->GPU transfer of the complete info about the cameras
  Image *d_images;
  size_t numBytes = num_images * sizeof(Image);
  cuda( cudaMalloc((void**)&d_images, numBytes) );
  cuda( cudaMemcpy(d_images, images, numBytes, cudaMemcpyHostToDevice) );
  lock_mem += numBytes;

  // CPU->GPU transfer of the rotation matrix
  float* d_rotation_matrix;
  numBytes = 3 * 3 * sizeof(float);
  cuda( cudaMalloc((void**)&d_rotation_matrix, numBytes) );
  cuda( cudaMemcpy(d_rotation_matrix, v_rot_matrix, numBytes, cudaMemcpyHostToDevice) );
  lock_mem += numBytes;

  size_t avail_mem = vram - lock_mem;

  std::cout << "Lock mem: " << lock_mem / 1024.0f << " KB. Avail mem: "
            << avail_mem / 1024 / 1024 << " MB. Required mem: "
            << dyn_mem / 1024.0f / 1024.0f << " MB." << std::endl;

  // real data per point (databytes/point): sizeof(Point)
  // npb: points per dataset part
  size_t npb = ((avail_mem / nStreams) - temp_storage_bytes) /
    (sizeof(Point) + auxbytes_point);
  // (integer division truncates fractional results toward 0)

  size_t partitions = cloud_points / npb + (cloud_points % npb != 0);
  size_t npb_last = cloud_points % npb;
  // (round up to have a last partition with the rest if not integer quotient)

  size_t npb_size = npb * sizeof(Point);
  size_t aux_size = npb * auxbytes_point;

  std::cout << "Number of CUDA Streams: " << nStreams << std::endl
            << "Number of point cloud partitions: " << partitions << std::endl
            << npb << " points/partition (" << npb_size / 1024 / 1024
            << " MB/partition)" << std::endl
            << "Aux + Tmp bytes/partition: " << aux_size << " + "
            << temp_storage_bytes << std::endl
            << "VRAM usage: " << nStreams << " x ("
            << npb_size / 1024 / 1024 << " MB + "
            << aux_size / 1024 / 1024 << " MB + "
            << temp_storage_bytes / 1024 / 1024 << " MB) = "
            << (nStreams * (npb_size + aux_size + temp_storage_bytes)) / 1024 / 1024 << " MB" << std::endl;

  // Resident dataset partitions (blocks) in VRAM (1 per stream)
  // Transf - Comput overlapping: while 1 block is computed other one is tx
  Point *d_block[nStreams];

  // Fragments projected for each camera on each stream
  unsigned int *d_frag_key[nStreams]; // frag_key = pixelid = pixelx * 1280 + pixely
  frag *d_frag_value[nStreams]; // (z value, point id): distancia del punto a la cámara, id punto

  // Extra allocation to sort-by-key previous arrays
  unsigned int *d_frag_key_extra[nStreams];
  frag *d_frag_value_extra[nStreams];

  // Final fragments ready to transfer to CPU (final snapshot for current block-camera)
  frag *d_snapshot[nStreams];

  // Create a set of DoubleBuffers to wrap pairs of device pointers
  // to sort-by-key the arrays (pixelid is the key)
  std::vector< cub::DoubleBuffer<unsigned int> > d_keys;
  std::vector< cub::DoubleBuffer<frag> > d_values;
  //  cub::DoubleBuffer<unsigned int> d_keys[nStreams];
  //  cub::DoubleBuffer<frag> d_values[nStreams];

  // Temporary storage required for sorting and reducing
  // => temp_storage_bytes per Stream
  void *d_temp_storage[nStreams];

  //Estructura temporal para chequear los fragmentos válidos en cada cámara
  frag *temp_snapshot; // fragimages_size x number of Streams

  int *d_num_runs_out[nStreams];

  // Array with streams artifacts
  cudaStream_t streams[nStreams];
  cudaStream_t auxstreams[nStreams];

  for (size_t stream = 0; stream < nStreams; ++stream) {
    cuda( cudaMalloc((void**) &d_block[stream], npb_size) );

    cuda( cudaMalloc((void**) &d_frag_key[stream], npb * sizeof(unsigned int)) );
    cuda( cudaMalloc((void**) &d_frag_key_extra[stream], npb * sizeof(unsigned int)) );
    cuda( cudaMalloc((void**) &d_frag_value[stream], npb * sizeof(frag)) );
    cuda( cudaMalloc((void**) &d_frag_value_extra[stream], npb * sizeof(frag)) );
    cuda( cudaMalloc((void**) &d_snapshot[stream], fragimages_size) );

    cuda( cudaMalloc(&d_temp_storage[stream], temp_storage_bytes) );

    cuda( cudaMalloc((void**) &d_num_runs_out[stream], sizeof(int)) );

    cuda( cudaStreamCreate(&streams[stream]) );
    cuda( cudaStreamCreate(&auxstreams[stream]) );

    d_keys.push_back(cub::DoubleBuffer<unsigned int>(d_frag_key[stream], d_frag_key_extra[stream]));
    d_values.push_back(cub::DoubleBuffer<frag>(d_frag_value[stream], d_frag_value_extra[stream]));
  }

  //temp_snapshot = (frag *) malloc(nStreams * fragimages_size);
  cuda( cudaMallocHost((void**)&temp_snapshot, nStreams * fragimages_size) );

  CustomMin reduction_op; // reduction operation: min value

  // CUDA stuff: threads/cuda_blk and grid of cuda_blks
  dim3 dimBlock(blk_size);

  dim3 dimGrid; // 1 thread/point
  dim3 dimGrid2((images_resolution + dimBlock.x - 1) / dimBlock.x);; // 1 thread/pixel

#ifdef _SYNCTIME_
  cuda( cudaDeviceSynchronize() );
  auto end = std::chrono::steady_clock::now();
  gpu_times->preliminary = end - start;
#endif

  size_t desp = 0; //iterador para el desplazamiento en la nube
  size_t part = 0; //block currently being managed
  size_t partpoints; // points in block currently being managed

  cudaEvent_t event_snapshotReady;
  //  cudaEventCreate (&event_snapshotReady);
  cudaEventCreateWithFlags(&event_snapshotReady, cudaEventDisableTiming);

  do {

    for (size_t stream = 0; stream < nStreams; ++stream) {

      partpoints = (++part < partitions)? npb : npb_last;

      //Tranferencia del bloque a la GPU
      cuda( cudaMemcpyAsync(d_block[stream], &cloud[desp], partpoints * sizeof(Point), cudaMemcpyHostToDevice, streams[stream]) );

      dimGrid = (partpoints + dimBlock.x - 1) / dimBlock.x; // 1 thread/point per CUDA BLK
      desp += partpoints;

      for (unsigned int c = 0; c < num_images; c++) {

        // First stage for camera #c: mapping (aka projection)
        // Get fragments for each projected point in c_projections
        // Multiple 3D points can be projected to the same fragment (with different z-component, dist)

        GPUmapping_kernel << <dimGrid, dimBlock, 0, streams[stream] >> > (partpoints, c, d_block[stream], d_images, d_rotation_matrix, d_frag_key[stream], d_frag_value[stream]);

        // Second stage for camera #c: check occlusion and update
        // general zbuffer Fragments for the same pixel are reduced by
        // distance to camera

        // First step: sort-by-key (pixelid is the key)

        // Run sorting operation
        cub::DeviceRadixSort::SortPairs(d_temp_storage[stream], temp_storage_bytes, d_keys[stream], d_values[stream], partpoints, 0, 32, streams[stream]);
        //        cub::DeviceRadixSort::SortPairs(d_temp_storage[stream], temp_storage_bytes, d_keys[stream], d_values[stream], partpoints);

        // Second step: reduce-by-key

        // Run reduce-by-key
        cub::DeviceReduce::ReduceByKey(d_temp_storage[stream], temp_storage_bytes, d_frag_key[stream], d_frag_key_extra[stream], d_frag_value[stream], d_frag_value_extra[stream], d_num_runs_out[stream], reduction_op, partpoints, streams[stream]);

        // Third stage for camera #c: copy pixels to final positions
        // filling the missing keys with 0 (pixels not covered by any fragment)
        // Every CUDA thread copies a pixel

        // Initialize destination buffer to UINT_MAX
        cuda( cudaMemsetAsync(d_frag_value[stream], UINT_MAX, fragimages_size, streams[stream]) );

        // Now just for debugging:
        // -> obtain number of pixels after reduction
        // unsigned int ncoveredpixels;
        // cuda( cudaMemcpy(&ncoveredpixels, d_num_runs_out[stream], sizeof(int), cudaMemcpyDeviceToHost) ); // GPU -> CPU
        // ncoveredpixels = ncoveredpixels - 1; // Last element after reduction is UINT_MAX
        // printf("Stream #%2d - Part#%2d - Camera#%3d: %d covered pixels\n", stream, partitions, c, ncoveredpixels);

        // Kernel to copy each pixel info into its final location
        // generating the final 960x1280 snapshot with dist+pointid per pixel
        GPUgenerate_snapshot << <dimGrid2, dimBlock, 0, streams[stream] >> > (d_num_runs_out[stream], d_frag_key_extra[stream], d_frag_value_extra[stream] /*src*/, d_snapshot[stream] /*dst*/);
        // cuda( cudaPeekAtLastError() );     // only for debugging
        // cuda( cudaDeviceSynchronize() );

        cudaEventRecord(event_snapshotReady, streams[stream]);

        // Transfer snapshot to CPU
        cudaStreamWaitEvent(auxstreams[stream], event_snapshotReady, 0);
        cuda( cudaMemcpyAsync(temp_snapshot + images_resolution * stream, d_snapshot[stream], fragimages_size, cudaMemcpyDeviceToHost, auxstreams[stream]) ); // GPU -> CPU

        // Problem to merge...
        // [TODO]

      }

      if (part == partitions)
        break;
    }

  } while (part < partitions);

  cuda( cudaFree(d_rotation_matrix) );
  cuda( cudaFree(d_images) );
  for (size_t stream = 0; stream < nStreams; ++stream) {
    cuda( cudaFree(d_temp_storage[stream]) );
    cuda( cudaFree(d_num_runs_out[stream]) );
    cuda( cudaFree(d_frag_value_extra[stream]) );
    cuda( cudaFree(d_frag_value[stream]) );
    cuda( cudaFree(d_frag_key_extra[stream]) );
    cuda( cudaFree(d_frag_key[stream]) );
    cuda( cudaFree(d_block[stream]) );
    cudaStreamDestroy(streams[stream]);
  }
  cudaFreeHost(temp_snapshot);

  // Ensure all CUDA stuff has really finished!
  cuda( cudaDeviceSynchronize() );

  return partitions;
}


__device__ void GPU_matrix_trans(float* trans, float* matrix)
{
  for (int c = 0; c < 3; c++) {
    for (int d = 0; d < 3; d++) {
      trans[c * 3 + d] = matrix[d * 3 + c];
    }
  }
}


__device__ void GPU_mult_matrix(float* result, float* m1, float* m2, int n)
{
  float res;
  for (int r = 0; r < n; ++r) {
    for (int c = 0; c < n; ++c) {
      res = 0;
      for (int k = 0; k < n; ++k) {
        res += m1[r * n + k] * m2[k * n + c];
      }
      result[r * n + c] = res;
    }
  }
}


__global__ void GPUmapping_kernel(const unsigned long npoints, const unsigned int cam_id, Point c_cloud[], Image c_images[], float c_rotation_matrix[], unsigned int frag_pixelid[], frag frag[])
{
  int task_id = blockIdx.x * blockDim.x + threadIdx.x;

  if (task_id < npoints) {

    /*****Transformación geométrica******/

    float trans_m1[9];//matriz transpuesta de la matriz de rotación de la cámara
    float trans_m2[9];//matriz transpuesta de la matriz de rotación de la transformación ICP
    float rot_m[9];

    //Transposición de matrices
    GPU_matrix_trans(trans_m1, c_images[cam_id].rotation_matrix);
    GPU_matrix_trans(trans_m2, c_rotation_matrix);

    //Multiplicación de matrices
    GPU_mult_matrix(rot_m, trans_m1, trans_m2, 3);

    //Traslación y Rotación
    float res[3];
    float temp;
    for (int r = 0; r < 3; ++r) {
      temp = 0;
      for (int c = 0; c < 3; ++c) {
        temp += rot_m[r * 3 + c] * (c_cloud[task_id].position[c] - c_images[cam_id].position[c]);
      }
      res[r] = temp;
    }

    /*****Proyección*******/
    //Fisheye Polynomial
    float D[4] = { c_images[cam_id].D[0], c_images[cam_id].D[1], c_images[cam_id].D[2], c_images[cam_id].D[3] };
    //Fisheye Affine Matrix
    float A[4] = { c_images[cam_id].A[0], c_images[cam_id].A[1], c_images[cam_id].A[2], c_images[cam_id].A[3] };

    float cx = c_images[cam_id].cx;
    float cy = c_images[cam_id].cy;

    //Punto 3D
    float x = res[0];
    float y = res[1];
    float z = res[2];

    float r = sqrt(pow(x, 2) + pow(y, 2));
    float theta = (2 / M_PI) * atan(r / z);
    float poly = D[0] + D[1] * theta + D[2] * theta * theta + D[3] * theta * theta * theta;
    float xh = (poly * x) / r;
    float yh = (poly * y) / r;

    //Coordenadas de la imagen

    int px = lrintf(A[2] * xh + A[3] * yh + cy);
    int py = lrintf(A[0] * xh + A[1] * yh + cx);

    if (px < 0 || px > 959 || py < 0 || py > 1279) {
      frag_pixelid[task_id] = UINT_MAX;
    } else {
      //Pixel en la imagen proyectado por el Punto 3D
      frag_pixelid[task_id] = lrintf(px) * 1280 + lrintf(py);

      frag[task_id].dist = sqrtf(pow((x - c_images[cam_id].position[0]), 2) + pow((y - c_images[cam_id].position[1]), 2) + pow((z - c_images[cam_id].position[2]), 2));
      frag[task_id].pointid = task_id;
    }

  }
}


__global__ void GPUgenerate_snapshot(const int * const nelements, unsigned int src_pixelid[], frag src_value[], frag dst_value[])
{
  const int task_id = blockIdx.x * blockDim.x + threadIdx.x;

  if (task_id < *nelements - 1) {
    unsigned int pixel_id = src_pixelid[task_id];

    //    For some debugging:
    //    printf("Task_id: %d - pixel_id %d - point %d - dist %f\n", task_id, pixel_id, src_value[task_id].pointid, src_value[task_id].dist);

    dst_value[pixel_id].dist = src_value[task_id].dist;
    dst_value[pixel_id].pointid = src_value[task_id].pointid;
  }
}
