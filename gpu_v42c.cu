#include <cstdio>
#include <glm/glm.hpp>
#include <iostream>
#include <vector>
#include "cpu.h"
#include "gpu_common.h"
#include "gpu_v42b.h"

__global__ void GPUmapping_kernel(const unsigned long npoints, glm::vec3 c_cloud[], const Image_glm *cam_info, frag zbuffer[]);
__global__ void GPUResetSnapshot(frag zbuffer[]);

unsigned int GPU_mapping_and_occlusion(Point* cloud, const unsigned long cloud_points, Image_glm* images, const unsigned int num_images, frag snapshots[], const unsigned int blk_size, gputime *gpu_times, size_t nStreams, const size_t vram)
{
  // Out-of-core algorithm: dataset partition, overlapping computation and transfers
  //
  // lock_mem: size of constant data needed in vram during the whole computation
  //
  // dyn_mem: size of structures that can be split for the out-of-core computation
  //          basically: whole dataset
  //          i.e. data bytes/point x number of points in dataset
  //
  // avail_mem: vram available after considering the lock_mem
  //
  // => avail_mem will be split into nStreams parts: 1 residentpart/CUDAstream
  //    Each part basically has:
  //    + data_set: part of dataset to be tx and processed (npb points/part)
  //    + snapshot: z-buffer with frag info per pixel: dist + point_id
  //                (fragimages_size = images_resolution * sizeof(frag))
  //
  // Summary:
  //
  //   VRAM = lock_mem + avail_mem =
  //          lock_mem + nStreams x (data_set + snapshot) =
  //          lock_mem + nStreams x (npb x databytes/point + fragimages_size)
  //
  // To partition data avail_mem is divided this way:
  //
  //  avail_mem = nStreams x (npb x databytes/point + fragimages_size)
  //
  // so, *npb* is the number of points per partition resident in VRAM,
  // being each resident partition tx/processed by a different CUDA Stream
  // => The idea, again, is to overlap computation and communication
  //
  //   npb = (avail_mem / nStreams - fragimages_size) / databytes/point
  //         [truncate to lower integer this quotient]
  //
  // Hence, dyn_mem is partitioned in parts (aka blocks) with npb points
  // => npb points * databytes/point = npb_size
  //
  // Dataset partition: #parts = cloud_points / npb
  //                    [round to higher integer whis quotient]

#ifdef _SYNCTIME_
  auto start = std::chrono::steady_clock::now();
#endif

  size_t dyn_mem = cloud_points * sizeof(glm::vec3) // Data: complete dataset
    + fragimages_size; // Z-buffer: (dist (float) + point_id (int)) x res

  size_t lock_mem = 100 * 1024 * 1024; // Estimation: 100MB of storage permanently needed in VRAM

  // CPU->GPU transfer of the complete info about the cameras
  Image_glm *d_images;
  size_t numBytes = num_images * sizeof(Image_glm);
  cuda( cudaMalloc((void**)&d_images, numBytes) );
  cuda( cudaMemcpy(d_images, images, numBytes, cudaMemcpyHostToDevice) );
  lock_mem += numBytes;

  size_t avail_mem = vram - lock_mem;

  // std::cout << "Lock mem: " << lock_mem / 1024.0f << " KB. Avail mem: "
  //           << avail_mem / 1024 / 1024 << " MB. Required mem: "
  //           << dyn_mem / 1024.0f / 1024.0f << " MB." << std::endl;

  // real data per point (databytes/point): sizeof(Point)
  // npb: max points per dataset part
  size_t npb;

  if (dyn_mem > avail_mem) { // not enough memory! => out-of-core
    if (nStreams < 2)
      nStreams = 2;
    npb = (avail_mem/nStreams - fragimages_size) / sizeof(glm::vec3);
    // (integer division truncates fractional results toward 0)
  } else {
    npb = (cloud_points / nStreams) + (cloud_points % nStreams);
  }

  size_t partitions = cloud_points / npb + (cloud_points % npb != 0);
  size_t npb_last = cloud_points % npb;
  if (npb_last == 0)
    npb_last = npb;
  // (round up to have a last partition with the rest if not integer quotient)

  size_t npb_size = npb * sizeof(glm::vec3);

  if (partitions < nStreams)
    nStreams = partitions;

  // std::cout << "Number of CUDA Streams: " << nStreams << std::endl
  //           << "Number of point cloud partitions: " << partitions << std::endl
  //           << npb << " points/partition (" << npb_size / 1024 / 1024
  //           << " MB/partition)" << std::endl
  //           << "VRAM usage: " << nStreams << " x ("
  //           << npb_size / 1024 / 1024 << " MB) = "
  //           << (nStreams * npb_size) / 1024 / 1024 << " MB" << std::endl;

  // Resident dataset partitions (blocks) in VRAM (1 per stream)
  // Transf - Comput overlapping: while 1 block is computed other one is tx
  glm::vec3 *d_block[nStreams];

  // Final fragments ready to transfer to CPU (final snapshot for current block-camera)
  frag *d_snapshot[nStreams];

  // snapshot for current camera
  frag *snapshot[nStreams],
    //Estructura temporal para chequear los fragmentos válidos en cada cámara
    *h_snapshot; // fragimages_size x number of Streams

  // Array with streams artifacts
  cudaStream_t streams[nStreams];
  cudaStream_t auxstreams[nStreams];

  for (size_t stream = 0; stream < nStreams; ++stream) {
    cuda( cudaMalloc((void**) &d_block[stream], npb_size) );

    cuda( cudaMalloc((void**) &d_snapshot[stream], fragimages_size) );

    cuda( cudaStreamCreate(&streams[stream]) );
    cuda( cudaStreamCreate(&auxstreams[stream]) );
  }

  //h_snapshot = (frag *) malloc(nStreams * fragimages_size);
  cuda( cudaMallocHost((void**)&h_snapshot, nStreams * fragimages_size) );

  // CUDA stuff: threads/cuda_blk and grid of cuda_blks
  dim3 dimBlock(blk_size);

  dim3 dimGrid; // 1 thread/point
  dim3 dimGrid2((images_resolution + dimBlock.x - 1) / dimBlock.x);; // 1 thread/pixel

#ifdef _SYNCTIME_
  cuda( cudaDeviceSynchronize() );
  auto end = std::chrono::steady_clock::now();
  gpu_times->preliminary = end - start;
#endif

  cudaEvent_t event_snapshotReady, event_snapshotRcvd;
  //  cudaEventCreate (&event_snapshotReady);
  cudaEventCreateWithFlags(&event_snapshotReady, cudaEventDisableTiming);
  cudaEventCreateWithFlags(&event_snapshotRcvd, cudaEventDisableTiming);

  if (partitions == 1) {
    //streams[0] for CPU->GPU and GPU kernels
    //auxtreams[0] for GPU->CPU

    snapshot[0] = snapshots;

    cuda( cudaMemcpyAsync(d_block[0], cloud, npb_size, cudaMemcpyHostToDevice, streams[0]) );

    dimGrid = (npb + dimBlock.x - 1) / dimBlock.x; // 1 thread/point per CUDA BLK

    for (unsigned int c = 0; c < num_images; c++) {
      GPUResetSnapshot<< <dimGrid2, dimBlock, 0, streams[0] >> > (d_snapshot[0]);
      GPUmapping_kernel<< <dimGrid, dimBlock, 0, streams[0] >> > (npb, d_block[0], d_images + c, d_snapshot[0]);

      //cuda( cudaPeekAtLastError() );     // only for debugging
      //cuda( cudaDeviceSynchronize() );

      cudaEventRecord(event_snapshotReady, streams[0]);

      // Merge c-1 image
      if (c > 0) {
        cudaEventSynchronize(event_snapshotRcvd);

        for (size_t j = 0; j < images_resolution; j++) {
          snapshots[j] = h_snapshot[j];
        }

        snapshot[0] += images_resolution; // move to snapshot for next camera
      }

      cudaStreamWaitEvent(auxstreams[0], event_snapshotReady, 0);
      cuda( cudaMemcpyAsync(h_snapshot, d_snapshot[0], fragimages_size, cudaMemcpyDeviceToHost, auxstreams[0]) ); // GPU -> CPU

      cudaEventRecord(event_snapshotRcvd, auxstreams[0]);
    }

  } else { // partitions > 1

    size_t desp = 0; //iterador para el desplazamiento en la nube
    size_t part = 0; //block currently being managed
    size_t partpoints[nStreams]; // points in block currently being managed
    unsigned int pending = 0;

    for (size_t stream = 0; stream < nStreams; ++stream) {

      partpoints[stream] = (++part < partitions)? npb : npb_last;

      //Tranferencia del bloque a la GPU
      cuda( cudaMemcpyAsync(d_block[stream], &cloud[desp], partpoints[stream] * sizeof(glm::vec3), cudaMemcpyHostToDevice, streams[stream]) );
      //std::cout << "CPU->GPU<" << stream << "> Blk #" << part-1 << " (" << partpoints[stream] << " points)" << std::endl;

      snapshot[stream] = snapshots; // Reset: first snapshot for first camera!
      ++pending;

      desp += partpoints[stream];
    }

    do {

      for (size_t stream = 0; stream < nStreams; ++stream) {

        if (partpoints[stream] == 0)
          break;

        dimGrid = (partpoints[stream] + dimBlock.x - 1) / dimBlock.x; // 1 thread/point per CUDA BLK

        for (unsigned int c = 0; c < num_images; c++) {

          GPUResetSnapshot<< <dimGrid2, dimBlock, 0, streams[stream] >> > (d_snapshot[stream]);
          GPUmapping_kernel<< <dimGrid, dimBlock, 0, streams[stream] >> > (partpoints[stream], d_block[stream], d_images + c, d_snapshot[stream]);
          //std::cout << "Kernels<" << stream << "> (" << partpoints[stream] << " points, cam#" << c << ")" << std::endl;

          //cuda( cudaPeekAtLastError() );     // only for debugging
          //cuda( cudaDeviceSynchronize() );
          //std::cout << "Kernels<" << stream << "> (" << partpoints[stream] << " points, cam#" << c << ")" << std::endl;

          cudaEventRecord(event_snapshotReady, streams[stream]);

          // Merge c-1 image
          if (c > 0) {
            cudaEventSynchronize(event_snapshotRcvd);

            frag *partial_snapshot, *final_snapshot;

            partial_snapshot = h_snapshot + images_resolution * stream;
            final_snapshot = snapshot[stream];

            for (size_t j = 0; j < images_resolution; j++) {
              if(partial_snapshot[j].dist < final_snapshot[j].dist){
                final_snapshot[j] = partial_snapshot[j];
              }
            }
            //std::cout << "Merge stream " << stream << " - image #" << c-1 << std::endl;

            snapshot[stream] += images_resolution; // move to snapshot for next camera
          }

          // Transfer snapshot to CPU
          cudaStreamWaitEvent(auxstreams[stream], event_snapshotReady, 0);
          cuda( cudaMemcpyAsync(h_snapshot + images_resolution * stream, d_snapshot[stream], fragimages_size, cudaMemcpyDeviceToHost, auxstreams[stream]) ); // GPU -> CPU

          cudaEventRecord(event_snapshotRcvd, auxstreams[stream]);

        }
        --pending;

        // Transfer next block for this stream (if needed!)
        if (part < partitions) {
          partpoints[stream] = (++part < partitions)? npb : npb_last;

          cuda( cudaMemcpyAsync(d_block[stream], &cloud[desp], partpoints[stream] * sizeof(Point), cudaMemcpyHostToDevice, streams[stream]) );
          //std::cout << "CPU->GPU<" << stream << "> Blk #" << part-1 << " (" << partpoints[stream] << " points)" << std::endl;

          ++pending;

          desp += partpoints[stream];
        } else { // No more CPU->GPU transfers
          partpoints[stream] = 0;
        }

        // Merge last image from previous block
        cudaEventSynchronize(event_snapshotRcvd);

        frag *partial_snapshot, *final_snapshot;

        partial_snapshot = h_snapshot + images_resolution * stream;
        final_snapshot = snapshot[stream];

        for (size_t j = 0; j < images_resolution; j++) {
          if(partial_snapshot[j].dist < final_snapshot[j].dist){
            final_snapshot[j] = partial_snapshot[j];
          }
        }

        //std::cout << "Merge stream " << stream << " - last image (" << then << ")" << std::endl;

        // Setup stuff for next block!
        snapshot[stream] = snapshots; // Reset: first snapshot for first camera!
      }

    } while (pending);

  }

  cuda( cudaFree(d_images) );
  for (size_t stream = 0; stream < nStreams; ++stream) {
    cuda( cudaFree(d_block[stream]) );
    cudaStreamDestroy(streams[stream]);
  }
  cudaFreeHost(h_snapshot);

  // Ensure all CUDA stuff has really finished!
  cuda( cudaDeviceSynchronize() );

  return partitions;
}

__global__ void GPUmapping_kernel(const unsigned long npoints, glm::vec3 c_cloud[], const Image_glm *cam_info, frag zbuffer[])
{
  unsigned int task_id = blockIdx.x * blockDim.x + threadIdx.x;

  if (task_id < npoints) {

    // Projection: 3D to 2D
    glm::vec3 tPoint = cam_info->rotationMatrix * (c_cloud[task_id] - cam_info->cameraPosition);
    float r = glm::length(glm::vec2(tPoint));
    float theta = (2.0f / M_PI) * atanf(r / tPoint.z);
    float poly = cam_info->fisheyePolynomial.x + cam_info->fisheyePolynomial.y * theta + cam_info->fisheyePolynomial.z * theta * theta + cam_info->fisheyePolynomial.w * theta * theta * theta;
    float xh = (poly * tPoint.x) / r;
    float yh = (poly * tPoint.y) / r;

    int px = int(cam_info->fisheyeAffine.z * xh + cam_info->fisheyeAffine.w * yh + cam_info->principalPoint.y);
    int py = int(cam_info->fisheyeAffine.x * xh + cam_info->fisheyeAffine.y * yh + cam_info->principalPoint.x);
    bool isPointOutside = px < 0 || px > 959 || py < 0 || py > 1279;

    if (!isPointOutside) {
      //Pixel en la imagen proyectado por el Punto 3D
      unsigned int fragment_id = px * 1280 + py;

      float dist = sqrtf(powf((tPoint.x - cam_info->cameraPosition[0]), 2) + powf((tPoint.y - cam_info->cameraPosition[1]), 2) + powf((tPoint.z - cam_info->cameraPosition[2]), 2));
      frag fragment = {task_id, dist};

      atomicMin(&zbuffer[fragment_id].dist64, fragment.dist64);
    }
  }
}


__global__ void GPUResetSnapshot(frag zbuffer[])
{
  unsigned int task_id = blockIdx.x * blockDim.x + threadIdx.x;

  if (task_id < 1228800) {
    zbuffer[task_id].pointid = UINT_MAX;
    zbuffer[task_id].dist = FLT_MAX;
  }
}


__global__ void GPUgenerate_snapshot(const int * const nelements, unsigned int src_pixelid[], frag src_value[], frag dst_value[])
{
  const int task_id = blockIdx.x * blockDim.x + threadIdx.x;

  if (task_id < *nelements - 1) {
    unsigned int pixel_id = src_pixelid[task_id];

    //    For some debugging:
    //    printf("Task_id: %d - pixel_id %d - point %d - dist %f\n", task_id, pixel_id, src_value[task_id].pointid, src_value[task_id].dist);

    dst_value[pixel_id].dist = src_value[task_id].dist;
    dst_value[pixel_id].pointid = src_value[task_id].pointid;
  }
}
