#include <cstdio>
#include <unistd.h>
#include <cuda.h>
#include <glm/glm.hpp>
#include <iostream>
#include <vector>
#include "cpu.h"
#include "gpu_common.h"
#include "gpu_soc_naive.h"

__device__ unsigned long long int my_atomicMin(unsigned long long int* address, float val1, int val2)
{
    frag loc, loctest;
    loc.dist = val1;
    loc.pointid = val2;
    loctest.dist64 = *address;
    while (loctest.dist > val1)
      loctest.dist64 = atomicCAS(address, loctest.dist64, loc.dist64);

    return loctest.dist64;
}

__global__ void GPUmapping_kernel(const unsigned long npoints, glm::vec3 c_cloud[], Image_glm cam_info[], frag zbuffer[])
{
  unsigned int task_id = blockIdx.x * blockDim.x + threadIdx.x;

  if (task_id < npoints) {

    // Projection: 3D to 2D
    glm::vec3 tPoint = cam_info->rotationMatrix * (c_cloud[task_id] - cam_info->cameraPosition);
    float r = glm::length(glm::vec2(tPoint));
    float theta = (2.0f / M_PI) * atanf(r / tPoint.z);
    float poly = cam_info->fisheyePolynomial.x + cam_info->fisheyePolynomial.y * theta + cam_info->fisheyePolynomial.z * theta * theta + cam_info->fisheyePolynomial.w * theta * theta * theta;
    float xh = (poly * tPoint.x) / r;
    float yh = (poly * tPoint.y) / r;

    int px = int(cam_info->fisheyeAffine.z * xh + cam_info->fisheyeAffine.w * yh + cam_info->principalPoint.y);
    int py = int(cam_info->fisheyeAffine.x * xh + cam_info->fisheyeAffine.y * yh + cam_info->principalPoint.x);
    bool isPointOutside = px < 0 || px > 959 || py < 0 || py > 1279;

    if (!isPointOutside) {
      //Pixel en la imagen proyectado por el Punto 3D
      unsigned int fragment_id = px * 1280 + py;

      float dist = glm::length(tPoint - cam_info->cameraPosition);

      //      frag_glm fragment(task_id | (dist << 32));
      //      atomicMin(&zbuffer[fragment_id], fragment);

      // frag thisfrag = {task_id, dist};
      // thisfrag.dist = dist;
      // thisfrag.pointid = task_id;
      // atomicMin(&zbuffer[fragment_id].dist64, thisfrag.dist64);

      // 32 bits float atomics (no 64-bit atomics in CC < 3.5)
      my_atomicMin(&zbuffer[fragment_id].dist64, dist, task_id);
    }
  }
}


__global__ void GPUResetSnapshot(unsigned int nfrags, frag zbuffer[])
{
  unsigned int task_id = blockIdx.x * blockDim.x + threadIdx.x;

  if (task_id < nfrags) {
    zbuffer[task_id].pointid = UINT_MAX;
    zbuffer[task_id].dist = FLT_MAX;
  }
}

__global__ void GPUFinalSnapshot(unsigned int nfrags, frag zbuffer[], unsigned int finalbuf[])
{
  unsigned int task_id = blockIdx.x * blockDim.x + threadIdx.x;

  if (task_id < nfrags) {
    finalbuf[task_id] = zbuffer[task_id].pointid;
  }
}


int parse_args_socver(int argc, char *argv[], unsigned int *cb, unsigned int *batch, long npoints)
{
  int opt;

  // d: path to dataset
  //    all rest filenames are relative to this path
  //    local var: path
  //    default: ./files/
  // p: pointcloud file (ply)
  //    local var: pointcloud
  //    default: nube_66M.ply
  // i: cameras (aka images) file
  //    local var: images
  //    default: images_180.txt
  // t: transformation matrix
  //    local var: matrix
  //    default: transformation_matrix.txt
  // m: file with the mapping points<->cameras
  //    local var: mapping
  //    default: point_cameras.bin
  // r: file with reflectances
  //    local var: reflectance
  //    default: reflectance.bin
  // h: CUDA threads/block
  //    default: CUDA_BLK
  // b: Batch size - number of cameras to process
  //    default: 1
  // n: Max points to load from dataset
  //    default: 0 (all points in pointcloud)

  *cb = CUDA_BLK;
  *batch = 1;

  while ((opt = getopt(argc,argv,"d:p:i:t:m:r:h:b:n:")) != EOF)
    switch(opt) {
    case 'd':
      dataset_path = optarg;
      break;
    case 'p':
      pointcloud_file = optarg;
      break;
    case 'i':
      images_file = optarg;
      break;
    case 't':
      matrix_file = optarg;
      break;
    case 'm':
      mapping_file = optarg;
      break;
    case 'r':
      reflectance_file = optarg;
      break;
    case 'h':
      *cb = atoi(optarg);
      break;
    case 'b':
      *batch = atoi(optarg);
      break;
    case 'n':
      *npoints = atoi(optarg);
      break;
    case '?': printf("Parameters:\n -d: path to dataset (all filenames are relative to this) DEFAULT: ./files/\n -p: pointcloud file (a ply file) DEFAULT: nube_66M.ply\n -i: cameras (aka images) file DEFAULT: images_180.txt\n -t: file with transformation matrix DEFAULT: transformation_matrix.txt\n -m: file with mapping points<->cameras DEFAULT: point_cameras.txt\n -r: file with reflectances DEFAULT: reflectance.bin\n -h: CUDA threads/block DEFAULT: CUDA_BLK\n -h: batch size DEFAULT: 1\n -0: List CUDA devices and properties\n");
      return -1;
    }

  if (dataset_path.size() == 0)
    dataset_path = "files/";

  if (pointcloud_file.size() == 0)
    pointcloud_file = "nube_66M.ply";

  if (images_file.size() == 0)
    images_file = "images_180.txt";

  if (matrix_file.size() == 0)
    matrix_file = "transformation_matrix.txt";

  if (mapping_file.size() == 0)
    mapping_file = "point_cameras.bin";

  if (reflectance_file.size() == 0)
    reflectance_file = "reflectance.bin";

  return 0;
}
