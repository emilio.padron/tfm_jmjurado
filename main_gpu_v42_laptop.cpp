#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cfloat>
#include <chrono>
#include <iostream>
#include <iomanip>
#include <vector>
#include "cpu.h"
#include "rply.h"
#include "gpu_common.h"
#include "gpu_v42_laptop.h"
using namespace std;

void printout_report(gputime *gpu_times, bool verbose);
long read_cloud_pinned(const long howmany = 0);

/*
  Función principal
*/
int main(int argc, char* argv[])
{
  long npoints;    // Points in dataset
  int device = 0;  // CUDA Device
  unsigned int cb; // CUDA Threads/Block
  float vram;      // Max GPU mem to use (GB)
  size_t nStreams; // Min CUDA Streams
  bool verbose;

  show_invocation(argc, argv);

  if (parse_args_gpuver(argc, argv, &device, &cb, &vram, &nStreams, &verbose) != 0)
    return -1;

  show_dataset_process();

  size_t freebytes = deviceinit(device, cb);
  if (vram > 0.0) {
    size_t bytes = vram * 1024 * 1024 * 1024; // GB to B
    if (bytes < freebytes)
      freebytes = bytes;
  } else {
    vram = freebytes / 1024.0f / 1024.0f / 1024.0f; // B to GB
  }

  /*LECTURA DE FICHEROS*/
  auto start = std::chrono::steady_clock::now();

  //Se lee la nube de puntos
  npoints = read_cloud_pinned();

  //Se lee fichero 1: imágenes
  Image *images = read_images();

  //Se lee fichero 2: cámaras por punto / puntos por cámara
  //Skipped in pure gpu version

  //Se lee fichero 3: matriz de transformación (ICP)
  float *transf_matrix;
  transf_matrix = (float*)malloc(4 * 4 * sizeof(float));
  read_transformation_matrix(transf_matrix);
  //Se lee fichero 4: matrices de reflectancia
  //Reflectance_matrix* reflectance_v = read_reflectance_matrix();

  auto end = std::chrono::steady_clock::now();

  std::cout << "Files loaded in " << (std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()) / 1000.0 << " ms" << std::endl;

  printf("Cloud size = %lu (%.2f GiB)\n", npoints, (float) (npoints * sizeof(Point)) / 1024 / 1024 / 1024);
  printf("Number of images = %u (960 x 1280 = %ld)\n", num_images, images_resolution);

  //  get_GPU_memory_footprintALT(npoints, num_images, vram, nStreams);
  get_GPU_memory_footprint(npoints, num_images, vram);

  //Calculo de la matriz de rotación y escala (ICP)
  float rotation_matrix[9];
  float v_scale[3];
  for (int i = 0; i < 3; i++) {
    float s_index = 0;
    for (int j = 0; j < 3; j++) {
      s_index += transf_matrix[j * 4 + i] * transf_matrix[j * 4 + i];
    }
    v_scale[i] = sqrtf(s_index);
  }
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      rotation_matrix[j * 3 + i] = transf_matrix[j * 4 + i] / v_scale[i];
    }
  }

  //Transposición de matriz ICP
  float transposed_ICP[9];

  for (int c = 0; c < 3; c++) {
    for (int d = 0; d < 3; d++) {
      transposed_ICP[c * 3 + d] = rotation_matrix[d * 3 + c];
    }
  }

  // Multiplicación de transpuesta de matriz de cada cámara por matriz ICP
  for (int i = 0; i < num_images; ++i) {
    float transposed[9];

    for (int c = 0; c < 3; c++) {
      for (int d = 0; d < 3; d++) {
        transposed[c * 3 + d] = images[i].rotation_matrix[d * 3 + c];
      }
    }

    float res;
    for (int r = 0; r < 3; ++r) {
      for (int c = 0; c < 3; ++c) {
        res = 0;
        for (int k = 0; k < 3; ++k) {
          res += transposed[r * 3 + k] * transposed_ICP[k * 3 + c];
        }
        images[i].rotation_matrix[r * 3 + c] = res;
      }
    }
  }

  /****************GPU*********************/
  // Buffer for all snapshots
  frag *snapshots = (frag *) malloc(num_images * fragimages_size);

  //Se inicializan snapshots con un valor alto de distancia
  //  memset(snapshots, UINT_MAX, num_images * fragimages_size);
  for(int i=0; i<num_images*images_resolution; ++i) {
    snapshots[i].pointid = UINT_MAX;
    snapshots[i].dist = FLT_MAX;
  }

  gputime gpu_times(num_images);

  start = std::chrono::steady_clock::now();

  size_t partitions = GPU_mapping_and_occlusion(cloud, npoints, images, num_images, snapshots, cb, &gpu_times, nStreams, freebytes);

  end = std::chrono::steady_clock::now();

  gpu_times.kernels_with_transfers = end - start;

  std::cout << std::endl << pointcloud_file << " " << images_file << " h" << cb;
  std::cout << " n" << vram << " s" << nStreams << " p" << partitions << " ";
  std::cout << std::fixed << std::setprecision(2) << (std::chrono::duration_cast<std::chrono::microseconds>(gpu_times.kernels_with_transfers).count()) / 1000.0 / 1000.0 << "s" << std::endl;

  return(0);
}

void printout_report(gputime *gpu_times, bool verbose)
{
  std::cout << "Total Time for Mapping & Occlusion (kernels + transfers): " << (std::chrono::duration_cast<std::chrono::microseconds>(gpu_times->kernels_with_transfers).count()) / 1000.0 << " ms" << std::endl;

  if (verbose) {
    std::cout << std::endl << "Times per camera (comp + transf):" << std::endl;

    for(int i=0; i<num_images; i++) {
      std::cout << std::setprecision(2) << std::fixed;
      std::cout << "CAM#" << std::setw(2) << i                            \
                << ": " << (std::chrono::duration_cast<std::chrono::microseconds>(gpu_times->camtime[i].allcomputation).count()) /1000.0 \
                << " ms" << std::endl;
    }
  }

#ifdef _SYNCTIME_
  std::cout << "-> Preliminary time (Some CPU->GPU & allocations): " << (std::chrono::duration_cast<std::chrono::microseconds>(gpu_times->preliminary).count()) / 1000.0 << " ms" << std::endl;
#endif
}


long read_cloud_pinned(const long howmany)
{
  p_ply ply = ply_open((dataset_path + pointcloud_file).c_str(), NULL, 0, NULL);

  if (ply_read_header(ply)) {
    long cloud_size;

    ply_get_element_info(ply_get_next_element(ply, 0), 0, &cloud_size);

    if (howmany > 0 && howmany < cloud_size) {
      // load only the desired number of points
      cloud_size = howmany;
      ply_setninstances(ply_get_next_element(ply, 0), howmany);
    }

    //    cloud = (Point*)malloc(cloud_size * sizeof(Point));
    cudaError_t status = cudaMallocHost((void**)&cloud, cloud_size * sizeof(Point));
    if (status != cudaSuccess) {
      printf("Error allocating pinned host memory\n");
      printf("Cloud not loaded! :-/");

      return 0;
    }

    ply_set_read_cb(ply, "vertex", "x", vertex_x, NULL, 0);
    ply_set_read_cb(ply, "vertex", "y", vertex_y, NULL, 0);
#ifdef _NOTONLYVERT_
    ply_set_read_cb(ply, "vertex", "z", vertex_z, NULL, 0);
    ply_set_read_cb(ply, "vertex", "red", color_r, NULL, 0);
    ply_set_read_cb(ply, "vertex", "green", color_g, NULL, 0);
    ply_set_read_cb(ply, "vertex", "blue", color_b, NULL, 1);
#else
    ply_set_read_cb(ply, "vertex", "z", vertex_z, NULL, 1);
#endif

    if (ply_read(ply)) {
      ply_close(ply);
    }

    printf("Cloud loaded: %ld points\n", cloud_size);

    return cloud_size;
  } else {
    printf("Cloud not loaded! :-/");

    return 0;
  }

}
