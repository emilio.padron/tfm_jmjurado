#ifndef _gpu_h
#define _gpu_h

#include <cuda_runtime.h>
#include <chrono>
#include "cpu.h"

class gputime {
public:
  unsigned int ncams {};
  unsigned int nparts {};

  std::chrono::duration<double> preliminary {};

  struct percam {
    std::chrono::duration<double> CPUGPUtransf {};
    std::chrono::duration<double> project {};
    std::chrono::duration<double> sort {};
    std::chrono::duration<double> reduce {};
    std::chrono::duration<double> fill {};
    std::chrono::duration<double> GPUCPUtransf {};
    std::chrono::duration<double> merge {};
  } *camtime;

  struct perpartition {
    std::chrono::duration<double> CPUGPUtransf {};
    std::chrono::duration<double> project {};
    std::chrono::duration<double> sort {};
    std::chrono::duration<double> reduce {};
    std::chrono::duration<double> fill {};
    std::chrono::duration<double> GPUCPUtransf {};
    std::chrono::duration<double> merge {};
  } *partime;

  std::chrono::duration<double> kernels_with_transfers {};

  gputime(unsigned int ncams) {
    gputime::ncams = ncams;
    camtime = new percam[ncams];
    partime = NULL;
  }

  gputime(unsigned int ncams, unsigned int nparts) {
    gputime::ncams = ncams;
    gputime::nparts = nparts;
    camtime = new percam[ncams];
    partime = new perpartition[nparts];
  }

  void addpartitions(unsigned int nparts) {
    gputime::nparts = nparts;
    partime = new perpartition[nparts];
  }

  ~gputime() {
    delete [] camtime;
    delete [] partime;
  }
};

__global__ void GPUmapping_kernel(const unsigned long npoints, const unsigned int cam_id, Point c_cloud[], Image c_images[], float c_rotation_matrix[], unsigned int frag_pixelid[], frag frag[]);

unsigned int GPU_mapping_and_occlusion(Point* cloud, const unsigned long cloud_size, Image* images, const unsigned int num_images, float* v_rot_matrix, frag snapshots[], const unsigned int blk_size, gputime *gpu_times, size_t gpu_memory = 1024ul * 1024 * 1024 /* 1 GB default VRAM */);

__global__ void GPUgenerate_snapshot(const int * const nelements, unsigned int src_pixelid[], frag src_value[], frag dst_value[]);

#endif
