#include <cstdio>
#include <unistd.h>
#include <iostream>
#include <iomanip>
#include <string>
#include <cub/version.cuh>
#include "gpu_common.h"
#include "cpu.h"

//Consts
const int CUDA_BLK = 64;  // Tamaño predeterm. de bloque de hilos CUDA
size_t max_grid_x;

void devicenfo(int device)
{
  struct cudaDeviceProp capabilities;

  cudaGetDeviceProperties(&capabilities, 0);

  printf("->CUDA Platform & Capabilities\n");
  printf("Name: %s\n", capabilities.name);
  printf("totalGlobalMem: %.2f MB\n", capabilities.totalGlobalMem / 1024.0f / 1024.0f);
  printf("sharedMemPerBlock: %.2f KB\n", capabilities.sharedMemPerBlock / 1024.0f);
  printf("regsPerBlock (32 bits): %d\n", capabilities.regsPerBlock);
  printf("warpSize: %d\n", capabilities.warpSize);
  printf("memPitch: %.2f KB\n", capabilities.memPitch / 1024.0f);
  printf("maxThreadsPerBlock: %d\n", capabilities.maxThreadsPerBlock);
  printf("maxThreadsDim: %d x %d x %d\n", capabilities.maxThreadsDim[0],
         capabilities.maxThreadsDim[1], capabilities.maxThreadsDim[2]);
  printf("maxGridSize: %d x %d\n", capabilities.maxGridSize[0],
         capabilities.maxGridSize[1]);
  printf("totalConstMem: %.2f KB\n", capabilities.totalConstMem / 1024.0f);
  printf("major.minor: %d.%d\n", capabilities.major, capabilities.minor);
  printf("clockRate: %.2f MHz\n", capabilities.clockRate / 1024.0f);
  printf("textureAlignment: %zd\n", capabilities.textureAlignment);
  printf("deviceOverlap: %d\n", capabilities.deviceOverlap);
  printf("multiProcessorCount: %d\n", capabilities.multiProcessorCount);
}

void devicesnfo(void)
{
  printf("->CUDA Platforms & Capabilities\n");
  int deviceCount;
  cudaGetDeviceCount(&deviceCount);
  int device;
  for (device = 0; device < deviceCount; ++device) {
    cudaDeviceProp deviceProp;
    cudaGetDeviceProperties(&deviceProp, device);
    printf("Device %d: %s - compute capability %d.%d - warp %d\n",
           device, deviceProp.name, deviceProp.major, deviceProp.minor, deviceProp.warpSize);
    printf("\tConcurrent kernels: %d\n", deviceProp.concurrentKernels);
    printf("\tOverlap data transf - kernel exec: %d\n", deviceProp.asyncEngineCount);

    printf("\tGlobal mem: %.1f GiB\n", deviceProp.totalGlobalMem/1024.0f/1024.0f/1024.0f);
    printf("\tShared mem/blk: %.1f KiB\n", deviceProp.sharedMemPerBlock/1024.0f);

    printf("\tNumber of multiprocs: %d\n", deviceProp.multiProcessorCount);

    printf("\tRegs/multiproc: %d\n", deviceProp.regsPerMultiprocessor);
    printf("\tRegs/blk: %d\n", deviceProp.regsPerBlock);

    printf("\tMaxThreads/blk: %d\n", deviceProp.maxThreadsPerBlock);
    printf("\tMaxThreads/dim: %d x %d x %d\n", deviceProp.maxThreadsDim[0],
           deviceProp.maxThreadsDim[1], deviceProp.maxThreadsDim[2]);
    printf("\tMaxGridSize: %d x %d\n", deviceProp.maxGridSize[0],
           deviceProp.maxGridSize[1]);
  }
}

size_t deviceinit(int dev, unsigned int cb)
{
  int device;

  cudaSetDevice(dev);

  cudaGetDevice(&device);
  struct cudaDeviceProp props;
  cudaGetDeviceProperties(&props, device);
  max_grid_x = props.maxGridSize[0];
  cudaRuntimeGetVersion (&device); // Reuse variable to obtain cuda version
  printf("*********************************************\n");
  printf("ID: %d\n", device);
  printf("Device name: %s (%.1f GiB, CompCap. %d.%d)\n",props.name, (float) props.totalGlobalMem/1024/1024/1024, props.major, props.minor);
  printf("CUDA: %d  CUB: %d.%d.%d\n", device, CUB_MAJOR_VERSION, CUB_MINOR_VERSION, CUB_SUBMINOR_VERSION);
  printf("Threads/Block: %d (CUDA Max Grid.x: %d)\n", cb, max_grid_x);
  printf("*********************************************\n");

  size_t avail_phy_vidmem = 0, total_phy_vidmem = 0;
  cuda( cudaMemGetInfo(&avail_phy_vidmem, &total_phy_vidmem));

  return(avail_phy_vidmem);
}

/*
  Función que ajusta el número de hilos, de bloques, y de bloques por hilo
  de acuerdo a las restricciones de la GPU
*/
void checkparams(unsigned int* n, unsigned int* cb)
{
  struct cudaDeviceProp capabilities;

  // Si menos numero total de hilos que tamaño bloque, reducimos bloque
  if (*cb > * n)
    *cb = *n;

  cudaGetDeviceProperties(&capabilities, 0);

  if (*cb > capabilities.maxThreadsDim[0]) {
    *cb = capabilities.maxThreadsDim[0];
    printf("->Núm. hilos/bloq cambiado a %d (máx por bloque para dev)\n\n",
           *cb);
  }

  if (((*n + *cb - 1) / *cb) > capabilities.maxGridSize[0]) {
    *cb = 2 * (*n - 1) / (capabilities.maxGridSize[0] - 1);
    if (*cb > capabilities.maxThreadsDim[0]) {
      *cb = capabilities.maxThreadsDim[0];
      printf("->Núm. hilos/bloq cambiado a %d (máx por bloque para dev)\n",
             *cb);
      if (*n > (capabilities.maxGridSize[0] * *cb)) {
        *n = capabilities.maxGridSize[0] * *cb;
        printf("->Núm. total de hilos cambiado a %d (máx por grid para \
dev)\n\n", *n);
      }
      else {
        printf("\n");
      }
    }
    else {
      printf("->Núm. hilos/bloq cambiado a %d (%d máx. bloq/grid para \
dev)\n\n",
             *cb, capabilities.maxGridSize[0]);
    }
  }
}

int parse_args_gpuver(int argc, char *argv[], int *device, unsigned int *cb, float *vram, size_t *nStreams, bool *verbose)
{
  int opt;

  // d: path to dataset
  //    all rest filenames are relative to this path
  //    local var: path
  //    default: ./files/
  // p: pointcloud file (ply)
  //    local var: pointcloud
  //    default: nube_66M.ply
  // i: cameras (aka images) file
  //    local var: images
  //    default: images_180.txt
  // t: transformation matrix
  //    local var: matrix
  //    default: transformation_matrix.txt
  // m: file with the mapping points<->cameras
  //    local var: mapping
  //    default: point_cameras.bin
  // r: file with reflectances
  //    local var: reflectance
  //    default: reflectance.bin
  // h: CUDA threads/block
  //    default: CUDA_BLK
  // n: vram in Giga Bytes
  //    local var: vram
  //    default: 1GB
  // s: Min streams to overlap async GPU tasks: comp & transf
  //    local var: nStreams
  //    default: 2

  *cb = CUDA_BLK;
  *verbose = false;
  *vram = -1.0f; // Use free mem in device by default
  *nStreams = 1;

  while ((opt = getopt(argc,argv,"d:p:i:t:m:r:h:g:0vn:s:")) != EOF)
    switch(opt) {
    case 'd':
      dataset_path = optarg;
      break;
    case 'p':
      pointcloud_file = optarg;
      break;
    case 'i':
      images_file = optarg;
      break;
    case 't':
      matrix_file = optarg;
      break;
    case 'm':
      mapping_file = optarg;
      break;
    case 'r':
      reflectance_file = optarg;
      break;
    case 'h':
      *cb = atoi(optarg);
      break;
    case 'g':
      *device = atoi(optarg);
      break;
    case '0':
      devicesnfo();
      return 1;
    case 'v':
      *verbose = true;
      break;
    case 'n':
      *vram = atof(optarg);
      break;
    case 's':
      *nStreams = atoi(optarg);
      break;
    case '?': printf("Parameters:\n -d: path to dataset (all filenames are relative to this) DEFAULT: ./files/\n -p: pointcloud file (a ply file) DEFAULT: nube_66M.ply\n -i: cameras (aka images) file DEFAULT: images_180.txt\n -t: file with transformation matrix DEFAULT: transformation_matrix.txt\n -m: file with mapping points<->cameras DEFAULT: point_cameras.txt\n -r: file with reflectances DEFAULT: reflectance.bin\n -h: CUDA threads/block DEFAULT: CUDA_BLK\n -g: CUDA device (gpu) to use\n -0: List CUDA devices and properties\n");
      return -1;
    }

  if (dataset_path.size() == 0)
    dataset_path = "files/";

  if (pointcloud_file.size() == 0)
    pointcloud_file = "nube_66M.ply";

  if (images_file.size() == 0)
    images_file = "images_180.txt";

  if (matrix_file.size() == 0)
    matrix_file = "transformation_matrix.txt";

  if (mapping_file.size() == 0)
    mapping_file = "point_cameras.bin";

  if (reflectance_file.size() == 0)
    reflectance_file = "reflectance.bin";

  return 0;
}

void get_GPU_memory_footprint(const unsigned long cloud_size, const unsigned int num_images, const float vram)
{
  // Size of cloud point dataset
  size_t cloudpoint_bytes = cloud_size * sizeof(Point);

  // Size of camera images
  size_t images_bytes = num_images * sizeof(Image);

  // Extra allocation to sort and reduce with CUB
  size_t extra_bytes = cloud_size * sizeof(unsigned int)
    + cloud_size * sizeof(unsigned int) + cloud_size * sizeof(frag)
    + cloud_size * sizeof(frag);

  std::cout << std::endl << "--GPU footprint-- (" << vram << "GB of VRAM)" << std::endl;
  std::cout << "Cloud point: " << cloudpoint_bytes / 1024 / 1024
            << " MBytes ("
            << sizeof(Point) << " Bytes per point)" << std::endl;
  std::cout << "Camera images: " << images_bytes / 1024 << " KBytes"
            << std::endl;
  std::cout << "Snapshot mapping fragment<->point: "
            << fragimages_size / 1024 / 1024
            << " MBytes/snapshot (" << sizeof(frag)
            << " Bytes/pix in 960x1280)" << std::endl;
  std::cout << "[in CPU " << num_images << " snapshots are kept in memory: "
            << (num_images * fragimages_size) / 1024.0f / 1024.0f / 1024.0f
            << " GBytes]" << std::endl;
  std::cout << "Extra bytes to sort&reduce: " << extra_bytes / 1024 / 1024
            << " MBytes (" << "2 x " << sizeof(frag) + sizeof(unsigned int)
            << " Bytes per point)" << std::endl;
  std::cout << "->TOTAL: " << (cloudpoint_bytes + images_bytes + fragimages_size
                               + extra_bytes) / 1024 / 1024 << " MBytes"
            << std::endl << std::endl;
}


void get_GPU_memory_footprintALT(const unsigned long cloud_size, const unsigned int num_images, const float vram, unsigned int nStreams)
{
  // Size of cloud point dataset
  size_t cloudpoint_bytes = cloud_size * sizeof(Point);

  // Size of camera images
  size_t images_bytes = num_images * sizeof(Image);

  size_t dyn_mem = cloudpoint_bytes // Data: complete dataset
    + fragimages_size; // Z-buffer: (dist (float) + point_id (int)) x res

  size_t lock_mem = 1024 * 1024; // Estimation: 1MB of storage permanently needed in VRAM
  size_t avail_mem = vram*1024.0f*1024.0f*1024.0f - lock_mem;

  // real data per point (databytes/point): sizeof(Point)
  // npb: max points per dataset part
  size_t npb;
  if (dyn_mem > avail_mem) { // not enough memory! => out-of-core
    if (nStreams < 2)
      nStreams = 2;
    npb = (avail_mem/nStreams - fragimages_size) / sizeof(Point);
    // (integer division truncates fractional results toward 0)
  } else {
    npb = (cloud_size / nStreams) + (cloud_size % nStreams);
  }

  size_t partitions = cloud_size / npb + (cloud_size % npb != 0);
  size_t npb_last = cloud_size % npb;
  if (npb_last == 0)
    npb_last = npb;
  // (round up to have a last partition with the rest if not integer quotient)

  size_t npb_size = npb * sizeof(Point);

  if (partitions < nStreams)
    nStreams = partitions;
  
  std::cout << std::endl << "--Footprint--" << std::endl;
  
  std::cout << std::endl << "CPU (" << vram << "GB of RAM)" << std::endl;
  std::cout << "Cloud point: " << std::setprecision(3) << cloudpoint_bytes / 1024.0f / 1024.0f / 1024.0f
            << " GiB of pinned memory ("
            << sizeof(Point) << " Bytes per point)" << std::endl;
  std::cout << "Camera images: " << images_bytes / 1024 << " KiB of pageable memory for cameras data"
            << std::endl;
  std::cout << "Snapshot mapping fragment<->point: "
            << fragimages_size / 1024 / 1024
            << " MBytes/snapshot (" << sizeof(frag)
            << " Bytes/pix in 960x1280)" << std::endl;
  std::cout << "->in CPU " << num_images << " snapshots are kept in pageable memory: "
	    << std::setprecision(3)
	    << (num_images * fragimages_size) / 1024.0f / 1024.0f / 1024.0f
            << " GiB" << std::endl;
  std::cout << "->in CPU " << nStreams << " snapshots are kept in pinned memory: "
	    << std::setprecision(3)
	    << (nStreams * fragimages_size) / 1024.0f / 1024.0f / 1024.0f
            << " GiB for txGPU->CPU" << std::endl;
  std::cout << "TOTAL pageable RAM needed: " << (images_bytes + num_images * fragimages_size) / 1024.0f / 1024.0f / 1024.0f << "GiB" << std::endl;
  std::cout << "TOTAL pinned RAM needed: " << (cloudpoint_bytes + nStreams * fragimages_size) / 1024.0f / 1024.0f / 1024.0f << "GiB" << std::endl;
  
  std::cout << std::endl << "GPU (" << vram << "GB of VRAM)" << std::endl;
  std::cout << "Cloud blocks: " << partitions << " partitions" << std::endl;
  std::cout << "->" << npb << " points/block (" << std::setprecision(3) << (float) npb_size / 1024 / 1024 / 1024
            << " GiB)" << std::endl;
  std::cout << "->" << std::setprecision(3) << (float) nStreams * npb_size / 1024 / 1024 / 1024
            << " GiB to keep " << nStreams << " blocks in GPU)" << std::endl;
  std::cout << "->" << std::setprecision(3)
	    << (nStreams * fragimages_size) / 1024.0f / 1024.0f / 1024.0f
            << " GiB for snapshots to comput" << std::endl;
  std::cout << "TOTAL VRAM needed: " << std::setprecision(3) << (nStreams * (npb_size + fragimages_size)) / 1024.0f / 1024.0f / 1024.0f << "GiB" << std::endl;

  std::cout << std::endl << "--/Footprint--" << std::endl << std::endl;
}
