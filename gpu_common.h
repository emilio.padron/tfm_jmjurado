#ifndef _gpu_common_h
#define _gpu_common_h

#include <assert.h>
#include "cuda_runtime.h"

//Consts
extern const int CUDA_BLK;  // Tamaño predeterm. de bloque de hilos CUDA
extern size_t max_grid_x;   // Max number of CUDA blks in x dimension of Grid

// Fragment info (projection of a 3D point)
//  unsigned int dist;     // -> float reinterpreted as an unsigned integer
//  unsigned int pointid;
//  Both unsigned integers (32bits) packed in an unsigned long (64bits)
typedef unsigned long long int frag_glm;

void devicenfo(void);
size_t deviceinit(int dev, unsigned int cb);
void checkparams(unsigned int* n, unsigned int* cb);
int parse_args_gpuver(int argc, char *argv[], int *device, unsigned int *cb, float *vram, size_t *nStreams, bool *verbose);
void get_GPU_memory_footprint(const unsigned long cloud_size, const unsigned int num_images, const float vram);
void get_GPU_memory_footprintALT(const unsigned long cloud_size, const unsigned int num_images, const float vram, const unsigned int nstreams);

// Convenience function for checking CUDA runtime API results
// can be wrapped around any runtime API call. No-op in release builds.
#define cuda(ans) { checkCuda((ans), __FILE__, __LINE__); }
inline
cudaError_t checkCuda(cudaError_t code, const char *file, int line)
{
#if defined(DEBUG) || defined(_DEBUG) || defined(_DEBUG_)
  if (code != cudaSuccess) {
    fprintf(stderr, "\n\n\nCUDA Runtime Error: %s %s %d\n\n\n",
            cudaGetErrorString(code), file, line);
    assert(code == cudaSuccess);
  }
#endif
  return code;
}

#endif
