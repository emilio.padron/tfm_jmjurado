#include <cstdio>
#include <cub/cub.cuh>
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include "cpu.h"
#include "gpu_common.h"
#include "gpu_v31.h"

// CustomMin functor for reduction
struct CustomMin {
  CUB_RUNTIME_FUNCTION __forceinline__
  __device__ frag operator()(const frag &a, const frag &b) const {
    return (b.dist < a.dist) ? b : a;
  }
};

unsigned int GPU_mapping_and_occlusion(Point* cloud, const unsigned long cloud_size, Image* images, const unsigned int num_images, float* v_rot_matrix, frag snapshots[], const unsigned int blk_size, gputime *gpu_times, size_t gpu_memory)
{
#ifdef _SYNCTIME_
  auto start = std::chrono::steady_clock::now();

  std::chrono::duration<double> CPUGPUtransf_t, project_t, sort_t, reduce_t,
    fill_t, GPUCPUtransf_t, merge_t;
#endif

  size_t dynamic_memory =  cloud_size * sizeof(Point);
  size_t lock_memory = 1024 * 1024; // Estimation: 1MB for cub's sort&reduce temp storage

  // CPU->GPU transfer of the complete info about the cameras
  Image *d_images;
  size_t numBytes = num_images * sizeof(Image);
  cuda( cudaMalloc((void**)&d_images, numBytes) );
  cuda( cudaMemcpy(d_images, images, numBytes, cudaMemcpyHostToDevice)) ;
  lock_memory += numBytes;

  // CPU->GPU transfer of the rotation matrix
  float* d_rotation_matrix;
  numBytes = 3 * 3 * sizeof(float);
  cuda( cudaMalloc((void**)&d_rotation_matrix, numBytes) );
  cuda( cudaMemcpy(d_rotation_matrix, v_rot_matrix, numBytes, cudaMemcpyHostToDevice) );
  lock_memory += numBytes;

  // Fragments projected for each camera
  unsigned int *d_frag_key; // frag_key = pixelid = pixelx * 1280 + pixely
  frag *d_frag_value; // (z value, point id): distancia del punto a la cámara, id punto

  // Extra allocation to sort-by-key previous arrays
  unsigned int *d_frag_key_extra;
  frag *d_frag_value_extra;

  dynamic_memory +=
    + 2 * cloud_size * sizeof(unsigned int) // d_frag_key + d_frag_key_extra
    + 2 * cloud_size * sizeof(frag); //d_frag_value + d_frag_value_extra

  //Cálculo del número de particiones y del tamaño de cada partición:
  //dynamic_mem: Memoria necesaria para procesar la nube de datos completa
  //lock_mem: Memoria que necesitamos no relacionada con tamaño de la nube:
  //gpu_mem: Memoria disponible en la GPU para procesar la nube, descontando la lock_mem
  gpu_memory -= lock_memory;

  std::cout << "Locked mem: " << lock_memory / 1024.0f << " KB. Free mem: " << gpu_memory / 1024 / 1024 << " MB. Required mem: " << dynamic_memory / 1024.0f / 1024.0f << " MB." << std::endl;

  unsigned int partitions = 1;
  if (dynamic_memory > gpu_memory) {
    partitions += dynamic_memory / gpu_memory;
  }

  // size: number of points in each dataset partition
  size_t size = 1 + cloud_size / partitions; //tamaño del vector que cabe en la memoria de la GPU para cada iteración

  std::cout << "Number of point cloud partitions: " << partitions
            << " (" << size << " Bytes/partition)" << std::endl;

  //Bloque de la nube
  Point *c_block;
  cuda( cudaMalloc((void**) &c_block, size * sizeof(Point)) );

  cuda( cudaMalloc((void**) &d_frag_key, size * sizeof(unsigned int)) );
  cuda( cudaMalloc((void**) &d_frag_key_extra, size * sizeof(unsigned int)) );
  cuda( cudaMalloc((void**) &d_frag_value, size * sizeof(frag)) );
  cuda( cudaMalloc((void**) &d_frag_value_extra, size * sizeof(frag)) );

  // Create a set of DoubleBuffers to wrap pairs of device pointers
  // to sort-by-key the arrays (pixelid is the key)
  cub::DoubleBuffer<unsigned int> d_keys(d_frag_key, d_frag_key_extra);
  cub::DoubleBuffer<frag> d_values(d_frag_value, d_frag_value_extra);

  // Temporary storage required for sorting and reducing
  void *d_temp_storage = NULL;
  size_t temp_storage_bytes = 0, needed_temp_bytes = 0;

  int *d_num_runs_out;
  cuda( cudaMalloc((void**) &d_num_runs_out, sizeof(int)) );

  CustomMin reduction_op; // reduction operation: min value

  // Dimension de hilos por bloque
  dim3 dimBlock(blk_size);

  // Rejilla unidimensional
  dim3 dimGrid((size + dimBlock.x - 1) / dimBlock.x);                // 1 thread/point
  dim3 dimGrid2((images_resolution + dimBlock.x - 1) / dimBlock.x);; // 1 thread/pixel

  gpu_times->addpartitions(partitions);

#ifdef _SYNCTIME_
  cuda( cudaDeviceSynchronize() );
  auto end = std::chrono::steady_clock::now();
  gpu_times->preliminary = end - start;
#endif

  // snapshot for current camera
  frag *snapshot = snapshots,
    //Estructura temporal para chequear los fragmentos válidos en cada cámara
    *temp_snapshot;
  temp_snapshot = (frag *) malloc(fragimages_size);

  size_t desp; //iterador para el desplazamiento en la nube

  for (unsigned int c = 0; c < num_images; c++) {
    numBytes = size; // numBytes is now number of points in current partition
    desp = 0;

    //Se inicializa el snapshot de la cámara c con un valor alto de distancia
    memset(snapshot, 0x77, fragimages_size);

    for (unsigned int i = 0; i < partitions; i++) {
#ifdef _SYNCTIME_
      auto start_it = std::chrono::steady_clock::now();
#endif

      /*Si es la última partición se debe chequear de nuevo el tamaño
        del bloque /porque es muy probable que el tamaño de la nube no
        sea disible por el número de particiones*/

      if (i == partitions - 1 && partitions > 1) {
        numBytes = cloud_size - desp;
      }

      //Tranferencia de bloque a la GPU
      cuda( cudaMemcpy(c_block, &cloud[desp], numBytes * sizeof(Point), cudaMemcpyHostToDevice) );
      desp += numBytes;

#ifdef _SYNCTIME_
      cuda( cudaDeviceSynchronize() );
      auto mid0 = std::chrono::steady_clock::now();
#endif

      // First stage for camera #c: mapping (aka projection)
      // Get fragments for each projected point in c_projections
      // Multiple 3D points can be projected to the same fragment (with different z-component, dist)

      GPUmapping_kernel << <dimGrid, dimBlock >> > (numBytes, c, c_block, d_images, d_rotation_matrix, d_frag_key, d_frag_value);

#ifdef _SYNCTIME_
      cuda( cudaPeekAtLastError() );
      cuda( cudaDeviceSynchronize() );
      auto mid1 = std::chrono::steady_clock::now();
#endif

      // Second stage for camera #c: check occlusion and update
      // general zbuffer Fragments for the same pixel are reduced by
      // distance to camera

      // First step: sort-by-key (pixelid is the key)

      // Determine temporary device storage requirements
      cub::DeviceRadixSort::SortPairs(NULL, needed_temp_bytes, d_keys, d_values, numBytes);

      // Allocate temporary storage
      if (needed_temp_bytes > temp_storage_bytes) {
        cuda( cudaFree(d_temp_storage) );
        temp_storage_bytes = needed_temp_bytes;
        cuda( cudaMalloc(&d_temp_storage, temp_storage_bytes) );
      }

      // Run sorting operation
      cub::DeviceRadixSort::SortPairs(d_temp_storage, temp_storage_bytes, d_keys, d_values, numBytes);

#ifdef _SYNCTIME_
      cuda( cudaPeekAtLastError() );
      cuda( cudaDeviceSynchronize() );
      auto mid2 = std::chrono::steady_clock::now();
#endif

      // Second step: reduce-by-key

      // Determine temporary device storage requirements
      cub::DeviceReduce::ReduceByKey(NULL, needed_temp_bytes, d_frag_key, d_frag_key_extra, d_frag_value, d_frag_value_extra, d_num_runs_out, reduction_op, numBytes);

      // Allocate temporary storage
      if (needed_temp_bytes > temp_storage_bytes) {
        cuda( cudaFree(d_temp_storage) );
        temp_storage_bytes = needed_temp_bytes;
        cuda( cudaMalloc(&d_temp_storage, temp_storage_bytes) );
      }

      // Run reduce-by-key
      cub::DeviceReduce::ReduceByKey(d_temp_storage, temp_storage_bytes, d_frag_key, d_frag_key_extra, d_frag_value, d_frag_value_extra, d_num_runs_out, reduction_op, numBytes);

#ifdef _SYNCTIME_
      cuda( cudaPeekAtLastError() );
      cuda( cudaDeviceSynchronize() );
      auto mid3 = std::chrono::steady_clock::now();
#endif

      // Third stage for camera #c: copy pixels to final positions
      // filling the missing keys with 0 (pixels not covered by any fragment)
      // Every CUDA thread copies a pixel

      // Initialize destination buffer to UINT_MAX
      cuda( cudaMemset(d_frag_value, UINT_MAX, fragimages_size) );

      // Now just for debugging:
      // -> obtain number of pixels after reduction
      // unsigned int ncoveredpixels;
      // cuda( cudaMemcpy(&ncoveredpixels, d_num_runs_out, sizeof(int), cudaMemcpyDeviceToHost) ); // GPU -> CPU
      // ncoveredpixels = ncoveredpixels - 1; // Last element after reduction is UINT_MAX
      // printf("Part#%2d - Camera#%3d: %d covered pixels\n", i, c, ncoveredpixels);

      // Kernel to copy each pixel info into its final location
      // generating the final 960x1280 snapshot with dist+pointid per pixel
      GPUgenerate_snapshot << <dimGrid2, dimBlock >> > (d_num_runs_out, d_frag_key_extra, d_frag_value_extra /*src*/, d_frag_value /*dst*/);

#ifdef _SYNCTIME_
      cuda( cudaPeekAtLastError() );
      cuda( cudaDeviceSynchronize() );
      auto mid4 = std::chrono::steady_clock::now();
#endif

      // Transfer snapshot to CPU
      cuda( cudaMemcpy(temp_snapshot, d_frag_value, fragimages_size, cudaMemcpyDeviceToHost) ); // GPU -> CPU

#ifdef _SYNCTIME_
      cuda( cudaDeviceSynchronize() );
      auto mid5 = std::chrono::steady_clock::now();
#endif

      //Merge
      for (size_t j = 0; j < images_resolution; j++) {
        if(temp_snapshot[j].dist < snapshot[j].dist){
          snapshot[j] = temp_snapshot[j];
        }
      }

#ifdef _SYNCTIME_
      cuda( cudaDeviceSynchronize() );
      auto end_it = std::chrono::steady_clock::now();

      CPUGPUtransf_t = mid0 - start_it;
      project_t = mid1 - mid0;
      sort_t = mid2 - mid1;
      reduce_t = mid3 - mid2;
      fill_t = mid4 - mid3;
      GPUCPUtransf_t = mid5 - mid4;
      merge_t = end_it - mid5;

      gpu_times->camtime[c].CPUGPUtransf += CPUGPUtransf_t;
      gpu_times->camtime[c].project += project_t;
      gpu_times->camtime[c].sort += sort_t;
      gpu_times->camtime[c].reduce += reduce_t;
      gpu_times->camtime[c].fill += fill_t;
      gpu_times->camtime[c].GPUCPUtransf += GPUCPUtransf_t;
      gpu_times->camtime[c].merge += merge_t;

      gpu_times->partime[i].CPUGPUtransf += CPUGPUtransf_t;
      gpu_times->partime[i].project += project_t;
      gpu_times->partime[i].sort += sort_t;
      gpu_times->partime[i].reduce += reduce_t;
      gpu_times->partime[i].fill += fill_t;
      gpu_times->partime[i].GPUCPUtransf += GPUCPUtransf_t;
      gpu_times->partime[i].merge += merge_t;
#endif

    }

    snapshot += images_resolution; // move to snapshot for next camera
  }

  cuda( cudaFree(d_temp_storage) );
  cuda( cudaFree(d_num_runs_out) );
  cuda( cudaFree(d_frag_value_extra) );
  cuda( cudaFree(d_frag_value) );
  cuda( cudaFree(d_frag_key_extra) );
  cuda( cudaFree(d_frag_key) );
  cuda( cudaFree(d_rotation_matrix) );
  cuda( cudaFree(d_images) );
  cuda( cudaFree(c_block) );
  free(temp_snapshot);

  // Ensure all CUDA stuff has really finished!
  cuda( cudaDeviceSynchronize() );

  return partitions;
}


__device__ void GPU_matrix_trans(float* trans, float* matrix)
{
  for (int c = 0; c < 3; c++) {
    for (int d = 0; d < 3; d++) {
      trans[c * 3 + d] = matrix[d * 3 + c];
    }
  }
}


__device__ void GPU_mult_matrix(float* result, float* m1, float* m2, int n)
{
  float res;
  for (int r = 0; r < n; ++r) {
    for (int c = 0; c < n; ++c) {
      res = 0;
      for (int k = 0; k < n; ++k) {
        res += m1[r * n + k] * m2[k * n + c];
      }
      result[r * n + c] = res;
    }
  }
}


__global__ void GPUmapping_kernel(const unsigned long npoints, const unsigned int cam_id, Point c_cloud[], Image c_images[], float c_rotation_matrix[], unsigned int frag_pixelid[], frag frag[])
{
  int task_id = blockIdx.x * blockDim.x + threadIdx.x;

  if (task_id < npoints) {

    /*****Transformación geométrica******/

    float trans_m1[9];//matriz transpuesta de la matriz de rotación de la cámara
    float trans_m2[9];//matriz transpuesta de la matriz de rotación de la transformación ICP
    float rot_m[9];

    //Transposición de matrices
    GPU_matrix_trans(trans_m1, c_images[cam_id].rotation_matrix);
    GPU_matrix_trans(trans_m2, c_rotation_matrix);

    //Multiplicación de matrices
    GPU_mult_matrix(rot_m, trans_m1, trans_m2, 3);

    //Traslación y Rotación
    float res[3];
    float temp;
    for (int r = 0; r < 3; ++r) {
      temp = 0;
      for (int c = 0; c < 3; ++c) {
        temp += rot_m[r * 3 + c] * (c_cloud[task_id].position[c] - c_images[cam_id].position[c]);
      }
      res[r] = temp;
    }

    /*****Proyección*******/
    //Fisheye Polynomial
    float D[4] = { c_images[cam_id].D[0], c_images[cam_id].D[1], c_images[cam_id].D[2], c_images[cam_id].D[3] };
    //Fisheye Affine Matrix
    float A[4] = { c_images[cam_id].A[0], c_images[cam_id].A[1], c_images[cam_id].A[2], c_images[cam_id].A[3] };

    float cx = c_images[cam_id].cx;
    float cy = c_images[cam_id].cy;

    //Punto 3D
    float x = res[0];
    float y = res[1];
    float z = res[2];

    float r = sqrt(pow(x, 2) + pow(y, 2));
    float theta = (2 / M_PI) * atan(r / z);
    float poly = D[0] + D[1] * theta + D[2] * theta * theta + D[3] * theta * theta * theta;
    float xh = (poly * x) / r;
    float yh = (poly * y) / r;

    //Coordenadas de la imagen

    int px = lrintf(A[2] * xh + A[3] * yh + cy);
    int py = lrintf(A[0] * xh + A[1] * yh + cx);

    if (px < 0 || px > 959 || py < 0 || py > 1279) {
      frag_pixelid[task_id] = UINT_MAX;
    } else {
      //Pixel en la imagen proyectado por el Punto 3D
      frag_pixelid[task_id] = lrintf(px) * 1280 + lrintf(py);

      frag[task_id].dist = sqrtf(pow((x - c_images[cam_id].position[0]), 2) + pow((y - c_images[cam_id].position[1]), 2) + pow((z - c_images[cam_id].position[2]), 2));
      frag[task_id].pointid = task_id;
    }

  }
}


__global__ void GPUgenerate_snapshot(const int * const nelements, unsigned int src_pixelid[], frag src_value[], frag dst_value[])
{
  const int task_id = blockIdx.x * blockDim.x + threadIdx.x;

  if (task_id < *nelements - 1) {
    unsigned int pixel_id = src_pixelid[task_id];

    dst_value[pixel_id].dist = src_value[task_id].dist;
    dst_value[pixel_id].pointid = src_value[task_id].pointid;
  }
}
