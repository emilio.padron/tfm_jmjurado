## GPU simple version [v2] (gpu.cu main_gpu.cpp)

> Single GPU
>
> Complete point cloud stored in GPU: (3xfloat + 3xint) x #points

1. CPU->GPU transfers:
   - Complete dataset
   - Camera info for all cameras
   - Rotation matrix

For every camera:

2. GPU Mapping: Model-View transformation + Projection

    ```
        GPUmapping_kernel
    ```

    - One fragment is obtained for *every* point in the cloud.
    - Fragments out of 960x1280 are marked after computation with pixel_id = UINT_MAX

3. GPU Sort-by-key (key = pixel_id)

    ```
        cub::DeviceRadixSort::SortPairs
    ```

    - Fragments are sorted in ascendant pixel_id
    - Fragments covering the same pixel (identical pixel_id) are contiguous after this operation

4. GPU Reduce-by-key (key = pixel_id, op = min dist)

    ```
        cub::DeviceReduce::ReduceByKey
    ```

    - Only the fragment with min dist is kept for each pixel_id

5. GPU Filling missing keys with 0 (pixels not covered by any fragment for this camera)

    ```
        GPUgenerate_snapshot
    ```

    - The final 960x1280 snapshot with dist+pointid per pixel is generated

6. GPU->CPU transfer of the image

    - The 960x1280 image obtained for this camera is transferred to CPU

## GPU out-of-core version [v31] (gpu_v31.cu main_gpu_v31.cpp)

> Single GPU
>
> A block of the point cloud stored in GPU
>
> Only an image (960x1280) is stored in GPU
>
> Do NOT Overlap transfer and computation

For every block in dataset

1. CPU->GPU transfers:
   - Dataset block
   - Camera info for all cameras (only the first CPU->GPU transfer)
   - Rotation matrix (only the first CPU->GPU transfer)

For every camera:

2. GPU Mapping for points in block: Model-View transformation + Projection
3. GPU Sort-by-key fragments from points in block (key = pixel_id)
4. GPU Reduce-by-key fragments from points in block (key = pixel_id, op = min dist)
5. GPU Filling missing keys with 0 (pixels not covered by any fragment for this camera in this block of points)
6. GPU->CPU transfer of the image for this camera and block

+ A temporary image for each camera is maintained in CPU RAM
    - kind of a z-buffer for camera, with min dist so far with processed blocks
    - 960x1280x8bytes(1float+1uint) = 9.37MB per camera
    - These z-buffers are updated with every new snapshot received for each camera: each fragment is compared to z-buffer value and discarded (pixel_id = UINT_MAX) if already occluded.


## GPU out-of-core version [v32] (gpu_v32.cu main_gpu_v32.cpp)

> Single GPU
>
> A block of the point cloud stored in GPU
>
> Only an image (960x1280) is stored in GPU
>
> Overlap transfer and computation

Same algorithm than previous v31 version, but now a couple of CUDA Streams
are used to overlap asynchronous transfers and GPU computation:

   - the available memory is split in 2 halves, one is the current one being
   computed while the other one is being transferred.


## Multi-GPU out-of-core version [Work in Progress]

> Multiple GPU
>
> A block of the point cloud is stored in each GPU each iteration
>
> Only an image (960x1280) is stored in each GPU
>
> Overlap transfer and computation

For every GPU, a block of dataset is transfered from CPU

1. CPU->GPU transfers:
   - Dataset block
   - Camera info for all cameras (only the first CPU->GPU transfer)
   - Rotation matrix (only the first CPU->GPU transfer)

For every camera, for this block in each GPU

2. GPU Mapping for points in block: Model-View transformation + Projection
3. GPU Sort-by-key fragments from points in block (key = pixel_id)
4. GPU Reduce-by-key fragments from points in block (key = pixel_id, op = min dist)
5. GPU Filling missing keys with 0 (pixels not covered by any fragment for this camera in this block of points)
6. GPU->CPU transfer of the image for this camera and block

+ A temporary image for each camera is maintained in CPU RAM
    - kind of a z-buffer for camera, with min dist so far with processed blocks

TODO: find out how to merge the results obtained from each GPU/block/camera

ALT version: Stored an image (960x1280) for each camera in each GPU, instead of just one (a temp z-buffer per GPU)
-> A lot of memory if number of cameras is thousands
-> Simplify *dramatically* the scheduling and reduce GPU->CPU and CPU->GPU transfers
