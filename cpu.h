#ifndef _cpu_h
#define _cpu_h

#include <vector>
#include <glm/mat3x3.hpp>
#include "rply.h"
using namespace std;
using namespace glm;

struct spectral_point {
  float *nir;
  float *red;
  float *green;
  float *reg;
};

struct c_p {
  int id_point;
  int id_camara;
};

struct Point_cameras {
  int id_point;
  int *cameras;
  unsigned int num_cameras;
};

struct Point {
  float position[3];
#ifdef _NOTONLYVERT_
  int r;
  int g;
  int b;
#endif
};

struct Image {
  float position[3];
  float rotation_matrix[9];
  int band;
  float A[4];
  float D[4];
  float cx;
  float cy;
};

struct Image_glm {
  mat3 rotationMatrix;
  vec3 cameraPosition;
  vec4 fisheyeAffine;
  vec4 fisheyePolynomial;
  vec2 principalPoint;
};

struct Reflectance_matrix {
  int band;
  float* m;
};

struct Band {
  int band;
  float A[4];
  float D[4];
  float cx;
  float cy;
};

struct Projection {
  float px; //pixelx
  float py; //pixely
  int c; //cámara sobre la que se proyecta
  int id_point;
  float dist; //distancia del punto a la cámara
  bool occluded;
};

// Fragment info (projection of a 3D point)
// 8 bytes per fragment (single precision)
// -> unsigned int frag_pixelid; // pixelid = pixelx * 1280 + pixely
// -> float frag_dist; // z value: distancia del punto a la cámara
// -> unsigned int frag_pointid; // 3D point id
typedef union {
  struct {
    unsigned int pointid;
    float dist;
  };
  unsigned long long int dist64;
} frag;

// typedef union {
//   float dist[2];                 // dist[0] = lowest
//   int pointid[2];                // pointid[1] = lowIdx
//   unsigned long long int dist64; // for atomic update
// } frag;

extern const size_t fragimages_size; // Size of image buffers

//Variables globales
extern Point *cloud;
extern int currentp;
extern const long batchsize;
extern int num_images;
extern int num_projections;

//Consts
extern const size_t images_resolution;
extern const int images_resolution_bits;

//Dataset files
extern std::string dataset_path, pointcloud_file, images_file, matrix_file,
  mapping_file, reflectance_file;

long read_cloud(const long howmany = 0);
long read_cloud_UM(const long howmany = 0);
Image* read_images();
Image_glm* read_images_glm();
vector<Point_cameras> read_cameras_point();
void mult_matrix(float* result, float* m1, float* m2, int n);
void read_transformation_matrix(float *result);
Reflectance_matrix* read_reflectance_matrix();
void mapping_and_occlusion(Point *cloud, const unsigned long cloud_points, Image images[], const unsigned int num_images, float* v_rot_matrix, frag snapshots[]);
void mapping(Projection** projections, Point *cloud, vector<Point_cameras> point_cameras, Image *images, float *rotation_matrix);
void oclussion_test(Projection** projections, vector<Point_cameras> point_cameras);
int parse_args(int argc, char *argv[]);
void show_dataset_process();
void show_invocation(int argc, char* argv[]);
unsigned int check_snapshot(frag snapshot[]);

void print_snapshot(frag snapshot[], unsigned int max = 1228800);

int vertex_x(p_ply_argument argument);
int vertex_y(p_ply_argument argument);
int vertex_z(p_ply_argument argument);
#ifdef _NOTONLYVERT_
int color_r(p_ply_argument argument);
int color_g(p_ply_argument argument);
int color_b(p_ply_argument argument);
#endif

#endif
