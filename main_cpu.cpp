#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <climits>
#include <cmath>
#include <chrono>
#include <iostream>
#include <vector>
#include "cpu.h"
using namespace std;

/*
  Funci�n principal
*/
int main(int argc, char* argv[])
{
  long npoints;

  show_invocation(argc, argv);

  if (parse_args(argc, argv) != 0)
    return -1;

  show_dataset_process();

  /*LECTURA DE FICHEROS*/
  auto start = std::chrono::steady_clock::now();

  //Se lee la nube de puntos
  npoints = read_cloud();

  //Se lee fichero 1: im�genes
  Image *images = read_images();

  //Se lee fichero 2: c�maras por punto
  //Skipped in this new CPU code using a z-buf approach

  //Se lee fichero 3: matriz de transformaci�n (ICP)
  float *transf_matrix;
  transf_matrix = (float*)malloc(4 * 4 * sizeof(float));
  read_transformation_matrix(transf_matrix);
  //Se lee fichero 4: matrices de reflectancia
  //Reflectance_matrix* reflectance_v = read_reflectance_matrix();

  auto end = std::chrono::steady_clock::now();

  std::cout << "Files loaded in " << (std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()) / 1000.0 << " ms" << std::endl;

  printf("Cloud size = %lu (%lu MBytes)\n", npoints, npoints * sizeof(Point) / 1024 / 1024);
  printf("Number of images = %u (960 x 1280 = %ld)\n", num_images, images_resolution);

  //Calculo de la matriz de rotaci�n y escala (ICP)
  float* rotation_matrix;
  rotation_matrix = (float*)malloc(3 * 3 * sizeof(float));
  float v_scale[3];
  for (int i = 0; i < 3; i++) {
    float s_index = 0;
    for (int j = 0; j < 3; j++) {
      s_index += transf_matrix[j * 4 + i] * transf_matrix[j * 4 + i];
    }
    v_scale[i] = sqrtf(s_index);
  }
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      rotation_matrix[j * 3 + i] = transf_matrix[j * 4 + i] / v_scale[i];
    }
  }


  /****************CPU*********************/

  // Buffer for all snapshots
  frag *snapshots = (frag *) malloc(num_images * fragimages_size);
  memset(snapshots, UINT_MAX, num_images * fragimages_size);

  start = std::chrono::steady_clock::now();
  mapping_and_occlusion(cloud, npoints, images, num_images, rotation_matrix, snapshots);
  end = std::chrono::steady_clock::now();

  std::cout << "Total elapsed time for Mapping & Occlusion: " << (std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()) / 1000.0 << " ms" << std::endl;

  return(0);
}
