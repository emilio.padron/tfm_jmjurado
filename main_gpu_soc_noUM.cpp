#include <cstdio>
#include <cstdlib>
#include <cmath>
#include <cfloat>
#include <chrono>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <vector>
#include "cpu.h"
#include "rply.h"
#include "gpu_common.h"
#include "gpu_soc.h"
using namespace std;

void printout_report(gputime *gpu_times, bool verbose);
Image* read_images_UM();
float *read_transformation_matrix_UM();

/*
  Función principal
*/
int main(int argc, char* argv[])
{
  long npoints;    // Points in dataset
  int device = 0;  // CUDA Device
  unsigned int cb; // CUDA Threads/Block
  float vram;      // Max GPU mem to use (GB)
  size_t nStreams; // Min CUDA Streams
  bool verbose;

  show_invocation(argc, argv);

  if (parse_args_gpuver(argc, argv, &device, &cb, &vram, &nStreams, &verbose) != 0)
    return -1;

  show_dataset_process();

  size_t freebytes = deviceinit(device, cb);
  if (vram > 0.0) {
    size_t bytes = vram * 1024 * 1024 * 1024; // GB to B
    if (bytes < freebytes)
      freebytes = bytes;
  } else {
    vram = freebytes / 1024.0f / 1024.0f / 1024.0f; // B to GB
  }

  /*LECTURA DE FICHEROS*/
  auto start = std::chrono::steady_clock::now();

  //Se lee la nube de puntos (no UM, regular malloc)
  npoints = read_cloud();

  //Se lee fichero 1: imágenes
  Image *images = read_images();

  //Se lee fichero 2: cámaras por punto / puntos por cámara
  //Skipped in pure gpu version

  //Se lee fichero 3: matriz de transformación (ICP)
  float *transf_matrix;
  transf_matrix = (float*)malloc(4 * 4 * sizeof(float));
  read_transformation_matrix(transf_matrix);
  //Se lee fichero 4: matrices de reflectancia
  //Reflectance_matrix* reflectance_v = read_reflectance_matrix();

  auto end = std::chrono::steady_clock::now();

  std::cout << "Files (except cloud point dataset) loaded in " << (std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()) / 1000.0 << " ms" << std::endl;

  printf("Number of images = %u (960 x 1280 = %ld)\n", num_images, images_resolution);

  std::cout << "[in CPU " << num_images << " snapshots are kept in memory: "
            << (num_images * fragimages_size) / 1024.0f / 1024.0f / 1024.0f
            << " GBytes]" << std::endl;

  get_GPU_memory_footprint(npoints, num_images, vram);

  //Calculo de la matriz de rotación y escala (ICP)
  float rotation_matrix[9];
  float v_scale[3];
  for (int i = 0; i < 3; i++) {
    float s_index = 0;
    for (int j = 0; j < 3; j++) {
      s_index += transf_matrix[j * 4 + i] * transf_matrix[j * 4 + i];
    }
    v_scale[i] = sqrtf(s_index);
  }
  for (int i = 0; i < 3; i++) {
    for (int j = 0; j < 3; j++) {
      rotation_matrix[j * 3 + i] = transf_matrix[j * 4 + i] / v_scale[i];
    }
  }

  //Transposición de matriz ICP
  float transposed_ICP[9];

  for (int c = 0; c < 3; c++) {
    for (int d = 0; d < 3; d++) {
      transposed_ICP[c * 3 + d] = rotation_matrix[d * 3 + c];
    }
  }

  // Multiplicación de transpuesta de matriz de cada cámara por matriz ICP
  for (int i = 0; i < num_images; ++i) {
    float transposed[9];

    for (int c = 0; c < 3; c++) {
      for (int d = 0; d < 3; d++) {
        transposed[c * 3 + d] = images[i].rotation_matrix[d * 3 + c];
      }
    }

    float res;
    for (int r = 0; r < 3; ++r) {
      for (int c = 0; c < 3; ++c) {
        res = 0;
        for (int k = 0; k < 3; ++k) {
          res += transposed[r * 3 + k] * transposed_ICP[k * 3 + c];
        }
        images[i].rotation_matrix[r * 3 + c] = res;
      }
    }
  }

  /****************GPU*********************/

  // Buffer for all snapshots (no UM, regular malloc)
 frag *snapshots = (frag *) malloc(num_images * fragimages_size);

  //Se inicializan snapshots con un valor alto de distancia
  //  memset(snapshots, UINT_MAX, num_images * fragimages_size);
  for(int i=0; i<num_images*images_resolution; ++i) {
    snapshots[i].pointid = UINT_MAX;
    snapshots[i].dist = FLT_MAX;
  }

  gputime gpu_times(num_images);

  start = std::chrono::steady_clock::now();

  size_t partitions = GPU_mapping_and_occlusion(cloud, npoints, images, num_images, snapshots, cb, &gpu_times, nStreams, freebytes);

  end = std::chrono::steady_clock::now();

  gpu_times.kernels_with_transfers = end - start;

  std::cout << std::endl << pointcloud_file << " " << images_file << " h" << cb;
  std::cout << " n" << vram << " s" << nStreams << " p" << partitions << " ";
  std::cout << std::fixed << std::setprecision(2) << (std::chrono::duration_cast<std::chrono::microseconds>(gpu_times.kernels_with_transfers).count()) / 1000.0 / 1000.0 << "s" << std::endl;

  return(0);
}

void printout_report(gputime *gpu_times, bool verbose)
{
  std::cout << "Total Time for Mapping & Occlusion (kernels + transfers): " << (std::chrono::duration_cast<std::chrono::microseconds>(gpu_times->kernels_with_transfers).count()) / 1000.0 << " ms" << std::endl;

  if (verbose) {
    std::cout << std::endl << "Times per camera (comp + transf):" << std::endl;

    for(int i=0; i<num_images; i++) {
      std::cout << std::setprecision(2) << std::fixed;
      std::cout << "CAM#" << std::setw(2) << i                            \
                << ": " << (std::chrono::duration_cast<std::chrono::microseconds>(gpu_times->camtime[i].allcomputation).count()) /1000.0 \
                << " ms" << std::endl;
    }
  }

#ifdef _SYNCTIME_
  std::cout << "-> Preliminary time (Some CPU->GPU & allocations): " << (std::chrono::duration_cast<std::chrono::microseconds>(gpu_times->preliminary).count()) / 1000.0 << " ms" << std::endl;
#endif
}


long read_cloud_UM(void)
{
  static p_ply ply;
  long cloud_size;

  ply = ply_open((dataset_path + pointcloud_file).c_str(), NULL, 0, NULL);

  if (ply_read_header(ply)) {
    ply_get_element_info(ply_get_next_element(ply, 0), 0, &cloud_size);

    // Unified memory allocated for INPUT
    cudaError_t status = cudaMallocManaged((void**)&cloud, cloud_size * sizeof(Point), cudaMemAttachHost);
    if (status != cudaSuccess) {
      printf("Error allocating unified memory for point cloud!\n");
      printf("Cloud not loaded! :-/");

      return 0;
    }

    ply_set_read_cb(ply, "vertex", "x", vertex_x, NULL, 0);
    ply_set_read_cb(ply, "vertex", "y", vertex_y, NULL, 0);
#ifdef _NOTONLYVERT_
    ply_set_read_cb(ply, "vertex", "z", vertex_z, NULL, 0);
    ply_set_read_cb(ply, "vertex", "red", color_r, NULL, 0);
    ply_set_read_cb(ply, "vertex", "green", color_g, NULL, 0);
    ply_set_read_cb(ply, "vertex", "blue", color_b, NULL, 1);
#else
    ply_set_read_cb(ply, "vertex", "z", vertex_z, NULL, 1);
#endif
  }

  if (ply_read(ply)) {
    ply_close(ply);
  }

  printf("Cloud loaded: %ld points\n", cloud_size);

  return cloud_size;
}


Image* read_images_UM()
{
  ifstream file((dataset_path + images_file).c_str());
  string line;

  getline(file, line);
  stringstream iss1(line);
  iss1 >> num_images;
  Image* images;

  // Unified memory allocated for INPUT
  cudaError_t status = cudaMallocManaged((void**)&images, num_images * sizeof(Image), cudaMemAttachHost);
  if (status != cudaSuccess) {
    printf("Error allocating unified memory for images data!\n");
    printf("Images not loaded! :-/");

    return 0;
  }

  int it = 0;
  while (getline(file, line))
    {
      stringstream iss(line);

      Image im;
      iss >> im.position[0] >> im.position[1] >> im.position[2];
      for (int r = 0; r < 3; ++r) {
        getline(file, line);
        stringstream iss2(line);
        for (int c = 0; c < 3; ++c) {
          iss2 >> im.rotation_matrix[r * 3 + c];
        }
      }
      getline(file, line);
      stringstream iss2(line);
      iss2 >> im.band;

      getline(file, line);
      stringstream iss3(line);
      iss3 >> im.D[0] >> im.D[1] >> im.D[2] >> im.D[3];

      getline(file, line);
      stringstream iss4(line);
      iss4 >> im.A[0] >> im.A[1] >> im.A[2] >> im.A[3];

      getline(file, line);
      stringstream iss5(line);
      iss5 >> im.cx >> im.cy;

      images[it] = im;
      ++it;
    }
  printf("File 1 loaded \n");
  return images;
}


float *read_transformation_matrix_UM()
{
  float m1[16];
  float m2[16];

  ifstream file((dataset_path + matrix_file).c_str());
  string line;
  if (file.is_open()) {
    //M1
    int it = 0;
    getline(file, line);
    while (line[0] != '*')
      {
        stringstream linestream(line);
        for (int c = 0; c < 4; c++) {
          linestream >> m1[it * 4 + c];
        }
        getline(file, line);
        ++it;
      }

    //M2
    it = 0;
    while (getline(file, line))
      {
        stringstream linestream(line);
        for (int c = 0; c < 4; c++) {
          linestream >> m2[it * 4 + c];
        }
        ++it;
      }
    file.close();
  }
  else printf("Unable to open file \n");

  // Unified memory allocated for INPUT
  float *result;
  cudaError_t status = cudaMallocManaged((void**)&result, 4 * 4 * sizeof(float), cudaMemAttachHost);
  if (status != cudaSuccess) {
    printf("Error allocating unified memory for transformation matrix!\n");
    printf("Transf. matrix not loaded! :-/");

    return 0;
  }

  mult_matrix(result, m2, m1, 4);

  printf("File 3 loaded \n");

  return result;
}


long read_cloud_UM_multibatch(long *cloud_size) // NULL pointer when not accessing 1st batch
{
  static p_ply ply;

  if (cloud_size != NULL) {
    ply = ply_open((dataset_path + pointcloud_file).c_str(), NULL, 0, NULL);

    if (ply_read_header(ply)) {
      ply_get_element_info(ply_get_next_element(ply, 0), 0, cloud_size);

      // Unified memory allocated for INPUT
      cudaError_t status = cudaMallocManaged((void**)&cloud, *cloud_size * sizeof(Point), cudaMemAttachHost);
      if (status != cudaSuccess) {
        printf("Error allocating unified memory for point cloud!\n");
        printf("Cloud not loaded! :-/");

        return 0;
      }

      ply_set_read_cb(ply, "vertex", "x", vertex_x, NULL, 0);
      ply_set_read_cb(ply, "vertex", "y", vertex_y, NULL, 0);
#ifdef _NOTONLYVERT_
      ply_set_read_cb(ply, "vertex", "z", vertex_z, NULL, 0);
      ply_set_read_cb(ply, "vertex", "red", color_r, NULL, 0);
      ply_set_read_cb(ply, "vertex", "green", color_g, NULL, 0);
      ply_set_read_cb(ply, "vertex", "blue", color_b, NULL, 1);
#else
      ply_set_read_cb(ply, "vertex", "z", vertex_z, NULL, 1);
#endif
    }

    long batch_size = ply_read_batch(ply);

    printf("Batch loaded: %ld points/%ld total\n", batch_size, *cloud_size);

    return batch_size;
  } else {
    printf("Cloud not loaded! :-/");

    return 0;
  }

}
