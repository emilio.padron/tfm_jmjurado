#include <cstdio>
#include <iostream>
#include <vector>
#include "cpu.h"
#include "gpu_common.h"
#include "gpu_v32.h"

__global__ void GPUmapping_kernel(const unsigned long npoints, const unsigned int cam_id, Point c_cloud[], Image c_images[], float c_rotation_matrix[], frag_glm zbuffer[]);
__global__ void GPUmapping_kernel_simple(const unsigned long npoints, const unsigned int cam_id, Point c_cloud[], Image c_images[], float c_rotation_matrix[], frag_glm zbuffer[]);
__global__ void GPUResetSnapshot(frag_glm zbuffer[]);
__global__ void GPUResetSnapshot_simple(frag_glm zbuffer[]);

__device__ int d_cullvar;

unsigned int GPU_mapping_and_occlusion(Point* cloud, const unsigned long cloud_points, Image* images, const unsigned int num_images, float* v_rot_matrix, frag snapshots[], const unsigned int blk_size, gputime *gpu_times, size_t nStreams, const size_t vram)
{
  // Out-of-core algorithm: dataset partition, overlapping computation and transfers
  //
  // lock_mem: size of constant data needed in vram during the whole computation
  //
  // dyn_mem: size of structures that can be split for the out-of-core computation
  //          basically: whole dataset
  //          i.e. data bytes/point x number of points in dataset
  //
  // avail_mem: vram available after considering the lock_mem
  //
  // => avail_mem will be split into nStreams parts: 1 residentpart/CUDAstream
  //    Each part basically has:
  //    + data_set: part of dataset to be tx and processed (npb points/part)
  //    + snapshot: z-buffer with frag info per pixel: dist + point_id
  //                (fragimages_size = images_resolution * sizeof(frag))
  //
  // Summary:
  //
  //   VRAM = lock_mem + avail_mem =
  //          lock_mem + nStreams x (data_set + snapshot) =
  //          lock_mem + nStreams x (npb x databytes/point + fragimages_size)
  //
  // To partition data avail_mem is divided this way:
  //
  //  avail_mem = nStreams x (npb x databytes/point + fragimages_size)
  //
  // so, *npb* is the number of points per partition resident in VRAM,
  // being each resident partition tx/processed by a different CUDA Stream
  // => The idea, again, is to overlap computation and communication
  //
  //   npb = (avail_mem / nStreams - fragimages_size) / databytes/point
  //         [truncate to lower integer this quotient]
  //
  // Hence, dyn_mem is partitioned in parts (aka blocks) with npb points
  // => npb points * databytes/point = npb_size
  //
  // Dataset partition: #parts = cloud_points / npb
  //                    [round to higher integer whis quotient]

#ifdef _SYNCTIME_
  auto start = std::chrono::steady_clock::now();
#endif

  size_t dyn_mem = cloud_points * sizeof(Point) // Data: complete dataset
    + fragimages_size; // Z-buffer: (dist (float) + point_id (int)) x res

  size_t lock_mem = 1024 * 1024; // Estimation: 1MB of storage permanently needed in VRAM

  // CPU->GPU transfer of the complete info about the cameras
  Image *d_images;
  size_t numBytes = num_images * sizeof(Image);
  cuda( cudaMalloc((void**)&d_images, numBytes) );
  cuda( cudaMemcpy(d_images, images, numBytes, cudaMemcpyHostToDevice) );
  lock_mem += numBytes;

  // CPU->GPU transfer of the rotation matrix
  float* d_rotation_matrix;
  numBytes = 3 * 3 * sizeof(float);
  cuda( cudaMalloc((void**)&d_rotation_matrix, numBytes) );
  cuda( cudaMemcpy(d_rotation_matrix, v_rot_matrix, numBytes, cudaMemcpyHostToDevice) );
  lock_mem += numBytes;

  size_t avail_mem = vram - lock_mem;

  std::cout << "Lock mem: " << lock_mem / 1024.0f << " KB. Avail mem: "
            << avail_mem / 1024 / 1024 << " MB. Required mem: "
            << dyn_mem / 1024.0f / 1024.0f << " MB." << std::endl;

  // real data per point (databytes/point): sizeof(Point)
  // npb: max points per dataset part
  size_t npb;

  if (dyn_mem > avail_mem) { // not enough memory! => out-of-core
    if (nStreams < 2)
      nStreams = 2;
    npb = (avail_mem/nStreams - fragimages_size) / sizeof(Point);
    // (integer division truncates fractional results toward 0)
  } else {
    npb = (cloud_points / nStreams) + (cloud_points % nStreams);
  }

  size_t partitions = cloud_points / npb + (cloud_points % npb != 0);
  size_t npb_last = cloud_points % npb;
  if (npb_last == 0)
    npb_last = npb;
  // (round up to have a last partition with the rest if not integer quotient)

  size_t npb_size = npb * sizeof(Point);

  if (partitions < nStreams)
    nStreams = partitions;

  std::cout << "Number of CUDA Streams: " << nStreams << std::endl
            << "Number of point cloud partitions: " << partitions << std::endl
            << npb << " points/partition (" << npb_size / 1024 / 1024
            << " MB/partition)" << std::endl
            << "VRAM usage: " << nStreams << " x ("
            << npb_size / 1024 / 1024 << " MB) = "
            << (nStreams * npb_size) / 1024 / 1024 << " MB" << std::endl;

  // Resident dataset partitions (blocks) in VRAM (1 per stream)
  // Transf - Comput overlapping: while 1 block is computed other one is tx
  Point *d_block[nStreams];

  // Final fragments ready to transfer to CPU (final snapshot for current block-camera)
  frag_glm *d_snapshot[nStreams];

  // snapshot for current camera
  frag *snapshot[nStreams],
    //Estructura temporal para chequear los fragmentos válidos en cada cámara
    *temp_snapshot; // fragimages_size x number of Streams

  // Array with streams artifacts
  cudaStream_t streams[nStreams];
  cudaStream_t auxstreams[nStreams];

  for (size_t stream = 0; stream < nStreams; ++stream) {
    cuda( cudaMalloc((void**) &d_block[stream], npb_size) );

    cuda( cudaMalloc((void**) &d_snapshot[stream], fragimages_size) );

    cuda( cudaStreamCreate(&streams[stream]) );
    cuda( cudaStreamCreate(&auxstreams[stream]) );
  }

  //temp_snapshot = (frag *) malloc(nStreams * fragimages_size);
  cuda( cudaMallocHost((void**)&temp_snapshot, nStreams * fragimages_size) );

  //Se inicializan snapshots con un valor alto de distancia
  memset(snapshots, UINT_MAX, num_images * fragimages_size);

  // CUDA stuff: threads/cuda_blk and grid of cuda_blks
  dim3 dimBlock(blk_size);

  dim3 dimGrid; // 1 thread/point
  dim3 dimGrid2((images_resolution + dimBlock.x - 1) / dimBlock.x);; // 1 thread/pixel

#ifdef _SYNCTIME_
  cuda( cudaDeviceSynchronize() );
  auto end = std::chrono::steady_clock::now();
  gpu_times->preliminary = end - start;
#endif

  cudaEvent_t event_snapshotReady, event_snapshotRcvd;
  //  cudaEventCreate (&event_snapshotReady);
  cudaEventCreateWithFlags(&event_snapshotReady, cudaEventDisableTiming);
  cudaEventCreateWithFlags(&event_snapshotRcvd, cudaEventDisableTiming);

  typeof(d_cullvar) cullvar;

  if (partitions == 1) {
    //streams[0] for CPU->GPU and GPU kernels
    //auxtreams[0] for GPU->CPU

    cuda( cudaMemcpyAsync(d_block[0], cloud, npb_size, cudaMemcpyHostToDevice, streams[0]) );

    for (unsigned int c = 0; c < num_images; c++) {
      dimGrid = (npb + dimBlock.x - 1) / dimBlock.x; // 1 thread/point per CUDA BLK

      GPUResetSnapshot<< <dimGrid2, dimBlock, 0, streams[0] >> > (d_snapshot[0]);
      GPUmapping_kernel<< <dimGrid, dimBlock, 0, streams[0] >> > (npb, c, d_block[0], d_images, d_rotation_matrix, d_snapshot[0]);

      //cuda( cudaPeekAtLastError() );     // only for debugging
      //cuda( cudaDeviceSynchronize() );

      cudaEventRecord(event_snapshotReady, streams[0]);

      // Merge c-1 image
      if (c > 0) {
        cudaEventSynchronize(event_snapshotRcvd);

        // for (size_t j = 0; j < images_resolution; j++) {
        //   if(temp_snapshot[j].dist < snapshots[j].dist){
        //     snapshots[j] = temp_snapshot[j];
        //   }
        // }

        snapshot[0] += images_resolution; // move to snapshot for next camera
      }

      cudaStreamWaitEvent(auxstreams[0], event_snapshotReady, 0);
      cuda( cudaMemcpyAsync(temp_snapshot, d_snapshot[0], fragimages_size, cudaMemcpyDeviceToHost, auxstreams[0]) ); // GPU -> CPU

      cudaEventRecord(event_snapshotRcvd, auxstreams[0]);
    }

  } else { // partitions > 1

    size_t desp = 0; //iterador para el desplazamiento en la nube
    size_t part = 0; //block currently being managed
    size_t partpoints[nStreams]; // points in block currently being managed
    unsigned int pending = 0;

    // One first transfer per stream (when nStreams < partitions)
    for (size_t stream = 0; stream < nStreams; ++stream) {

      partpoints[stream] = (++part < partitions)? npb : npb_last;

      cuda( cudaMemcpyAsync(d_block[stream], &cloud[desp], partpoints[stream] * sizeof(Point), cudaMemcpyHostToDevice, streams[stream]) );
      //std::cout << "CPU->GPU<" << stream << "> Blk #" << part-1 << " (" << partpoints[stream] << " points)" << std::endl;

      snapshot[stream] = snapshots; // Reset: first snapshot for first camera!
      ++pending;

      desp += partpoints[stream];
    }

    do {

      for (size_t stream = 0; stream < nStreams; ++stream) {

        if (partpoints[stream] == 0)
          break;

        dimGrid = (partpoints[stream] + dimBlock.x - 1) / dimBlock.x; // 1 thread/point per CUDA BLK

        for (unsigned int c = 0; c < num_images; c++) {

          GPUResetSnapshot<< <dimGrid2, dimBlock, 0, streams[stream] >> > (d_snapshot[stream]);
          GPUmapping_kernel<< <dimGrid, dimBlock, 0, streams[stream] >> > (partpoints[stream], c, d_block[stream], d_images, d_rotation_matrix, d_snapshot[stream]);
          //std::cout << "Kernels<" << stream << "> (" << partpoints[stream] << " points, cam#" << c << ")" << std::endl;

	  //cuda( cudaPeekAtLastError() );     // only for debugging
	  //cuda( cudaDeviceSynchronize() );

          cudaEventRecord(event_snapshotReady, streams[stream]);

          // Merge c-1 image
          if (c > 0) {
            cudaEventSynchronize(event_snapshotRcvd);

            if (cullvar != 0) {
              frag *partial_snapshot, *final_snapshot;

              partial_snapshot = temp_snapshot + images_resolution * stream;
              final_snapshot = snapshot[stream];

              // for (size_t j = 0; j < images_resolution; j++) {
              //   if(partial_snapshot[j].dist64 < final_snapshot[j].dist64){
              //     final_snapshot[j] = partial_snapshot[j];
              //   }
              // }
            }

            snapshot[stream] += images_resolution; // move to snapshot for next camera
          }

          // Transfer snapshot to CPU
          cudaStreamWaitEvent(auxstreams[stream], event_snapshotReady, 0);

          cuda( cudaMemcpyFromSymbolAsync(&cullvar, d_cullvar, sizeof(cullvar), 0, cudaMemcpyDeviceToHost, auxstreams[stream]) );

          if (cullvar != 0) {
            cuda( cudaMemcpyAsync(temp_snapshot + images_resolution * stream, d_snapshot[stream], fragimages_size, cudaMemcpyDeviceToHost, auxstreams[stream]) ); // GPU -> CPU
            //std::cout << "CPU<-GPU<" << stream << ">" << " (" << partpoints[stream] << " points, cam#" << c << ")" << std::endl;
          }

          cudaEventRecord(event_snapshotRcvd, auxstreams[stream]);

        }
        --pending;

        // Transfer next block for this stream (if needed!)
        if (part == partitions) { // No more CPU->GPU transfers
          partpoints[stream] = 0;
          continue;
        }

        partpoints[stream] = (++part < partitions)? npb : npb_last;

        cuda( cudaMemcpyAsync(d_block[stream], &cloud[desp], partpoints[stream] * sizeof(Point), cudaMemcpyHostToDevice, streams[stream]) );
        //std::cout << "CPU->GPU<" << stream << "> Blk #" << part-1 << " (" << partpoints[stream] << " points)" << std::endl;

        // Merge last image from previous block
        cudaEventSynchronize(event_snapshotRcvd);

        if (cullvar != 0) {
          frag *partial_snapshot, *final_snapshot;

          partial_snapshot = temp_snapshot + images_resolution * stream;
          final_snapshot = snapshot[stream];

          // for (size_t j = 0; j < images_resolution; j++) {
          //   if(partial_snapshot[j].dist < final_snapshot[j].dist){
          //     final_snapshot[j] = partial_snapshot[j];
          //   }
          // }
        }

        // Setup stuff for next block!
        snapshot[stream] = snapshots; // Reset: first snapshot for first camera!
        ++pending;

        desp += partpoints[stream];

      }

    } while (pending);

  }

  cuda( cudaFree(d_rotation_matrix) );
  cuda( cudaFree(d_images) );
  for (size_t stream = 0; stream < nStreams; ++stream) {
    cuda( cudaFree(d_block[stream]) );
    cudaStreamDestroy(streams[stream]);
  }
  cudaFreeHost(temp_snapshot);

  // Ensure all CUDA stuff has really finished!
  cuda( cudaDeviceSynchronize() );

  return partitions;
}


__device__ void GPU_matrix_trans(float* trans, float* matrix)
{
  for (int c = 0; c < 3; c++) {
    for (int d = 0; d < 3; d++) {
      trans[c * 3 + d] = matrix[d * 3 + c];
    }
  }
}


__device__ void GPU_mult_matrix(float* result, float* m1, float* m2, int n)
{
  float res;
  for (int r = 0; r < n; ++r) {
    for (int c = 0; c < n; ++c) {
      res = 0;
      for (int k = 0; k < n; ++k) {
        res += m1[r * n + k] * m2[k * n + c];
      }
      result[r * n + c] = res;
    }
  }
}


__global__ void GPUmapping_kernel(const unsigned long npoints, const unsigned int cam_id, Point c_cloud[], Image c_images[], float c_rotation_matrix[], frag_glm zbuffer[])
{
  unsigned int task_id = blockIdx.x * blockDim.x + threadIdx.x;

  if (task_id < npoints) {

    /*****Transformación geométrica******/

    float trans_m1[9];//matriz transpuesta de la matriz de rotación de la cámara
    float trans_m2[9];//matriz transpuesta de la matriz de rotación de la transformación ICP
    float rot_m[9];

    //Transposición de matrices
    GPU_matrix_trans(trans_m1, c_images[cam_id].rotation_matrix);
    GPU_matrix_trans(trans_m2, c_rotation_matrix);

    //Multiplicación de matrices
    GPU_mult_matrix(rot_m, trans_m1, trans_m2, 3);

    //Traslación y Rotación
    float res[3];
    float temp;
    for (int r = 0; r < 3; ++r) {
      temp = 0;
      for (int c = 0; c < 3; ++c) {
        temp += rot_m[r * 3 + c] * (c_cloud[task_id].position[c] - c_images[cam_id].position[c]);
      }
      res[r] = temp;
    }

    /*****Proyección*******/
    //Fisheye Polynomial
    float D[4] = { c_images[cam_id].D[0], c_images[cam_id].D[1], c_images[cam_id].D[2], c_images[cam_id].D[3] };
    //Fisheye Affine Matrix
    float A[4] = { c_images[cam_id].A[0], c_images[cam_id].A[1], c_images[cam_id].A[2], c_images[cam_id].A[3] };

    float cx = c_images[cam_id].cx;
    float cy = c_images[cam_id].cy;

    //Punto 3D
    float x = res[0];
    float y = res[1];
    float z = res[2];

    float r = sqrtf(powf(x, 2) + powf(y, 2));
    float theta = (2 / M_PI) * atanf(r / z);
    float poly = D[0] + D[1] * theta + D[2] * theta * theta + D[3] * theta * theta * theta;
    float xh = (poly * x) / r;
    float yh = (poly * y) / r;

    //Coordenadas de la imagen

    //int px = lrintf(A[2] * xh + A[3] * yh + cy);
    //int py = lrintf(A[0] * xh + A[1] * yh + cx);
    int px = __float2int_rn(A[2] * xh + A[3] * yh + cy);
    int py = __float2int_rn(A[0] * xh + A[1] * yh + cx);

    if (px < 0 || px > 959 || py < 0 || py > 1279)
      return; // Discarded!
    else
      d_cullvar=1;

    //Pixel en la imagen proyectado por el Punto 3D
    unsigned int fragment_id = px * 1280 + py;

    unsigned long dist = __float_as_uint(sqrtf(powf((x - c_images[cam_id].position[0]), 2) + powf((y - c_images[cam_id].position[1]), 2) + powf((z - c_images[cam_id].position[2]), 2)));

    frag_glm fragment(task_id | (dist << 32));

    atomicMin(&zbuffer[fragment_id], fragment);
  }

}


__global__ void GPUmapping_kernel_simple(const unsigned long npoints, const unsigned int cam_id, Point c_cloud[], Image c_images[], float c_rotation_matrix[], frag_glm zbuffer[])
{
  unsigned int task_id = blockIdx.x * blockDim.x + threadIdx.x;

  if (task_id < npoints) {

    /*****Transformación geométrica******/

    float trans_m1[9];//matriz transpuesta de la matriz de rotación de la cámara
    float trans_m2[9];//matriz transpuesta de la matriz de rotación de la transformación ICP
    float rot_m[9];

    //Transposición de matrices
    GPU_matrix_trans(trans_m1, c_images[cam_id].rotation_matrix);
    GPU_matrix_trans(trans_m2, c_rotation_matrix);

    //Multiplicación de matrices
    GPU_mult_matrix(rot_m, trans_m1, trans_m2, 3);

    //Traslación y Rotación
    float res[3];
    float temp;
    for (int r = 0; r < 3; ++r) {
      temp = 0;
      for (int c = 0; c < 3; ++c) {
        temp += rot_m[r * 3 + c] * (c_cloud[task_id].position[c] - c_images[cam_id].position[c]);
      }
      res[r] = temp;
    }

    /*****Proyección*******/
    //Fisheye Polynomial
    float D[4] = { c_images[cam_id].D[0], c_images[cam_id].D[1], c_images[cam_id].D[2], c_images[cam_id].D[3] };
    //Fisheye Affine Matrix
    float A[4] = { c_images[cam_id].A[0], c_images[cam_id].A[1], c_images[cam_id].A[2], c_images[cam_id].A[3] };

    float cx = c_images[cam_id].cx;
    float cy = c_images[cam_id].cy;

    //Punto 3D
    float x = res[0];
    float y = res[1];
    float z = res[2];

    float r = sqrtf(powf(x, 2) + powf(y, 2));
    float theta = (2 / M_PI) * atanf(r / z);
    float poly = D[0] + D[1] * theta + D[2] * theta * theta + D[3] * theta * theta * theta;
    float xh = (poly * x) / r;
    float yh = (poly * y) / r;

    //Coordenadas de la imagen

    //int px = lrintf(A[2] * xh + A[3] * yh + cy);
    //int py = lrintf(A[0] * xh + A[1] * yh + cx);
    int px = __float2int_rn(A[2] * xh + A[3] * yh + cy);
    int py = __float2int_rn(A[0] * xh + A[1] * yh + cx);

    if (px < 0 || px > 959 || py < 0 || py > 1279)
      return; // Discarded!

    //Pixel en la imagen proyectado por el Punto 3D
    unsigned int fragment_id = px * 1280 + py;

    unsigned long dist = __float_as_uint(sqrtf(powf((x - c_images[cam_id].position[0]), 2) + powf((y - c_images[cam_id].position[1]), 2) + powf((z - c_images[cam_id].position[2]), 2)));

    frag_glm fragment(task_id | (dist << 32));

    atomicMin(&zbuffer[fragment_id], fragment);
  }

}


__global__ void GPUResetSnapshot(frag_glm zbuffer[])
{
  unsigned int task_id = blockIdx.x * blockDim.x + threadIdx.x;

  if (task_id < 1228800) {
    zbuffer[task_id] = ULONG_MAX;
  }

  if (task_id == 0)
    d_cullvar = 0;
}


__global__ void GPUResetSnapshot_simple(frag_glm zbuffer[])
{
  unsigned int task_id = blockIdx.x * blockDim.x + threadIdx.x;

  if (task_id < 1228800) {
    zbuffer[task_id] = ULONG_MAX;
  }
}


__global__ void GPUgenerate_snapshot(const int * const nelements, unsigned int src_pixelid[], frag src_value[], frag dst_value[])
{
  const int task_id = blockIdx.x * blockDim.x + threadIdx.x;

  if (task_id < *nelements - 1) {
    unsigned int pixel_id = src_pixelid[task_id];

    //    For some debugging:
    //    printf("Task_id: %d - pixel_id %d - point %d - dist %f\n", task_id, pixel_id, src_value[task_id].pointid, src_value[task_id].dist);

    dst_value[pixel_id].dist = src_value[task_id].dist;
    dst_value[pixel_id].pointid = src_value[task_id].pointid;
  }
}
