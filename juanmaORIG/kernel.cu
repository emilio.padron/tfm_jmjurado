
#include "cuda_runtime.h"
#include "device_launch_parameters.h"

#include <stdio.h>
#include <time.h> 
#include <math.h>
#include <iostream>
#include <assert.h>
#include<string>
#include "rply.h"
#include <fstream>
#include <sstream>
#include <vector>
using namespace std;

#define M_PI 3.1415926535897932385E0

//const int N = 4651218;  // Tama�o de la nube
//const int N = 744671;
//const int N = 3395368;
const int N = 66374475;

const int CUDA_BLK = 64;  // Tama�o predeterm. de bloque de hilos CUDA

struct spectral_point {
	float *nir;
	float *red;
	float *green;
	float *reg;
};

struct c_p {
	int id_point;
	int id_camara;
};


struct Camera_points {
	int id_camera;
	int *points;
	unsigned int num_points;
};

struct Point_cameras {
	int id_point;
	int *cameras;
	unsigned int num_cameras;
};

struct Point {
	float position[3];
	int r;
	int g;
	int b;
};

struct Image {
	float position[3];
	float rotation_matrix[9];
	int band;
	float A[4];
	float D[4];
	float cx;
	float cy;
};

struct Reflectance_matrix {
	int band;
	float* m;
};

struct Band {
	int band;
	float A[4];
	float D[4];
	float cx;
	float cy;
};

struct Projection {
	float px; //pixelx
	float py; //pixely
	int c; //c�mara sobre la que se proyecta
	int id_point;
	float dist; //distancia del punto a la c�mara
	bool occluded;
};

//Variables globales

Point *cloud;
int i = 0;
int num_images;
int num_projections;

// Declaraci�n de funci�n para ver recursos del device
void devicenfo(void);

//Leer la nube (PLY)
static int vertex_x(p_ply_argument argument) {
	long eol;
	ply_get_argument_user_data(argument, NULL, &eol);
	//printf("%g ", ply_get_argument_value(argument));
	cloud[i].position[0] = ply_get_argument_value(argument);
	return 1;
}
static int vertex_y(p_ply_argument argument) {
	long eol;
	ply_get_argument_user_data(argument, NULL, &eol);
	//printf("%g ", ply_get_argument_value(argument));
	cloud[i].position[1] = ply_get_argument_value(argument);
	return 1;
}
static int vertex_z(p_ply_argument argument) {
	long eol;
	ply_get_argument_user_data(argument, NULL, &eol);
	//printf("%g ", ply_get_argument_value(argument));
	cloud[i].position[2] = ply_get_argument_value(argument);
	return 1;
}
static int color_r(p_ply_argument argument) {
	long eol;
	ply_get_argument_user_data(argument, NULL, &eol);
	//printf("%g ", ply_get_argument_value(argument));
	cloud[i].r = ply_get_argument_value(argument);
	return 1;
}
static int color_g(p_ply_argument argument) {
	long eol;
	ply_get_argument_user_data(argument, NULL, &eol);
	//printf("%g ", ply_get_argument_value(argument));
	cloud[i].g = ply_get_argument_value(argument);
	return 1;
}
static int color_b(p_ply_argument argument) {
	long eol;
	ply_get_argument_user_data(argument, NULL, &eol);
	//printf("%g \n", ply_get_argument_value(argument));
	cloud[i].b = ply_get_argument_value(argument);
	i++;
	return 1;
}


void mult_matrix(float* result, float* m1, float* m2, int n) {
	float res;
	for (int r = 0; r < n; ++r) {
		for (int c = 0; c < n; ++c) {
			res = 0;
			for (int k = 0; k < n; ++k) {
				res += m1[r * n + k] * m2[k * n + c];
			}
			result[r * n + c] = res;
		}
	}
}

void matrix_trans(float *trans, float *matrix) {

	for (int c = 0; c < 3; c++) {
		for (int d = 0; d < 3; d++) {
			trans[c * 3 + d] = matrix[d * 3 + c];
		}
	}

}

void read_cloud(const int cloud_size) {

	p_ply ply = ply_open("./files/nube.ply", NULL, 0, NULL);
	cloud = (Point*)malloc(cloud_size * sizeof(Point));

	if (ply_read_header(ply)) {

		ply_set_read_cb(ply, "vertex", "x", vertex_x, NULL, 0);
		ply_set_read_cb(ply, "vertex", "y", vertex_y, NULL, 0);
		ply_set_read_cb(ply, "vertex", "z", vertex_z, NULL, 0);
		ply_set_read_cb(ply, "vertex", "red", color_r, NULL, 0);
		ply_set_read_cb(ply, "vertex", "green", color_g, NULL, 0);
		ply_set_read_cb(ply, "vertex", "blue", color_b, NULL, 1);

		if (ply_read(ply)) {
			ply_close(ply);
		}
	}
	printf("Cloud loaded \n");

}

//Leer par�metros de cada imagen (posici�n y matriz de rotaci�n)
Image* read_images() {

	ifstream file("./files/images.txt");
	string line;

	getline(file, line);
	stringstream iss1(line);
	iss1 >> num_images;
	Image* images;
	images = (Image*)malloc(num_images * sizeof(Image));

	int it = 0;
	while (getline(file, line))
	{
		stringstream iss(line);

		Image im;
		iss >> im.position[0] >> im.position[1] >> im.position[2];
		for (int r = 0; r < 3; ++r) {
			getline(file, line);
			stringstream iss2(line);
			for (int c = 0; c < 3; ++c) {
				iss2 >> im.rotation_matrix[r * 3 + c];
			}
		}
		getline(file, line);
		stringstream iss2(line);
		iss2 >> im.band;

		getline(file, line);
		stringstream iss3(line);
		iss3 >> im.D[0] >> im.D[1] >> im.D[2] >> im.D[3];

		getline(file, line);
		stringstream iss4(line);
		iss4 >> im.A[0] >> im.A[1] >> im.A[2] >> im.A[3];

		getline(file, line);
		stringstream iss5(line);
		iss5 >> im.cx >> im.cy;

		images[it] = im;
		++it;
	}
	printf("File 1 loaded \n");
	return images;
}


//Leer camaras asociadas a cada punto 3D
//vector<Point_cameras> read_cameras_point(const int cloud_size) {
//
//	vector<Point_cameras> projs;
//	ifstream file("./files/point_cameras.txt");
//	string line;
//	int it = 0;
//	int cams;
//	while (getline(file, line))
//	{
//		if (line != "") {
//			Point_cameras c_p;
//			stringstream iss(line);
//			iss >> cams;
//			c_p.num_cameras = cams;
//			c_p.cameras = (int*)malloc(cams * sizeof(int));
//			c_p.id_point = it;
//			int token;
//			int k = 0;
//			while (iss >> token) {
//				c_p.cameras[k] = token;
//				++k;
//			}
//			projs.push_back(c_p);
//		}
//		++it;
//	}
//	printf("File 1 loaded \n");
//	return projs;
//}

vector<Point_cameras> read_cameras_point(Camera_points* camera_points,const int cloud_size) {

	vector<Point_cameras> point_cameras;

	FILE *f;
	f = fopen("./files/point_cameras.bin", "rb");
	int n; //n�mero de proyecciones globales
	fread(&n, sizeof(int), 1, f);
	num_projections = 0;
	for (int i = 0; i < n; i++) {
		Point_cameras c_p;

		int id_point;
		fread(&id_point, sizeof(int), 1, f);
		//cout << "idPoint: " << id_point << "\n";
		c_p.id_point = id_point;

		int size;
		fread(&size, sizeof(int), 1, f);
		//cout <<"size: "<< size << "\n";
		c_p.num_cameras = size;
		num_projections += size;
		int *cameras;
		cameras = (int*)malloc(size * sizeof(int));
		fread(cameras, sizeof(int), size, f);
		c_p.cameras = cameras;
		point_cameras.push_back(c_p);
		//for (int j = 0; j < size; j++) {
		//	cout << cameras[j] << " ";
		//}
		//cout << endl;
	}

	//Creamos el vector de camera_points;

	for (int i = 0; i < num_images; i++) {
		vector<int> v;
		for (int j = 0; j < point_cameras.size(); j++) {
			for (int k = 0; k < point_cameras.at(j).num_cameras; k++) {
				if (point_cameras.at(j).cameras[k] == i)
					v.push_back(point_cameras.at(j).id_point);
			}
		}
		Camera_points c_p;
		c_p.num_points = v.size();
		c_p.id_camera = i;
		int *points;
		points = (int*)malloc(v.size() * sizeof(int));
		for (int j = 0; j < v.size(); j++) {
			points[j] = v.at(j);
		}
		c_p.points = points;
		camera_points[i] = c_p;
	}

	printf("File 2 loaded \n");

	return point_cameras;
}




//Leer matriz de transformaci�n (ICP)
void read_transformation_matrix(float *result) {

	float *m1;
	float *m2;
	m1 = (float*)malloc(4 * 4 * sizeof(float));
	m2 = (float*)malloc(4 * 4 * sizeof(float));

	ifstream file("./files/transformation_matrix.txt");
	string line;
	if (file.is_open()) {
		//M1
		int it = 0;
		getline(file, line);
		while (line[0] != '*')
		{
			stringstream linestream(line);
			for (int c = 0; c < 4; c++) {
				linestream >> m1[it * 4 + c];
			}
			getline(file, line);
			++it;
		}

		//M2
		it = 0;
		while (getline(file, line))
		{
			stringstream linestream(line);
			for (int c = 0; c < 4; c++) {
				linestream >> m2[it * 4 + c];
			}
			++it;
		}
		file.close();
	}
	else printf("Unable to open file \n");

	mult_matrix(result, m2, m1, 4);

	printf("File 3 loaded \n");

}

//Leer matrices de reflectancia

Reflectance_matrix* read_reflectance_matrix() {

	Reflectance_matrix* reflec_v;

	FILE *f;
	f = fopen("./files/reflectance.bin", "rb");
	int num_img;
	fread(&num_img, sizeof(int), 1, f);
	reflec_v = (Reflectance_matrix*)malloc(num_img * sizeof(Reflectance_matrix));
	for (int i = 0; i < num_img; i++) {

		Reflectance_matrix reflec;
		int band;
		fread(&band, sizeof(int), 1, f);
		reflec.band = band;

		float* matrix;
		matrix = (float*)malloc(960 * 1280 * sizeof(float));
		fread(matrix, sizeof(float), 960 * 1280, f);
		reflec.m = matrix;
		reflec_v[i] = reflec;
	}
	printf("File 4 loaded \n");
	return reflec_v;
	/*


	ifstream file("./files/reflectance.txt");
	string line;
	getline(file, line);

	stringstream iss1(line);
	iss1 >> num_images;
	reflec_v = (Reflectance_matrix*)malloc(num_images * sizeof(Reflectance_matrix));

	int it = 0;
	while (getline(file, line))
	{
		Reflectance_matrix reflec;
		reflec.m = (float**)malloc(960 * sizeof(float*));
		stringstream iss2(line);
		iss2 >> reflec.band;
		for (int r = 0; r < 960; r++) {
			getline(file, line);
			stringstream iss3(line);
			float token;
			int c = 0;
			reflec.m[r] = (float*)malloc(1280 * sizeof(float));
			while (iss3 >> token) {
				reflec.m[r][c] = token;
				++c;
			}
		}
		getline(file, line);
		reflec_v[it] = reflec;
		++it;
	}
	*/
}

//Leer metadatos en cada banda
void read_metadata(Band* metadata) {

	ifstream file("./files/metadata.txt");
	string line;
	if (file.is_open()) {
		for (int it = 0; it < 4; it++) {
			getline(file, line);
			stringstream iss1(line);
			iss1 >> metadata[it].band;

			getline(file, line);
			stringstream iss2(line);
			iss2 >> metadata[it].A[0] >> metadata[it].A[1] >> metadata[it].A[2] >> metadata[it].A[3];

			getline(file, line);
			stringstream iss3(line);
			iss3 >> metadata[it].D[0] >> metadata[it].D[1] >> metadata[it].D[2] >> metadata[it].D[3];

			getline(file, line);
			stringstream iss4(line);
			iss4 >> metadata[it].cx >> metadata[it].cy;
		}
	}
	printf("File 5 loaded \n");
}

/*
  Funci�n que ajusta el n�mero de hilos, de bloques, y de bloques por hilo
  de acuerdo a las restricciones de la GPU
*/
void checkparams(unsigned int* n, unsigned int* cb)
{
	struct cudaDeviceProp capabilities;

	// Si menos numero total de hilos que tama�o bloque, reducimos bloque
	if (*cb > * n)
		*cb = *n;

	cudaGetDeviceProperties(&capabilities, 0);

	if (*cb > capabilities.maxThreadsDim[0]) {
		*cb = capabilities.maxThreadsDim[0];
		printf("->N�m. hilos/bloq cambiado a %d (m�x por bloque para dev)\n\n",
			*cb);
	}

	if (((*n + *cb - 1) / *cb) > capabilities.maxGridSize[0]) {
		*cb = 2 * (*n - 1) / (capabilities.maxGridSize[0] - 1);
		if (*cb > capabilities.maxThreadsDim[0]) {
			*cb = capabilities.maxThreadsDim[0];
			printf("->N�m. hilos/bloq cambiado a %d (m�x por bloque para dev)\n",
				*cb);
			if (*n > (capabilities.maxGridSize[0] * *cb)) {
				*n = capabilities.maxGridSize[0] * *cb;
				printf("->N�m. total de hilos cambiado a %d (m�x por grid para \
dev)\n\n", *n);
			}
			else {
				printf("\n");
			}
		}
		else {
			printf("->N�m. hilos/bloq cambiado a %d (%d m�x. bloq/grid para \
dev)\n\n",
*cb, capabilities.maxGridSize[0]);
		}
	}
}

/*
  Sacar por pantalla informaci�n del *device*
*/
void devicenfo(void)
{
	struct cudaDeviceProp capabilities;

	cudaGetDeviceProperties(&capabilities, 0);

	printf("->CUDA Platform & Capabilities\n");
	printf("Name: %s\n", capabilities.name);
	printf("totalGlobalMem: %.2f MB\n", capabilities.totalGlobalMem / 1024.0f / 1024.0f);
	printf("sharedMemPerBlock: %.2f KB\n", capabilities.sharedMemPerBlock / 1024.0f);
	printf("regsPerBlock (32 bits): %d\n", capabilities.regsPerBlock);
	printf("warpSize: %d\n", capabilities.warpSize);
	printf("memPitch: %.2f KB\n", capabilities.memPitch / 1024.0f);
	printf("maxThreadsPerBlock: %d\n", capabilities.maxThreadsPerBlock);
	printf("maxThreadsDim: %d x %d x %d\n", capabilities.maxThreadsDim[0],
		capabilities.maxThreadsDim[1], capabilities.maxThreadsDim[2]);
	printf("maxGridSize: %d x %d\n", capabilities.maxGridSize[0],
		capabilities.maxGridSize[1]);
	printf("totalConstMem: %.2f KB\n", capabilities.totalConstMem / 1024.0f);
	printf("major.minor: %d.%d\n", capabilities.major, capabilities.minor);
	printf("clockRate: %.2f MHz\n", capabilities.clockRate / 1024.0f);
	printf("textureAlignment: %zd\n", capabilities.textureAlignment);
	printf("deviceOverlap: %d\n", capabilities.deviceOverlap);
	printf("multiProcessorCount: %d\n", capabilities.multiProcessorCount);
}

void mapping(Projection** projections, Point *cloud, vector<Point_cameras> point_cameras, Image *images, float *rotation_matrix) {

	int size = point_cameras.size();
	for (int it = 0; it < size; it++) {

		Projection* proj_point;
		unsigned int num_cameras = point_cameras[it].num_cameras;
		proj_point = (Projection*)malloc(num_cameras * sizeof(Projection));

		//printf("IT: %d \n", num_cameras);

		//C�maras para cada punto
		for (int k = 0; k < num_cameras; k++) {

			//Camara visible desde ese punto
			//Id del punto en la nube
			int cam = point_cameras[it].cameras[k];
			int id_point = point_cameras[it].id_point;
			/*****Transformaci�n geom�trica******/

			float *trans_m1; //matriz transpuesta de la matriz de rotaci�n de la c�mara
			float *trans_m2;//matriz transpuesta de la matriz de rotaci�n de la transformaci�n ICP
			float *rot_m;
			//Inicializaci�n
			trans_m1 = (float*)malloc(3 * 3 * sizeof(float));
			trans_m2 = (float*)malloc(3 * 3 * sizeof(float));
			rot_m = (float*)malloc(3 * 3 * sizeof(float));

			//Transposici�n de matrices
			matrix_trans(trans_m1, images[cam].rotation_matrix);
			matrix_trans(trans_m2, rotation_matrix);

			//Multiplicaci�n de matrices
			mult_matrix(rot_m, trans_m1, trans_m2, 3);

			//Traslaci�n y Rotaci�n
			
			float res[3];
			float temp;
			for (int r = 0; r < 3; ++r) {
				temp = 0;
				for (int c = 0; c < 3; ++c) {

					temp += rot_m[r * 3 + c] * (cloud[id_point].position[c] - images[cam].position[c]);
				}
				res[r] = temp;
			}
			//printf("POINT %d, x: %f \n", id_point, cloud[id_point].position[0]); 
			//printf("POINT %d, x: %f \n", id_point, res[0]);

			/*****Proyecci�n*******/

			Projection p;

			//Fisheye Polynomial
			float D[4] = { images[cam].D[0], images[cam].D[1], images[cam].D[2], images[cam].D[3] };
			//Fisheye Affine Matrix
			float A[4] = { images[cam].A[0], images[cam].A[1], images[cam].A[2], images[cam].A[3] };

			float cx = images[cam].cx;
			float cy = images[cam].cy;

			//Punto 3D
			float x = res[0];
			float y = res[1];
			float z = res[2];

			float r = sqrt(pow(x, 2) + pow(y, 2));
			float theta = (2 / M_PI) * atan(r / z);
			float poly = D[0] + D[1] * theta + D[2] * theta * theta + D[3] * theta * theta * theta;
			float xh = (poly * x) / r;
			float yh = (poly * y) / r;

			//Coordenadas de la imagen

			float px = A[2] * xh + A[3] * yh + cy;
			float py = A[0] * xh + A[1] * yh + cx;

			//P�xel en la imagen proyectado por el Punto 3D
			p.px = round(px);
			p.py = round(py);

			p.c = cam;
			p.id_point = id_point;
			float distance = sqrtf(pow((x - images[cam].position[0]), 2) + pow((y - images[cam].position[1]), 2) + pow((z - images[cam].position[2]), 2));
			p.dist = distance;
			proj_point[k] = p;
			if (p.px < 0 || p.px > 960 || p.py < 0 || p.py > 1280) {
				printf("POINT %d -> c: %d, px: %f, py: %f, dist: %f \n", id_point, cam, p.px, p.py, p.dist);
			}

		}
		projections[it] = proj_point;

	}
	printf("Projection -> x: %f y: %f  \n", projections[1][0].px, projections[1][0].py);

}

void oclussion_test(Projection** projections, vector<Point_cameras> point_cameras) {
	
	for (int it = 0; it < num_images; it++) {

		//Se inicializa la matriz de oclusi�n a 0
		float *m_occlusion;
		int numbytes = 960 * 1280 * sizeof(float);
		m_occlusion = (float*)malloc(numbytes);
		memset(m_occlusion, 0x77, numbytes);

		c_p *indices;
		numbytes = 960 * 1280 * sizeof(c_p);
		indices = (c_p*)malloc(numbytes);
		memset(indices, -1, numbytes);
		int id_point, id_cam;
		for (int i = 0; i < point_cameras.size(); i++) {
			for (int j = 0; j < point_cameras[i].num_cameras; j++) {
				if (projections[i][j].c == it) {
					int px = projections[i][j].px;
					int py = projections[i][j].py;
					float dist = projections[i][j].dist;
					if (dist < m_occlusion[px * 1280 + py]) {
						id_point = indices[px * 1280 + py].id_point;
						//Si hay un punto proyectado sobre ese pixel
						if (id_point != -1) {
							id_cam = indices[px * 1280 + py].id_camara;
							projections[id_point][id_cam].occluded = true;
						}
						indices[px * 1280 + py].id_point = i;
						indices[px * 1280 + py].id_camara = j;
						m_occlusion[px * 1280 + py] = dist;
						projections[i][j].occluded = false;
					}
					else {
						projections[i][j].occluded = true;
					}
				}
			}
		}
	}

	int cont = 0;
	for (int i = 0; i < point_cameras.size(); i++) {
		for (int j = 0; j < point_cameras[i].num_cameras; j++) {
			if (projections[i][j].occluded == true)
				++cont;
	//		printf("ID: %d, CAM: %d, oc: %d , px: %f , py: %f dist: %f \n", projections[i][j].id_point, projections[i][j].c, projections[i][j].occluded, projections[i][j].px, projections[i][j].py, projections[i][j].dist);
		}
	}
	printf("Number of occluded points: %d \n", cont);


}
void reflectance_assignment() {


}

//GPU

//Declaraci�n de los kernels
__global__ void mapping_kernel(float* ctest, Point* c_cloud, int* c_cameras, int* c_points, Image* c_images, float* c_rotation_matrix, Projection* c_projections, const int total_points);


__global__ void oclussion_kernelv1(Projection* c_projections, int* c_cameras, const int num_images, float* test);
__global__ void oclussion_kernelv2(Projection* c_projections, int* c_row_points, const int num_images, float* test);
__global__ void oclussion_kernelv3(Projection* c_projections, int* c_points, const int num_images, float* test);


Projection* mapping_GPU(Point* cloud, const int cloud_size, int num_projections, int* v_cameras, Camera_points* camera_points, vector<Point_cameras> point_cameras, Image* images, float* v_rot_matrix, const unsigned int blk_size, float* gpu_time)
{

	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	int bytes = 0;

	struct cudaDeviceProp capabilities;
	cudaGetDeviceProperties(&capabilities, 0);
	float gpu_memory = capabilities.totalGlobalMem / 1024.0f / 1024.0f;

	printf("totalGlobalMem: %.2f MB\n", gpu_memory);

	//Nube de puntos
	Point *c_cloud;
	unsigned int numBytes = cloud_size * sizeof(Point);
	cudaMalloc((void**)&c_cloud, numBytes);
	cudaMemcpy(c_cloud, cloud, numBytes, cudaMemcpyHostToDevice);
	bytes += numBytes;
	
	printf("CLOUD: %d \n", bytes);
	//C�maras por punto
	int* cameras;
	int* points;

	const int total_points = num_projections;
	cameras = (int*)malloc(total_points * sizeof(int));
	points = (int*)malloc(total_points * sizeof(int));
	int it = 0;
	int num_points;

	

	for (int i = 0; i < num_images; i++) {
		num_points = camera_points[i].num_points;
		v_cameras[i] = camera_points[i].num_points;

		for (int j = 0; j < num_points; j++) {
			cameras[it] = camera_points[i].id_camera;
			points[it] = camera_points[i].points[j];
			++it;
		}
	}

	int* c_cameras;
	int* c_points;
	numBytes = total_points * sizeof(int);
	cudaMalloc((void**)&c_cameras, numBytes);
	cudaMalloc((void**)&c_points, numBytes);

	cudaMemcpy(c_points, points, numBytes, cudaMemcpyHostToDevice);
	cudaMemcpy(c_cameras, cameras, numBytes, cudaMemcpyHostToDevice);
	bytes += 2 * numBytes;

	//Datos de una im�gen
	Image *c_images;
	numBytes = num_images * sizeof(Image);
	cudaMalloc((void**)&c_images, numBytes);
	cudaMemcpy(c_images, images, numBytes, cudaMemcpyHostToDevice);
	bytes += numBytes;

	//Matriz de rotaci�n
	float* c_rotation_matrix;
	numBytes = 3 * 3 * sizeof(float);
	cudaMalloc((void**)&c_rotation_matrix, numBytes);
	cudaMemcpy(c_rotation_matrix, v_rot_matrix, numBytes, cudaMemcpyHostToDevice);
	bytes += numBytes;

	//Proyecciones
	Projection* projections; // en el host
	Projection* c_projections;

	numBytes = total_points * sizeof(Projection);

	projections = (Projection*)malloc(numBytes);
	cudaMalloc((void**)&c_projections, numBytes);
	bytes += numBytes;

	//Profiling
	float* c_test;
	numBytes = 3 * sizeof(float);
	cudaMalloc((void**)&c_test, numBytes);
	cudaMemset(c_test, 0, numBytes);

	float * test;
	test = (float*)malloc(3 * sizeof(float));

	////Dimension de hilos por bloque
	dim3 dimBlock(blk_size);

	//// Rejilla unidimensional
	dim3 dimGrid((N + dimBlock.x - 1) / dimBlock.x);

	printf("DimBlock: %d ; DimGrid: %d \n", dimBlock.x, dimGrid.x);


	printf("Cloud size: %d \n", bytes);


	//KERNEL PROJECTION
	//Cada tarea realiza la proyecci�n de un punto 3D sobre una de las c�maras desde la que es visible
	cudaEventRecord(start);

	mapping_kernel << <dimGrid, dimBlock >> > (c_test, c_cloud, c_cameras, c_points, c_images, c_rotation_matrix, c_projections, total_points);

	cudaDeviceSynchronize();
	cudaEventRecord(stop);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(gpu_time, start, stop);


	cudaMemcpy(projections, c_projections, total_points * sizeof(Projection), cudaMemcpyDeviceToHost); // GPU -> CPU
	cudaMemcpy(test, c_test, 3 * sizeof(float), cudaMemcpyDeviceToHost); // GPU -> CPU  

	//printf("TEST -> 0: %f 1: %f  \n", test[0], test[1]);

	//printf("Projection -> x: %f y: %f  \n", projections[0].px, projections[0].py);

	//Liberar memoria
	cudaFree(c_cloud);
	cudaFree(c_cameras);
	cudaFree(c_points);
	cudaFree(c_images);
	cudaFree(c_rotation_matrix);
	cudaFree(c_projections);

	free(cameras);
	free(points);

	return projections;
}


Projection* mapping_multi_GPU(Point* cloud, const int cloud_size, int num_projections, int* v_cameras, Camera_points* camera_points, vector<Point_cameras> point_cameras, Image* images, float* v_rot_matrix, const unsigned int blk_size, float* gpu_time)
{

	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);
	
	float dynamic_memory = 0;
	float lock_memory = 0;
	unsigned int numBytes;

	struct cudaDeviceProp capabilities;
	cudaGetDeviceProperties(&capabilities, 0);
	//float gpu_memory = capabilities.totalGlobalMem / 1024.0f / 1024.0f;
	float gpu_memory = 1024.0f; //se considera que la gpu tiene 1 GB de memoria
	printf("Gobal memory: %.2f MB\n", gpu_memory);
	
	/*Memoria din�mica*/

	//--Nube de puntos
	numBytes = cloud_size * sizeof(Point);
	dynamic_memory += numBytes;

	printf("CLOUD: %f MB \n", dynamic_memory/1024.0/1024.0);
	
	//--vectores auxiliares

	dynamic_memory += 2 * num_projections * sizeof(int);

	//Datos de una im�gen
	Image *c_images;
	numBytes = num_images * sizeof(Image);
	cudaMalloc((void**)&c_images, numBytes);
	cudaMemcpy(c_images, images, numBytes, cudaMemcpyHostToDevice);
	lock_memory += numBytes;

	//Matriz de rotaci�n
	float* c_rotation_matrix;
	numBytes = 3 * 3 * sizeof(float);
	cudaMalloc((void**)&c_rotation_matrix, numBytes);
	cudaMemcpy(c_rotation_matrix, v_rot_matrix, numBytes, cudaMemcpyHostToDevice);
	lock_memory += numBytes;

	//Vector de proyecciones (RESULTADO)
	Projection* projections; // en el host
	numBytes = num_projections * sizeof(Projection);
	projections = (Projection*)malloc(numBytes);
	
	dynamic_memory += numBytes;

	/*Profiling*/
	float* c_test;
	numBytes = 3 * sizeof(float);
	cudaMalloc((void**)&c_test, numBytes);
	cudaMemset(c_test, 0, numBytes);
	float * test;
	test = (float*)malloc(3 * sizeof(float));

	//Dimension de hilos por bloque
	dim3 dimBlock(blk_size);

	// Rejilla unidimensional
	dim3 dimGrid((N + dimBlock.x - 1) / dimBlock.x); //REVISAR

	printf("DimBlock: %d ; DimGrid: %d \n", dimBlock.x, dimGrid.x);
	
	//printf("Lock memory: %f bytes\n", lock_memory);
	//printf("Dynamic memory: %f bytes\n", dynamic_memory);

	/**** PARTICIONES ****/
	gpu_memory -= (lock_memory / 1024.0f / 1024.0f);
	dynamic_memory = dynamic_memory / 1024.0f / 1024.0f;
	printf(" GPU: %f /// REQUIRED: %f \n", gpu_memory,  dynamic_memory);

	int partitions = 1;
	if (dynamic_memory > gpu_memory) {
		partitions += dynamic_memory / gpu_memory;
	}
	printf("Partitions: %d \n", partitions);
	/* KERNEL */
	//Cada tarea realiza la proyecci�n de un punto 3D sobre una de las c�maras desde la que es visible
	cudaEventRecord(start);
	
	int size = point_cameras.size() / partitions;
	int num_points = point_cameras.size();
	int desp = 0;
	int desp2 = 0;
	int it = 0;
	int num_cameras;
	//printf("SIZE: %d \n", point_cameras.size());

	for (int i = 0; i < partitions; i++) {
		num_cameras = 0;
		//Si es la �ltima partici�n
		if (i == partitions - 1 && partitions > 1) {
			size = num_points - size;
		}
		//printf("s: %d \n", size);

		//Bloque de la nube
		Point *block_cloud;
		block_cloud = (Point*)malloc(size * sizeof(Point));
		for (int j = 0; j < size; j++) {
			block_cloud[j] = cloud[point_cameras[j + desp].id_point];
			num_cameras += point_cameras[j + desp].num_cameras;
		}

		//Se copia a GPU el bloque de nube
		Point * c_block;
		numBytes = size * sizeof(Point);
		cudaMalloc((void**)&c_block, numBytes);
		cudaMemcpy(c_block, block_cloud, numBytes, cudaMemcpyHostToDevice);

		//Vectores auxiliares de puntos y c�maras

		int* b_cameras;
		int* b_points;
		numBytes = num_cameras * sizeof(int);
		b_cameras = (int*)malloc(numBytes);
		b_points = (int*)malloc(numBytes);
		it = 0;
		for (int j = 0; j < size; j++) {
			for (int k = 0; k < point_cameras[j + desp].num_cameras; k++) {
				b_cameras[it] = point_cameras[j + desp].cameras[k];
				b_points[it] = j;
				++it;
			}
		}
		
		int* c_cameras;
		int* c_points;
		cudaMalloc((void**)&c_cameras, numBytes);
		cudaMalloc((void**)&c_points, numBytes);

		//Se copian bloques de puntos y c�maras a la GPU
		cudaMemcpy(c_points, b_points, numBytes, cudaMemcpyHostToDevice);
		cudaMemcpy(c_cameras, b_cameras, numBytes, cudaMemcpyHostToDevice);
		
		//Vector de resultados
		Projection* proj; // en el host
		Projection* c_proj;
		numBytes = num_cameras * sizeof(Projection);
		proj = (Projection*)malloc(numBytes);
		cudaMalloc((void**)&c_proj, numBytes);


		mapping_kernel << <dimGrid, dimBlock >> > (c_test, c_block, c_cameras, c_points, c_images, c_rotation_matrix, c_proj, num_cameras);

		cudaMemcpy(proj, c_proj, num_cameras * sizeof(Projection), cudaMemcpyDeviceToHost); // GPU -> CPU
		cudaMemcpy(test, c_test, 3 * sizeof(float), cudaMemcpyDeviceToHost); // GPU -> CPU  

		for (int j = 0; j < num_cameras; j++) {
			projections[j + desp2] = proj[j];
		}

		desp += size;
		desp2 += num_cameras;
		cudaFree(c_proj);
		cudaFree(c_block);
		cudaFree(c_points);

	}

	cudaDeviceSynchronize();
	cudaEventRecord(stop);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(gpu_time, start, stop);
	


	//printf("TEST -> 0: %f 1: %f  \n", test[0], test[1]);

	printf("Projection -> x: %f y: %f  \n", projections[1].px, projections[1].py);

	//Liberar memoria
	cudaFree(c_images);
	cudaFree(c_rotation_matrix);

	return projections;
}


void oclussion_GPU(Projection* _projections, int num_projections, int* v_cameras, const unsigned int blk_size, float* gpu_time) {

	cudaEvent_t start, stop;
	cudaEventCreate(&start);
	cudaEventCreate(&stop);


	Projection* projections;
	projections = (Projection*)malloc(num_projections * sizeof(Projection));
	int rows = 960;
	int cols = 1280;
	
	//Ordenar por filas
	int * row_points;
	row_points = (int*)malloc(num_images * rows * sizeof(int));
	int temp = 0;
	int it = 0;
	int cont = 0;
	for (int i = 0; i < num_images; i ++) {
		for (int k = 0; k < rows; k++) {
			for (int j = temp; j < temp + v_cameras[i]; j++) {
				if (_projections[j].px == k) {
					projections[it] = _projections[j];
					++it;
					++cont;
				}
			}
			row_points[rows * i + k] = cont;
		}
		temp += v_cameras[i];
	}

	//Ordenar por p�xeles
	
	//int * points;

	//int img_size = rows * cols;
	//points = (int*)malloc(num_images * rows * cols * sizeof(int));

	//int temp = 0;
	//int it = 0;
	//int cont = 0;
	//for (int i = 0; i < num_images; i++) {
	//	for (int k = 0; k < img_size; k++) {
	//		for (int j = temp; j < temp + v_cameras[i]; j++) {
	//			int pos = _projections[j].px * cols + _projections[j].py;
	//			if (pos == k) {
	//				projections[it] = _projections[j];
	//				++it;
	//				++cont;
	//			}
	//		}
	//		points[(img_size * i) + k] = cont;
	//	}
	//	temp += v_cameras[i];
	//}

	//for (int i = 0; i < 960 * num_images;i++) {
	//	printf("%d ", row_points[i]);
	//}


	Projection* c_projections;
	int numBytes = num_projections * sizeof(Projection);
	cudaMalloc((void**)&c_projections, numBytes);
	cudaMemcpy(c_projections, projections, numBytes, cudaMemcpyHostToDevice);


	//int *c_points;
	//numBytes = num_images * img_size * sizeof(int);
	//cudaMalloc((void**)&c_points, numBytes);
	//cudaMemcpy(c_points, points, numBytes, cudaMemcpyHostToDevice);


	int *c_row_points;
	numBytes = num_images * 960 * sizeof(int);
	cudaMalloc((void**)&c_row_points, numBytes);
	cudaMemcpy(c_row_points, row_points, numBytes, cudaMemcpyHostToDevice);

	//int* c_cameras;
	//numBytes = num_images * sizeof(int);
	//cudaMalloc((void**)&c_cameras, numBytes);
	//cudaMemcpy(c_cameras, v_cameras, numBytes, cudaMemcpyHostToDevice);

	//Profiling
	float* c_test;
	numBytes = 3 * sizeof(float);
	cudaMalloc((void**)&c_test, numBytes);
	cudaMemset(c_test, 0.0, numBytes);

	float * test;
	test = (float*)malloc(3 * sizeof(float));
	
	//Dimension de hilos por bloque
	dim3 dimBlock(blk_size);

	// Rejilla unidimensional
	dim3 dimGrid(((num_images * rows) + dimBlock.x - 1) / dimBlock.x);
	//dim3 dimGrid(((num_images * img_size) + dimBlock.x - 1) / dimBlock.x);
	//Kernel
	cudaEventRecord(start);
	//oclussion_kernelv1 << <dimGrid, dimBlock >> > (c_projections, c_cameras, num_images, c_test);
	oclussion_kernelv2 << <dimGrid, dimBlock >> > (c_projections, c_row_points, num_images, c_test);
	//oclussion_kernelv3 << <dimGrid, dimBlock >> > (c_projections, c_points, num_images, c_test);

	cudaDeviceSynchronize();
	cudaEventRecord(stop);
	cudaEventSynchronize(stop);
	cudaEventElapsedTime(gpu_time, start, stop);
	*gpu_time = *gpu_time; //milliseconds

	cudaMemcpy(_projections, c_projections, num_projections * sizeof(Projection), cudaMemcpyDeviceToHost); // GPU -> CPU
	cudaMemcpy(test, c_test, 3 * sizeof(float), cudaMemcpyDeviceToHost); // GPU -> CPU  

	//printf("test: %f  \n", test[0]);
	cont = 0;
	for (int i = 0; i < num_projections; i++) {
		if(_projections[i].occluded == true)
			cont++;
	//	if(_projections[i].id_point > 201746 && _projections[i].id_point < 201764)
	//		printf("ID: %d CAM: %d oc: %d px: %f py: %f dist: %f  \n", _projections[i].id_point, _projections[i].c, _projections[i].occluded, _projections[i].px, _projections[i].py, _projections[i].dist);
	}
	printf("Number of occluded points: %d \n", cont);

}

__device__ void GPU_matrix_trans(float* trans, float* matrix)
{
	for (int c = 0; c < 3; c++) {
		for (int d = 0; d < 3; d++) {
			trans[c * 3 + d] = matrix[d * 3 + c];
		}
	}
}

__device__ void GPU_mult_matrix(float* result, float* m1, float* m2, int n) {

	float res;
	for (int r = 0; r < n; ++r) {
		for (int c = 0; c < n; ++c) {
			res = 0;
			for (int k = 0; k < n; ++k) {
				res += m1[r * n + k] * m2[k * n + c];
			}
			result[r * n + c] = res;
		}
	}
}


__global__ void mapping_kernel(float* c_test, Point* c_cloud, int* c_cameras, int* c_points, Image* c_images, float* c_rotation_matrix, Projection* c_projections, const int total_points)
{

	int task = blockIdx.x * blockDim.x + threadIdx.x;

	if (task < total_points) {

		//Camara visible desde ese punto
		//Id del punto en la nube
		//int cam = point_cameras[it].cameras[k];
		//int id_point = point_cameras[it].id_point;
		int cam = c_cameras[task];
		int id_point = c_points[task];

		/*****Transformaci�n geom�trica******/

		float trans_m1[9]; //matriz transpuesta de la matriz de rotaci�n de la c�mara
		float trans_m2[9];//matriz transpuesta de la matriz de rotaci�n de la transformaci�n ICP
		float rot_m[9];
		
		//Transposici�n de matrices
		GPU_matrix_trans(trans_m1, c_images[cam].rotation_matrix);
		GPU_matrix_trans(trans_m2, c_rotation_matrix);

		//Multiplicaci�n de matrices
		GPU_mult_matrix(rot_m, trans_m1, trans_m2, 3);

		//Traslaci�n y Rotaci�n
		float res[3];
		float temp;
		for (int r = 0; r < 3; ++r) {
			temp = 0;
			for (int c = 0; c < 3; ++c) {

				temp += rot_m[r * 3 + c] * (c_cloud[id_point].position[c] - c_images[cam].position[c]);
			}
			res[r] = temp;
		}

		/*****Proyecci�n*******/

		Projection p;
		//Fisheye Polynomial
		float D[4] = { c_images[cam].D[0], c_images[cam].D[1], c_images[cam].D[2], c_images[cam].D[3] };
		//Fisheye Affine Matrix
		float A[4] = { c_images[cam].A[0], c_images[cam].A[1], c_images[cam].A[2], c_images[cam].A[3] };

		float cx = c_images[cam].cx;
		float cy = c_images[cam].cy;

		//Punto 3D
		float x = res[0];
		float y = res[1];
		float z = res[2];

		float r = sqrt(pow(x, 2) + pow(y, 2));
		float theta = (2 / M_PI) * atan(r / z);
		float poly = D[0] + D[1] * theta + D[2] * theta * theta + D[3] * theta * theta * theta;
		float xh = (poly * x) / r;
		float yh = (poly * y) / r;

		//Coordenadas de la imagen

		float px = A[2] * xh + A[3] * yh + cy;
		float py = A[0] * xh + A[1] * yh + cx;

		//P�xel en la imagen proyectado por el Punto 3D
		p.px = round(px);
		p.py = round(py);

		//if (task == 0) {
		//	c_test[0] = p.px;
		//	c_test[1] = p.py;
		//}

		p.c = cam;
		p.id_point = id_point;
		float distance = sqrtf(pow((x - c_images[cam].position[0]), 2) + pow((y - c_images[cam].position[1]), 2) + pow((z - c_images[cam].position[2]), 2));
		p.dist = distance;
		p.occluded = true;
		//if (px < 0 || px > 960 || py < 0 || py > 1280) {
		//	p.occluded = true;
		//}

		//__syncthreads();

		//ordenar el vector
		//Se conoce para cada proyecci�n el px y py
		//Hay que conocer el n�mero total de puntos que caen en el pixel.
		//El problema que no se conoce la forma de partcionar de forma din�mica este array

		c_projections[task] = p;
	}

}



//Se chequea para cada fila (hilo) todos los puntos que caen dentro del viewport de la imagen
__global__ void oclussion_kernelv1(Projection* c_projections, int* c_cameras, const int num_images, float* c_test) {

	//ID del hilo	
	int task = blockIdx.x * blockDim.x + threadIdx.x;
	
	//Registros
	int px, py;
	float dist;
	const int cols = 1280;
	const int rows = 960;
	
	float matrix [cols];
	int indices [cols];
	int desp = 0;
	//Inicializaci�n
	for (int i = 0; i < cols; i++) {
		matrix[i] = 10000.0;
		indices[i] = -1;
	}

	if (task < (num_images * rows)) {
	
		int id_row = task % rows;
		int id_cam = task / rows;

		//int desp2 = cols * id_row;

		if (task > 0) {
			for (int i = 0; i < id_cam; i++) {
				desp += c_cameras[i];
			}
		}

		for (int j = desp; j < desp + c_cameras[id_cam]; j++) {
			
			px = c_projections[j].px;
			if (px == id_row) {
				py = c_projections[j].py;
				dist = c_projections[j].dist;
				c_projections[j].occluded = true;

				if (dist < matrix[py]) {
					//Si ya hay otro punto proyectado
					if (indices[py] != -1) {
						c_projections[indices[py]].occluded = true;
					}
					matrix[py] = dist;
					indices[py] = j;
					c_projections[j].occluded = false;
				}
			}
		}
	}
}

//Se chequea para cada fila
__global__ void oclussion_kernelv2(Projection* c_projections, int* c_row_points, const int num_images, float* c_test) {

	//ID del hilo	
	int task = blockIdx.x * blockDim.x + threadIdx.x;

	//Registros
	int py;
	float dist;
	const int cols = 1280;
	const int rows = 960;

	float zbuffer[cols];
	int indices[cols];
	int desp = 0;
	//Inicializaci�n
	for (int i = 0; i < cols; i++) {
		zbuffer[i] = 10000.0;
		indices[i] = -1;
	}
	
	Projection rowpoints;

	if (task < (num_images * rows)) {

		int limit = c_row_points[task];
		if (task > 0) {
			desp = c_row_points[task - 1];
		}
		//Secuencialmente se comprueba con el zbuffer los puntos para cada fila de la imagen
		for (int j = desp; j < limit; j++) {
			//global memory to local memory
			rowpoints = c_projections[j];

			py = rowpoints.py;
			dist = rowpoints.dist;
			//Si la distancia es menor a la del zbuffer
			if (dist < zbuffer[py]) {
				//Si ya hay un punto proyectado
				if (indices[py] != -1)
					c_projections[indices[py]].occluded = true;
				zbuffer[py] = dist;
				indices[py] = j;
				c_projections[j].occluded = false;
			}
		}
	}
}


__global__ void oclussion_kernelv3(Projection* c_projections, int* c_points, const int num_images, float* c_test) {

	//ID del hilo	
	int task = blockIdx.x * blockDim.x + threadIdx.x;

	//Registros
	int py;
	float dist;
	const int cols = 1280;
	const int rows = 960;

	float zbuffer = 10000.0;
	int indice = -1;
	int desp = 0;

	Projection rowpoints;

	if (task < (num_images * rows * cols)) {

		int limit = c_points[task];
		if (task == 960)
			c_test[0] = 10;

		if (task > 0) {
			desp = c_points[task - 1];
		}

		for (int j = desp; j < limit; j++) {
			//global memory to local memory
			rowpoints = c_projections[j];

			py = rowpoints.py;
			dist = rowpoints.dist;

			if (dist < zbuffer) {
				//Si ya hay un punto proyectado
				if (indice != -1)
					c_projections[indice].occluded = true;
				zbuffer = dist;
				indice = j;
				c_projections[j].occluded = false;
			}
		}

	}
}



/*
  Funci�n principal
*/
int main(int argc, char* argv[])
{

	// N�mero de elementos en los vectores (predeterminado: N)
	unsigned int n = (argc > 1) ? atoi(argv[1]) : N;

	if (n == 0) {
		devicenfo();
		return(0);
	}
	/*LECTURA DE FICHEROS*/
	//Se lee la nube de puntos
	read_cloud(n);

	//Se lee fichero 1: im�genes
	Image *images = read_images();

	//Se lee fichero 2: c�maras por punto / puntos por c�mara

	Camera_points* camera_points;
	camera_points = (Camera_points*)malloc(num_images * sizeof(Camera_points));
	vector<Point_cameras> point_cameras = read_cameras_point(camera_points, n);
	   
	//Se lee fichero 3: matriz de transformaci�n (ICP)
	float *transf_matrix;
	transf_matrix = (float*)malloc(4 * 4 * sizeof(float));
	read_transformation_matrix(transf_matrix);
	//Se lee fichero 4: matrices de reflectancia
	Reflectance_matrix* reflectance_v = read_reflectance_matrix();

	printf("Cloud size = %u \n", n);
	printf("Number of images = %u \n", num_images);
	printf("Number of 3D points to be projected: %d \n", point_cameras.size());
	printf("Total of projections to be calculated: %d \n", num_projections);

	//Calculo de la matriz de rotaci�n y escala (ICP)
	float* rotation_matrix;
	rotation_matrix = (float*)malloc(3 * 3 * sizeof(float));
	float v_scale[3];
	for (int i = 0; i < 3; i++) {
		float s_index = 0;
		for (int j = 0; j < 3; j++) {
			s_index += transf_matrix[j * 4 + i] * transf_matrix[j * 4 + i];
		}
		v_scale[i] = sqrtf(s_index);
	}
	for (int i = 0; i < 3; i++) {
		for (int j = 0; j < 3; j++) {
			rotation_matrix[j * 3 + i] = transf_matrix[j * 4 + i] / v_scale[i];
		}
	}


	/****************CPU*********************/
	
	Projection** projections;
	projections = (Projection**)malloc(point_cameras.size() * sizeof(Projection*));

	printf("\n----------------CPU---------------- \n");

	clock_t start, end;
	clock_t start2, end2;
	float cpu_time_used;

	start = clock();
	mapping(projections, cloud, point_cameras, images, rotation_matrix);
	end2 = clock();
	cpu_time_used = ((float)((end2 - start)) / CLOCKS_PER_SEC) * 1000;
	printf("Time for Mapping: %f milliseconds \n", cpu_time_used);

	start2 = clock();
	oclussion_test(projections, point_cameras);
	end = clock();
	cpu_time_used = ((float)((end - start2)) / CLOCKS_PER_SEC) * 1000;
	printf("Time for Occlusion: %f milliseconds \n", cpu_time_used);

	cpu_time_used = ((float)((end - start)) / CLOCKS_PER_SEC) * 1000;
	printf(" -> CPU time: %f milliseconds \n", cpu_time_used);
		

	/****************GPU*********************/

	printf("\n----------------GPU---------------- \n");
	// N�mero de hilos en cada bloque CUDA (predeterminado: CUDA_BLK)
	unsigned int cb = (argc > 2) ? atoi(argv[2]) : CUDA_BLK;

	Projection* gpu_projections;
	float gpu_time = 0;
	float gpu_time2 = 0;
	int* v_cameras;
	v_cameras = (int*)malloc(num_images * sizeof(int));
	start = clock();
	//gpu_projections = mapping_GPU(cloud, n, num_projections, v_cameras, camera_points, point_cameras, images, rotation_matrix, cb, &gpu_time);
	gpu_projections = mapping_multi_GPU(cloud, n, num_projections, v_cameras, camera_points, point_cameras, images, rotation_matrix, cb, &gpu_time);

	printf("Time for Mapping (kernel): %f milliseconds\n", gpu_time);

	//oclussion_GPU(gpu_projections, num_projections,v_cameras, cb, &gpu_time2);
	printf("Time for Occlusion (kernel): %f milliseconds\n", gpu_time2);

	end = clock();
	float gpu_time_used = ((float)(end - start)) / CLOCKS_PER_SEC * 1000;
	printf(" -> GPU time: %f milliseconds \n", gpu_time_used);

	// Reservamos e inicializamos vectores
	//puts("Press <enter> to quit:");
	//getchar();
	return(0);

}
